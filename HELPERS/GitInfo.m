%GITINFO    Get information about git repository.
%
%   GITINFO properties:
%       branch          - Current Branch
%       sha             - ID of current commit
%       remote_url      - URL of git repoistory
%
%   GITINFO methods:
%       GitInfo         - Constructor for GitInfo object
%
%   Author: Maximilian Behr (MPI Magdeburg, CSC)

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019
%

classdef GitInfo

    properties(SetAccess = private)
        branch; % current branch
        sha; % ID of current commit
        remote_url; % URL of git repository
    end

    methods
        function obj = GitInfo()
            [~, branch] = system('git rev-parse --abbrev-ref HEAD');
            obj.branch = strtrim(branch);

            [~, sha] = system('git rev-parse HEAD');
            obj.sha = strtrim(sha);

            [~, remote_url] = system('git config --get remote.origin.url');
            obj.remote_url = strtrim(remote_url);
        end
    end
end
