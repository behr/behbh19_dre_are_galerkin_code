function nrm = two_norm_sym_handle(fun, n)
%TWO_NORM      Computes the 2-norm of an symmetric matrix given as a
%   function handle.
%
%   NRM = TWO_NORM_SYM_HANDLE(FUN, N) compute the 2-norm of a symmetric
%   matrix given as a function handle FUN. N is the dimension of the
%   space or the number of rows/cols of the matrix, which is represented
%   by the function handle FUN. FUN must return the matrix vector product.
%
%   See also TEST_SPLITTING_DRE.
%
%   Author: Maximilian Behr (MPI Magdeburg, CSC)

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019
%

%% check inputs
assert(isa(fun, 'function_handle'), '%s: %s is no function handle.', mfilename, inputname(1));
validateattributes(n, {'double'}, {'real', 'square', '>', 0, 'scalar', 'integer', 'nonnan', 'finite', 'nonsparse'}, mfilename, inputname(2));

%% compute the 2-norm as largest eigenvalue in magnitude
eopts = struct('isreal', true, 'issym', true);
nrm = abs(eigs(fun, n, 1, 'LM', eopts));
end
