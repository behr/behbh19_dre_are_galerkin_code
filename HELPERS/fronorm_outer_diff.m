function nrm = fronorm_outer_diff(L1, X1, L2, X2)
%FRONORM_OUTER_DIFF     Computes the Frobenius norm of L1*X1*L1'-L2*X2*L2'
%   for low-rank factorizations.
%
%   NRM = FRONORM_OUTER_DIFF(L1, X1, L2, X2) compute the Frobenius norm of the
%   diffence of the low-rank L1*X1*L1' and L2*X2*L2'.
%   All matrices must be real and dense. X1 and X2 must be symmetric.
%   The symmetry is not checked. FRONRM_OUTER_DIFF is only efficient if the number
%   of columns of L1 and L2 compared to the number of rows L1 and L2 is small.
%
%   See also FRONORM_OUTER.
%
%   Author: Maximilian Behr (MPI Magdeburg, CSC)

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019
%

%% check inputs
validateattributes(L1, {'double'}, {'real', '2d'}, mfilename, inputname(1));
validateattributes(X1, {'double'}, {'real', '2d', 'square'}, mfilename, inputname(2));
validateattributes(L2, {'double'}, {'real', '2d'}, mfilename, inputname(3));
validateattributes(X2, {'double'}, {'real', '2d', 'square'}, mfilename, inputname(4));

%% compute Frobenius norm via eigenvalues
%X1_ = 0.5*(X1+X1');
%X2_ = 0.5*(X2+X2');
%ret = sqrt(sum(real(eig(([Q1,Q2]'*[Q1,Q2])*blkdiag(X1_,-X2_))).^2));
% variant above seems to be inaccurate

X1_ = 0.5 * (X1 + X1');
X2_ = 0.5 * (X2 + X2');

[~, R, p] = qr([L1, L2], 0);
per(p) = 1:length(p);
R = R(:, per);
nrm = sqrt(sum(real(eig(R*blkdiag(X1_, -X2_)*R')).^2));

end