function nrm = fronorm_outer(L, X)
%FRONORM_OUTER      Computes the Frobenius norm of L*X*L' of a low-rank
%   factorization.
%
%   NRM = FRONORM_OUTER(L, X) compute the Frobenius norm of a low-rank
%   factorization L*X*L'. FRONRM_OUTER is only efficient if the number
%   of columns of L compared to the number of rows is small.
%   L and X must be real and dense. X is assumed to be symmetric.
%   FRONORM_OUTER does not check the symmetry of X.
%
%   See also FRONORM_OUTER_DIFF.
%
%   Author: Maximilian Behr (MPI Magdeburg, CSC)

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019
%

%% check inputs
validateattributes(L, {'double'}, {'real', '2d'}, mfilename, inputname(1));
validateattributes(X, {'double'}, {'real', '2d', 'square'}, mfilename, inputname(2));

%% compute Frobenius norm via eigenvalues
%X2 = 0.5*(X+X');
%ret = sqrt(sum(real(eig((Q'*Q)*X2)).^2));
% variant above seems to be inaccurate

%% compute a qr factorization of L
X2 = 0.5 * (X + X');
[~, R, p] = qr(L, 0);
per(p) = 1:length(p);
R = R(:, per);
nrm = sqrt(sum(real(eig(R*X2*R')).^2));

end
