function control_parfor(par)
%CONTROL_PARFOR Enable/disable automatic parallel pool creation.
%
%   CONTROL_PARFOR(PAR) turns automatic parallel pool creation on/ off
%   in parfor loops. PAR must be a logical.
%
%   Author: Maximilian Behr (MPI Magdeburg, CSC)

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019
%

if par
    ps = parallel.Settings;
    ps.Pool.AutoCreate = true;
    poolobj = gcp('nocreate');
    if isempty(poolobj)
        parpool('local');
    end
else
    ps = parallel.Settings;
    ps.Pool.AutoCreate = false;
    delete(gcp);
end

end
