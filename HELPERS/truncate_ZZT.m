function [Z_new, Q_new, S_new, cols_new, cols_old] = truncate_ZZT(Z, trunc_rel_tol)
%TRUNCATE_ZZT    Truncate a real low-rank factor Z using a compact svd.
%
%   [Z_new, Q_new, S_new, cols_new, cols_old,  nrm2_Z_new, nrmF_Z_new, absnrm2_err, relnrm2_err] = TRUNCATE_ZZT(Z, trunc_rel_tol)
%   trucates the low-rank factor Z using an ecnomy svd.
%   It holds that Z_new = Q_new * S_new. Z must be real, and dense.
%   The truncation is done, such that the inequality
%
%       || Z_new - Z ||_2  <= trunc_rel_tol * || Z ||_2
%
%   holds.
%
%   The matrix S_new is a real diagonal matrix. The diagonal entries
%   are ordered in a non-increasing fashion.
%   cols_new is the number of columns of Z_new or Q_new.
%   cols_old is the number of columns of Z_old.
%
%   See also: FRONORM_OUTER, FRONORM_OUTER_DIFF and TWO_NORM_SYM_HANDLE.
%
%   Author: Maximilian Behr (MPI Magdeburg, CSC)

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019

%% check input arguments
validateattributes(Z, {'double'}, {'real', 'nonnan', 'finite', '2d'}, mfilename, inputname(1));
validateattributes(trunc_rel_tol, {'double'}, {'real', 'square', '>', 0, 'scalar'}, mfilename, inputname(2));

%% Compute the economy svd and truncate
cols_old = size(Z,2);
[Q, S, ~] = svd(Z, 0);
tol = trunc_rel_tol * max(abs(diag(S)));
idx = diag(S) > tol;

% avoid return of something empty
if ~any(idx)
    idx = 1;
    warning('%s would truncate everything. The best rank one approximation is used instead.', mfilename);
end

Q_new = Q(:, idx);
S_new = abs(S(idx, idx));
Z_new = Q_new * S_new;
cols_new = size(Z_new,2);

end