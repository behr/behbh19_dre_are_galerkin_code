function ret = send_mail_system_mail(subject, content, recipent)
%SEND_MAIL_SYSTEM_MAIL  Send an email using the system mail command.
%
%   RET = SEND_MAIL_SYSTEM_MAIL(SUBJECT, CONTENT, RECIPENT)
%   sends a mail with subject SUBJECT, content CONTENT to the recipent
%   RECIPENT. SEND_MAILSYSTEM_MAIL uses the unix mail command.
%   The exit code from the mail command is returned.
%
%   Author: Maximilian Behr (MPI Magdeburg, CSC)

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019
%

ret = system(sprintf('echo "%s" | mail -s "%s" %s', content, subject, recipent));

end
