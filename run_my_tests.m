%RUN_MY_TESTS   Runs all unit tests of the project.
%
%   RUN_MY_TESTS() runs all unit tests.
%
%   Author: Maximilian Behr (MPI Magdeburg, CSC)

%
%  This program is free software; you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation; either version 2 of the License, or
%  (at your option) any later version.
%
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
%
%  You should have received a copy of the GNU General Public License
%  along with this program; if not, see <http://www.gnu.org/licenses/>.
%
%  Copyright (C) Maximilian Behr
%                2019
%

function ret = run_my_tests()

%% import test package
import matlab.unittest.TestSuite;

%% create and run suites
[mydir, ~, ~] = fileparts(mfilename('fullpath'));

suite_names = { ...
    'EXAMPLE_DAVISON_MAKI_FAIL', 'EXAMPLE_EIG_DECAY_ARE', ...
    'EXAMPLE_EIG_DECAY_DRE', 'EXAMPLE_ENTRIES_DECAY_DRE', ...
    'SPLITTING_DRE', 'ARE_GALERKIN_DRE', 'HELPERS', ...
    };

for i_suite_names = 1:length(suite_names)
    suite_name = suite_names{i_suite_names};
    suite = TestSuite.fromFolder(strcat(mydir, '/', suite_name));
    results = run(suite);
    disp(results);
    ret = any([results.Failed]);
    if ret, warning('Suite %s failed.', suite_name);
        return;
    end
    
end
end
