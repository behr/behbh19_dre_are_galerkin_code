---
title: Invariant Galerkin Ansatz Spaces and Davison Maki Methods for the Numerical Solution of Differential Riccati Equations
authors: Maximilian Behr, Peter Benner, Jan Heiland
journal:
DOI: https://doi.org/10.5281/zenodo.2629737
---

[CMESS]: https://gitlab.mpi-magdeburg.mpg.de/mess/cmess
[MATLAB]: https://de.mathworks.com/
[CSCBIB]: https://gitlab.mpi-magdeburg.mpg.de/csc-administration/csc-bibfiles
[GPLV2]: https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
[BGPLV2]: https://img.shields.io/badge/License-GPL%20v2-blue.svg
[GPLV3]: https://www.gnu.org/licenses/gpl-3.0
[BGPLV3]: https://img.shields.io/badge/License-GPL%20v3-blue.svg
[ZENODO_BADGE]: https://zenodo.org/badge/doi/10.5281/zenodo.2629737.svg
[ZENODO_URL]: https://doi.org/10.5281/zenodo.2629737
[DRESPLIT]: http://www.tonystillfjord.net/DREsplit_20180618.zip
[MPIMD]: https://www.mpi-magdeburg.mpg.de


# BehBH19_DRE_ARE_GALERKIN_CODE
Latex Files and `Matlab` Code for the Preprint.


**License:**

[![License: GPL v2][BGPLV2]][GPLV2] [![License: GPL v3][BGPLV3]][GPLV3]

**Copyright 2019 by Maximilian Behr (MPI Magdeburg)**

[![pipeline status](https://gitlab.mpi-magdeburg.mpg.de/behr/behbh19_dre_are_galerkin_code/badges/master/pipeline.svg)](https://gitlab.mpi-magdeburg.mpg.de/behr/behbh19_dre_are_galerkin_code/commits/master)

[![ZENODO_BADGE]][ZENODO_URL]

The [MATLAB][MATLAB] Code is licensed under `GPL v2` or later.
See [LICENSE](LICENSE) for details.

## Requirements
This Project depends on several other `git` repositories:

* [C-M.E.S.S][CMESS] (`GPL v2` or later, Copyright 2009 - 2018 by Peter Benner, Martin Koehler, Jens Saak (MPI Magdeburg)) and it`s submodules
* [csc-bibfiles][CSCBIB]


These repositories are added as submodules to this project.
Therefore you have to clone this project using:

```
git clone --recursive git@gitlab.mpi-magdeburg.mpg.de:behr/behbh19_dre_are_galerkin_code.git
```

The project needs the [MATLAB][MATLAB] interface of [C-M.E.S.S][CMESS]. You have to build it before using the codes.

For comparison we use the splitting scheme solver from Tony Stillfjord [DRESPLIT][DRESPLIT].

## Description of the Directories and Files

|   Directory / File            |   Description                                         |
|:-----------------------------:|:-----------------------------------------------------:|
|    ARE_GALERKIN_DRE           |   Algebraic Riccati Equation approach for DRE         |
|    ci-scripts                 |   Script for Continous Integration                    |
|    cmess-releases             |   [C-M.E.S.S][CMESS] subproject                       |
|    build_cmess.sh             |   Bash script, to compile [C-M.E.S.S][CMESS]          |
|    COMPARE_DRE_SOLVERS        |   Compare different DRE Solvers                       |
|    data                       |   Test Matrices                                       |
|    EXAMPLE_DAVISON_MAKI_FAIL  |   Example Pictures for Preprint                       |
|    EXAMPLE_EIG_DECAY_ARE      |   Example Pictures for Preprint                       |
|    EXAMPLE_EIG_DECAY_DRE      |   Example Pictures for Preprint                       |
|    EXAMPLE_ENTRIES_DECAY_DRE  |   Example Pictures for Preprint                       |
|    HELPERS                    |   Functions for miscallenous tasks                    |
|    mmread                     |   `mtx` read and write functions                      |
|    MODIFIED_DAVISON_MAKI      |   Modified Davison Maki Solver for DRE                |
|    SPLITTING_DRE              |   Wrapper around [DRESPLIT][DRESPLIT]                 |
|    SPLITTING_DRE/DREsplit     |   [DRESPLIT][DRESPLIT] Code from Tony Stillfjord      |
|    add_to_path.m              |   [MATLAB][MATLAB] script to adapth the path          |
|    lint_my_code.m             |   [MATLAB][MATLAB] script to lint the code            |
|    run_my_tests               |   [MATLAB][MATLAB] script to run the unit tests       |


### Convert `*.mtx` files from `*.mat` files

The `data` directory contains the test matrices as `*.mtx` files. These
files have to be converted to `*.mat` files. You can
simply call the script

```
    data2mat
```
to do this. The results will be written to disk.

### Compile [MATLAB][MATLAB] interface to [C-M.E.S.S][CMESS]

* execute the script `build_cmess.sh` or compile [C-M.E.S.S][CMESS] and the corresponding [MATLAB][MATLAB] interface on your own

### Run [MATLAB][MATLAB] tests

* open [MATLAB][MATLAB]
* execute `add_to_path.m` script this add the necessary directories to the [MATLAB][MATLAB] path
* execute `data2mat` script to convert `*.mtx` files  to `*.mat` files
* call `run_my_tests`

### ARE Galerkin based approach

All codes for the galerkin based approached are in `ARE_GALERKIN_DRE`.

## Reproduce Results
All numerical experiments were carried out on the cluster `Mechthild` of the [Max Planck Institute Magdeburg][MPIMD].

### Get the Reference Solution
We precomputed the reference solution using the splitting scheme codes [DRESPLIT][DRESPLIT] by Tony Stillfjord.
The reference solution is available as  a `*.mat` file and can be downloaded from [ZENODO][ZENODO_URL].

### Reproduce numerical Results
All computations were carried out on the computer cluster `Mechthild` of the [Max Planck Institute Mageburg][MPIMD].
You can place the downloaded reference solutions in `data_reference_solution` or add them available to the [MATLAB][MATLAB]
`path`.
In order to reproduce the results adapt the `SLURM` jobfiles
 * `run_compare_dre_solvers_rail_are_galerkin_dre.job`
 * `run_compare_dre_solvers_rail_split.job`
if necessary, and submit them.
If you want to reproduce the results without using `SLURM`, extract the relevant parts of the jobfiles to a `bash` script and
execute it.

# Testing

The Code was tested with:

* **MATLAB 2018a**
