%ADD_TO_PATH    Script adds all necessary directories to path.
%
%   Author: Maximilian Behr (MPI Magdeburg, CSC)

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019
%

s = pwd;
addpath(s);
addpath(genpath(sprintf('%s/ABSTRACT', s)));
addpath(genpath(sprintf('%s/ARE_GALERKIN_DRE', s)));
addpath(genpath(sprintf('%s/COMPARE_DRE_SOLVERS', s)));
addpath(genpath(sprintf('%s/data', s)));
addpath(genpath(sprintf('%s/data_reference_solution', s)));
addpath(genpath(sprintf('%s/EXAMPLE_DAVISON_MAKI_FAIL', s)));
addpath(genpath(sprintf('%s/EXAMPLE_EIG_DECAY_ARE', s)));
addpath(genpath(sprintf('%s/EXAMPLE_EIG_DECAY_DRE', s)));
addpath(genpath(sprintf('%s/EXAMPLE_ENTRIES_DECAY_DRE', s)));
addpath(genpath(sprintf('%s/HELPERS', s)));
addpath(genpath(sprintf('%s/mess_build/matlab', s)));
addpath(genpath(sprintf('%s/mmread', s)));
addpath(genpath(sprintf('%s/MODIFIED_DAVISON_MAKI_DRE', s)));
addpath(genpath(sprintf('%s/REFERENCE_SOLUTION', s)));
addpath(genpath(sprintf('%s/SPLITTING_DRE', s)));
mess_path;
clear s;
