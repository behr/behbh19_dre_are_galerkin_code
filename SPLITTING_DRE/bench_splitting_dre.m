% BENCH_SPLITTING_DRE   Benchmark for splitting_dre solvers.
%
% Author: Maximilian Behr (MPI Magdeburg, CSC)

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019
%

%% clear all
clearvars, close all;
clc

%% parameters
instance = 'rail_371';
T = 1;
ks = [-1, -2, -3, -4];
%split_types = cellstr(enumeration('splitting_dre_t')');
split_types = {'SPLITTING_LIE', 'SPLITTING_STRANG', 'SPLITTING_SYMMETRIC2', ...
    'SPLITTING_SYMMETRIC4', 'SPLITTING_SYMMETRIC6', 'SPLITTING_SYMMETRIC8'};
parallel_fors = [false, true];

%% load data
data = load(instance);
A = data.A;
E = data.E;
B = data.B;
C = data.C;
n = size(A, 1);

%% create filenames and firectories for results
mybasename = sprintf('%s_T%.2f', instance, T);
[mydir, ~, ~] = fileparts(mfilename('fullpath'));
mydir = fullfile(mydir, 'results', mfilename, mybasename);
[status, message, messageid] = rmdir(mydir, 's');
if ~status
    fprintf('%s - %s:%s-%s\n', datestr(now), mfilename, message, messageid);
end
mydiary = fullfile(mydir, sprintf('%s.log', mybasename));
mymat = fullfile(mydir, sprintf('%s.mat', mybasename));
mycsv = fullfile(mydir, sprintf('%s.csv', mybasename));
mkdir(mydir);

%% delete old diary and create new
delete(mydiary);
diary(mydiary);

%% prepare table
dummy = zeros(length(ks), 1);
tab = table();
tab = addvars(tab, ks', 'NewVariableNames', 'StepSize');

for parallel_for = parallel_fors
    for split_type = split_types
        tab = addvars(tab, dummy, 'NewVariableNames', sprintf('%s_parfor%d', char(split_type), parallel_for));
    end
end

%% benchmark solver
for i_parallel_for = 1:length(parallel_fors)
    for i_split_type = 1:length(split_types)
        for i_k = 1:length(ks)

            % get variable
            k = ks(i_k);
            split_type = split_types{i_split_type};
            parallel_for = parallel_fors(i_parallel_for);

            % control par for
            control_parfor(parallel_for);

            % solver options
            solver_opt = splitting_dre_options();
            solver_opt.T = T;
            solver_opt.k = k;
            solver_opt.splitting = split_type;

            % solve
            solver = splitting_dre(A, E, B, C, zeros(n, 1), solver_opt);
            solver.prepare();

            fprintf('%s - Start integration,    T = %.4f, %s, k = %d and parfor = %d.\n', datestr(now), T, char(split_type), k, parallel_for);
            solver.time_step_until(T);
            fprintf('%s - Finished integration, T = %.4f, %s, k = %d and parfor = %d.\n', datestr(now), T, char(split_type), k, parallel_for);
            fprintf('Computational Time = %f.\n\n', solver.wtime_time_step_until);

            % store in table
            tab(i_k, 1+i_split_type+length(split_types)*(i_parallel_for - 1)) = {solver.wtime_time_step_until};
        end
    end
end

%% display results
display(tab)

%% write results to files

% write csv file from table
writetable(tab, mycsv, 'Delimiter', ',');

% write mat file
hostname = getComputerName();
timenow = datestr(now);
gitinfo = GitInfo();
save(mymat, 'instance', 'ks', 'parallel_fors', 'split_types', 'tab', 'hostname', 'timenow', 'gitinfo', 'T');

%% turn diary off
diary('off');
