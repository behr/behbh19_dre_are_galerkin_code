function [phats, old_indices] = expand_interval(ps, hnew)

%
% Copyright 2018 Tony Stillfjord
%
% This file is part of DREsplit.
%
% DREsplit is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% DREsplit is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with DREsplit.  If not, see <http://www.gnu.org/licenses/>.
%


q = length(ps) + 1;

phats = [ps; hnew];

d = [phats(2); phats(3:end) - phats(1:end-2); hnew - ps(end)];
[~, ind] = min(d);

phats = [phats(1:ind-1); phats(ind+1:end)];
old_indices = [1:ind-1, ind+1:q-1]';

end
