function [L, D, errest] = additive_symmetric_with_errest_step(s, gamma, beta, h, M, A, B, IQL, IQD, L0, D0, Rinv, args)
% Do a step of size h with the 2s-order symmetric sum-splitting method
%
%       gamma(1)( exp(hG)exp(hF)P0 +  exp(hF)exp(hG)P0 ) +
%       gamma(2)( (exp(h/2G)exp(h/2F))^2 P0 + (exp(h/2F)exp(h/2G))^2 P0 ) +
%         ... +
%       gamma(s)( (exp(h/sG)exp(h/sF))^s P0 + (exp(h/sF)exp(h/sG))^s P0 )
% where
%       P0 = L0*D0*L0'.
%
% Here F(P) = M^{-T} A^T P + PAM^{-1} + M^{-T}QM^{-1} and
% G(P) = -P B Rinv B^T P.

% Further, IQL{j}*IQD{j}*IQL{j}^T should be a low-rank factorization of
% \int_{0}^{h/j}{ exp(s(A M^{-1})^T)M^{-T} Q M^{-1} exp(s A M^{-1}) ds}
%
% Estimate the error by computing the 2(s-1)-order approximation given
% by the coefficients beta rather than gamma

%
% Copyright 2018 Tony Stillfjord
%
% This file is part of DREsplit.
%
% DREsplit is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% DREsplit is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with DREsplit.  If not, see <http://www.gnu.org/licenses/>.
%

    parfor j = 1:s

        L1{j} = L0; D1{j} = D0;
        L2{j} = L0; D2{j} = D0;

        for k = 1:j
            [L1{j}, D1{j}] = expF(h / j, M, A, IQL{j}, IQD{j}, L1{j}, D1{j}, args);
            [L1{j}, D1{j}] = expG(h / j, B, Rinv, L1{j}, D1{j});

            [L2{j}, D2{j}] = expG(h / j, B, Rinv, L2{j}, D2{j});
            [L2{j}, D2{j}] = expF(h / j, M, A, IQL{j}, IQD{j}, L2{j}, D2{j}, args);
        end

    end

    % Stack the results
    L_tmp = [cell2mat(L1), cell2mat(L2)];

    D_tmp1 = D1;
    D_tmp2 = D2;

    for j = 1:s
        D_tmp1{j} = gamma(j)*D1{j};
        D_tmp2{j} = gamma(j)*D2{j};
    end

    D = blkdiag(D_tmp1{:}, D_tmp2{:});

    [L, D] = column_compress_LDLT(L_tmp, D, args.CC_tol);

    % Compute the error estimate
    if s > 1
        for j = 1:s-1
            D_tmp1{j} = (gamma(j) - beta(j))*D1{j};  % Probably more efficient than using the already
            D_tmp2{j} = (gamma(j) - beta(j))*D2{j};  % computed D_tmp and subtracting beta*D
        end
        D_tmp1{s} = gamma(s)*D1{s};
        D_tmp2{s} = gamma(s)*D2{s};
    else
        D_tmp1{1} = (gamma(1) - 1)*D1{1};   % Use the Lie splitting step as the embedded method
        D_tmp2{1} = (gamma(1) - 0)*D2{1};
    end
    Derr = blkdiag(D_tmp1{:}, D_tmp2{:});

    [Lerr, Derr] = column_compress_LDLT(L_tmp, Derr, args.CC_tol);

    errest = outerfrobnorm_LDLT(Lerr, Derr) / outerfrobnorm_LDLT(L, D);

end
