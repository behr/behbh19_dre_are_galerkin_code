% Compute order conditions for the asymmetric and symmetric additive
% splitting schemes

%
% Copyright 2018 Tony Stillfjord
%
% This file is part of DREsplit.
%
% DREsplit is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% DREsplit is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with DREsplit.  If not, see <http://www.gnu.org/licenses/>.
%


% Symmetric
n = 3;

A = zeros(n,n);
A(1,:) = 1;
for k = 1:n-1
    A(k+1, :) = (1:n).^(-2*k);
end

b = [1/2; zeros(n-1, 1)];

gamma = A\b

%
% % n=1:
% gamma = [1/2];

% % n=2:
% gamma = [-1/6; 2/3];

% % n=3:
% gamma = [1/48; -16/30; 81/80];

% % n=4:
% gamma = [-1/720; 8/45; -729/560; 512/315];



% Asymmetric
s = 4;

A = zeros(s,s);
A(1,:) = 1;
for k = 1:s-1
    A(k+1, :) = (1:s).^(-k);
end

b = [1; zeros(s-1, 1)];

gamma = A\b


% % n=1:
% gamma = [1];
%
% % n=2:
% gamma = [-1, 2];
%
% % n=3:
% gamma = [1/2, -4, 9/2];
%
% % n=4:
% gamma = [-1/6, 4, -27/2, 32/3];
