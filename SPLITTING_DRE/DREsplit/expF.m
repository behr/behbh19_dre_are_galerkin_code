function [L, D] = expF(h, M, A, IQL, IQD, L0, D0, args)
% Compute the low-rank LDLT factorization of the solution
%
%    exp(h(A M^{-1})^T) P exp(h A M^{-1}),
%
% to the equation
%
%    M^T \dot{P} M = A^T P M + M^T PA + Q,
%
% where  P = L0 D0 L0^T and IQL*IQD*IQL^T should be a low-rank factorization of
% \int_{0}^{h}{ exp(s(A M^{-1})^T)M^{-T} Q M^{-1} exp(s A M^{-1}) ds}

%
% Copyright 2018 Tony Stillfjord
%
% This file is part of DREsplit.
%
% DREsplit is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% DREsplit is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with DREsplit.  If not, see <http://www.gnu.org/licenses/>.
%

    % The low-rank factor exp(h(A M^{-1})^T) L is the solution to
    %    M' \dot{x} = A'x, x(0) = L
    % at time t = h:
    L = [args.impODEobj.eval(h, L0), IQL];
    D = blkdiag(D0, IQD);

    [L, D] = column_compress_LDLT(L, D, args.CC_tol);

end

