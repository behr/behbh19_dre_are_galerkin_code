function s = outerfrobnormdiff_LDLT(L1, D1, L2, D2)
% Compute the Frobenius norm of L1 D1 L1' - L2 D2 L2'
% for symmetric D1, D2

%
% Copyright 2018 Tony Stillfjord
%
% This file is part of DREsplit.
%
% DREsplit is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% DREsplit is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with DREsplit.  If not, see <http://www.gnu.org/licenses/>.
%

    [L, D] = column_compress_LDLT([L1, L2], blkdiag(D1, -D2), eps);
    % Exploit the invariance under cyclic permutations property of the trace
    % to compute norm(L*D*L','fro')
    s = sqrt(trace((L'*L*D)^2));

    % Expensive version
%     s = norm(L1*D1*L1' - L2*D2*L2', 'fro');

end
