
%
% Copyright 2018 Tony Stillfjord
%
% This file is part of DREsplit.
%
% DREsplit is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% DREsplit is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with DREsplit.  If not, see <http://www.gnu.org/licenses/>.
%



% Note: this will probably take at least an hour to run due to testing
% several different solvers and many different (and small) time steps.
% Optimising the impODE method would help a lot.
clear all
% close('all')

%% Set up the problem
Nx = 50;
Nx2 = Nx^2;

% Finite-difference approximation of Laplacian on [0,1]^2
xx = linspace(0,1, Nx+2);
x = xx(2:end-1);
dx = 1/(Nx+1);
e = ones(Nx, 1);
A_1D = 1/dx^2*spdiags([e, -2*e, e], -1:1, Nx, Nx);

[X, Y] = meshgrid(x,x);
I_1D = eye(Nx, Nx);
A_2D = kron(A_1D, I_1D) + kron(I_1D, A_1D);

problem.A = A_2D;
problem.M = speye(Nx2, Nx2); % mass matrix = I in this case

% 3 inputs, control on the squares [j/4, j/4 + 1/8] x [j/4, j/4 + 1/8]
Nb = 3;
B = zeros(Nx2, Nb);
for j = 1:Nb
    B(:, j) = reshape((X > j/4) & (X < j/4 + 1/8) & (Y > j/4) & (Y < j/4 + 1/8), Nx2, 1);
end
problem.B = B;

% 1 output, average of all states
C = 1/Nx2 * ones(1, Nx2);
problem.C = C;

% LDLT-factorization of Q = C'C
problem.LQ = C';
problem.DQ = 1e0 * eye(size(C', 2));

% LDLT-factorization of initial condition P(0) = 0
L0 = zeros(Nx2, 1);
problem.L0 = L0;
problem.D0 = eye(size(L0, 2));

% R^{-1}, inverse of weighting factor for input in cost functional
% With R this small, the control has an impact. It also makes the quadratic
% term more significant.
problem.Rinv = 1e6*eye(size(problem.B, 2));

% Column compression tolerance
problem.CC_tol = Nx*eps;

% Options for computing e^{hA}x terms
% problem.impODEmethod = 'IRK';          % Use implicit Runge-Kutta, order 5
% problem.IRKorder = 5;
% problem.iterate_ehA = true;        % Subdivide computation of e^{hA}x into multiple steps hj = h/j, increase j until convergence
% problem.exp_tol = 1e-3;            % Tolerance for above procedure

% problem.impODEmethod = 'exact';    % Alternative: use expm

% Alternative: use block Krylov method
problem.impODEmethod = 'krylov';
problem.krylov_tol = 1e-10;

% Integration interval
problem.t0 = 0;
problem.tend = 0.1;

% % Adaptiveness options (for the adaptive additive splitting schemes)
% problem.controller = 'PI'; % PI, unoptimized parameters
% % problem.controller = 0;  % Deadbeat (simplest, most commonly used)
% problem.epus = true;       % Use error per unit step measure rather than error per step
% problem.reuse_integral_factors = false; % Use the updating algorithm for computing the low-rank-factorization of I_Q(h) when h changes. False: recompute it from scratch every time.

% Output options
problem.intermediates = false;     % Store intermediate steps
problem.print_step = true;         % Print number of total steps and steps taken
problem.print_output = false;      % Print extra info every step

%% Solver options
Nts = 2.^(1:8);  % Test all these numbers of time steps
solvers = {'additive_symmetric2', 'Strang', 'additive_symmetric4'};    % Test all these solvers
% solvers = {'Strang_LDLT'};
problem.timelimit = inf;   % Terminate computation for a given solver if the previous time step used this much time - for when total computation time is limited and we need to make sure output has been stored
problem.path = [pwd, '/'];         % Store results here

% What kind of errors to compute
problem.errortype = 'Relative';
% problem.errortype = 'Absolute';
problem.errnorm = 'final';         % Use only error at final time (i.e. t=0 in usual backward formulation)
% problem.errnorm = 'L2';          % L2 in time
problem.feedback_error = false;    % Use error in feedback matrix BR^{-1}P instead of P

% Low-rank approximation of reference solution - false means the approximation
% corresponding to the smallest time step and the last solver is used
Lref = false;
Dref = false;

% Clean up and summarize arguments
% delete([problem.cachepath, '*.mat'])
disp(problem)

%%
% This does all the computations
[Ls, Ds, times, hs, errs, hs_solvers, Nts_solvers] = convtest(problem, solvers, Nts, Lref, Dref);

% Notes:
% Errors are stored in the structure errs, access using errs.(solver),
% where solver is one of the strings in solvers
%
% hs_solvers and Nts_solvers are the step sizes and number of steps
% actually used. These are guaranteed to be the same as hs and Nts if
% timelimit = Inf but might differ if not

% The discrete version of the Hilbert-Schmidt norm for operators on
% functions in L^2 is the Frobenius norm scaled by sqrt(dx) (cf. root-mean-
% square norm for L2 functions). In the case of relative errors, the
% factors cancel.
if strcmp(problem.errortype, 'Absolute')
    for solver = solvers
        solver = solver{1};
        errs.(solver) = sqrt(dx) * errs.(solver);
    end
end

%% Plot the errors vs. step sizes

% If the errors level out, this is likely due to the column compression.
% The higher-order methods seem to require quite small time steps to
% exhibit their theoretical orders (as in this case, the 4th-order method
% is only order 2). Their errors are nevertheless smaller than their lower-
% order counterparts.
linewidth = 1.5;
markersize = 10;

fig_err = plot_order(solvers, hs_solvers, errs, 'Relative', linewidth, markersize);
hold on

p = 5; hplot = hs(1:end); order = 2;
loglog(hplot, hplot.^order * 2*errs.(solvers{1})(p) / hs(p)^order, '-k', 'LineWidth', linewidth)

p = 6; hplot = hs(4:end-1); order = 4;
loglog(hplot, hplot.^order * 2*errs.(solvers{3})(p) / hs(p)^order, '-.k', 'LineWidth', linewidth)

names = {'\textnormal{Symmetric order 2}', ...
         '\textnormal{Strang}', ...
         '\textnormal{Symmetric order 4}', ...
         '$O(h^2)$', ...
         '$O(h^4)$'};

leg_err = legend(names, 'location', 'southeast');
set(leg_err, 'Interpreter', 'LaTex', 'fontsize', 14)

%% Plot the errors vs. computation times

% This shows which method is most efficient on the current computer. It
% depends a lot on the CPU architecture, parallellization etc., but also on
% the problem solved.
fig_eff = plot_efficiency(solvers, times, errs, 'Relative', linewidth, markersize);

names = {'\textnormal{Symmetric order 2}', ...
        '\textnormal{Strang}', ...
        '\textnormal{Symmetric order 4}'};

leg_eff = legend(names, 'location', 'southwest');
set(leg_eff, 'Interpreter', 'LaTex', 'fontsize', 14)
