function J = gauss6_jacobian(h, M, A)

%
% Copyright 2018 Tony Stillfjord
%
% This file is part of DREsplit.
%
% DREsplit is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% DREsplit is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with DREsplit.  If not, see <http://www.gnu.org/licenses/>.
%

    hA = h*A;
    J = [M - 5/36*hA, -(2/9 - sqrt(15)/15)*hA, -(5/36 - sqrt(15)/30)*hA ;
         -(5/36 + sqrt(15)/24)*hA,    M - 2/9*hA, -(5/36 - sqrt(15)/24)*hA;
         -(5/36 + sqrt(15)/30)*hA, -(2/9 + sqrt(15)/15)*hA, M - 5/36*hA];

end
