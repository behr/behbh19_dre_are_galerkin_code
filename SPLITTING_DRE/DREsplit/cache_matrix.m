function matrix = cache_matrix(fun, path, h, M, A)
% Save/load matrix computed by function fun to/from file at path

%
% Copyright 2018 Tony Stillfjord
%
% This file is part of DREsplit.
%
% DREsplit is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% DREsplit is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with DREsplit.  If not, see <http://www.gnu.org/licenses/>.
%


    % Parallel problems if several processess write to same
    % file simultaneously - use one file per process
    task = getCurrentTask();
    if isempty(task) % not every call to impODE is from parallel code
        ID = 0;
    else
        ID = task.ID;
    end
    filename = [path, '_', num2str(h), '_', int2str(ID), '.mat'];

    if exist(filename, 'file')
        load(filename, 'matrix')
    else
        matrix = fun(h, M, A);
        save(filename, 'matrix')
    end

end
