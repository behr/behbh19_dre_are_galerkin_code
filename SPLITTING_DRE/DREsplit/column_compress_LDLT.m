function [Lr, Dr] = column_compress_LDLT(L, D, CC_tol)
% Column-compress LDL^T for symmetric D
% L \in \R^{n \times k}
% D \in \R^{k \times k}

%
% Copyright 2018 Tony Stillfjord
%
% This file is part of DREsplit.
%
% DREsplit is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% DREsplit is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with DREsplit.  If not, see <http://www.gnu.org/licenses/>.
%

    [Q, R, p] = qr(L, 0);

    pt = zeros(size(p)); % Compute inverse permutation pt
    pt(p) = 1:length(p);
    % => R*P'*D*P*R' = R(:,pt)*D*R(:,pt)'

    % Round-off errors might make the symmetric matrix R*P'*D*P*R'
    % (slightly) non-symmetric  which leads to potentially
    % complex-valued eigenvalues and a non-orthogonal eigenvector matrix.
    % (Even norm(X-X') = 1e-35 causes problems!) We therefore symmetrize
    % this (small) matrix first.
    X = R(:,pt)*D*R(:,pt)';
    X = 1/2*(X+X');

    % [V, E] = eig(X, 'vector'); % Not supported on C3SE cluster
    [V, E] = eig(full(X));
    E = diag(E);

    % Remove the superfluous vectors
    [Es, ind] = sort(abs(E), 'descend');
    tol = Es(1) * CC_tol; % Relative tolerance
    r = find(Es > tol);
    if length(r) < 1
        r = 0;
    else
        r = r(end);
    end

    Lr = Q*V(:,ind(1:r));
    Dr = diag(E(ind(1:r)));


    % (Very expensive, testing only) alternative, pure diagonalization
    % of full matrix:
    %
    % P = L*D*L';
    % P = 1/2*(P + P');
    % [V, D] = eig(P);
    % E = diag(D);
    %
    % [Es, ind] = sort(abs(E), 'descend');
    % r = find(Es > CC_tol);
    % if length(r) < 1
    %     r = 0;
    % else
    %     r = r(end);
    % end
    %
    % Lr = V(:,ind(1:r));
    % Dr = diag(E(ind(1:r)));

end
