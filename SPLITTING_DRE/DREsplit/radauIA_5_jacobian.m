function J = radauIA_5_jacobian(h, M, A)

%
% Copyright 2018 Tony Stillfjord
%
% This file is part of DREsplit.
%
% DREsplit is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% DREsplit is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with DREsplit.  If not, see <http://www.gnu.org/licenses/>.
%

    hA = h*A;
    J = [M - 1/9*hA, -(-1-sqrt(6))/18*hA, -(-1+sqrt(6))/18*hA ;
             -1/9*hA,    M - (88+7*sqrt(6))/360*hA, -(88-43*sqrt(6))/360*hA;
             -1/9*hA, -(88+43*sqrt(6))/360*hA, M - (88-7*sqrt(6))/360*hA];
end
