function [t, Ls, Ds, ms, time, timeIQ] = additive_symmetric(s, args)
% Approximate the solution to
%
% \dot(P) = A'P + PA + Q - PB Rinv B'P  , P(0) = P0,
%
% by taking Nt time steps of size h with the 2s-order additive symmetric
% splitting scheme. See additive_symmetric_step for definition.
%
% Here,
% P0 = L0 D0 L0' and Q = IQL IQD IQL'
%
% All inputs submitted in argument structure args.
%
% Outputs: t, time steps
%          Ls, low-rank factors L at time steps (if args.intermediates)
%          otherwise only at final time
%          Ds, as above, but for D in LDL^T
%          ms, as above, but for the ranks of L
%          time, estimated total elapsed computation time, excluding
%                unpacking and trivial setup
%          timeIQ, estimated computation time for evaluating the
%                  integral(s) IQ(h/j)

%
% Copyright 2018 Tony Stillfjord
%
% This file is part of DREsplit.
%
% DREsplit is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% DREsplit is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with DREsplit.  If not, see <http://www.gnu.org/licenses/>.
%

    % Unpack most of args for clarity
    A = args.A;  B = args.B; Rinv = args.Rinv;  M = args.M;
    LQ = args.LQ; DQ = args.DQ; L0 = args.L0; D0 = args.D0;
    t0 = args.t0;  tend = args.tend;  Nt = args.Nt;
    intermediates = args.intermediates; print_step = args.print_step;

    h = (tend-t0) / Nt;
    t = linspace(t0, tend, Nt+1);

    % Method coefficients
    switch s
        case 1
            gamma = [1/2];
        case 2
            gamma = [-1/6, 2/3];
        case 3
            gamma = [1/48; -8/15; 81/80];
        case 4
            gamma = [-1/720; 8/45; -729/560; 512/315];
    end

    hfactors = 1./(1:s);

    ms = zeros(Nt, 1);
    ms(1) = size(L0,2);

    if intermediates
        Ls{Nt+1} = [];
        Ls{1} = L0;
        Ds{Nt+1} = [];
        Ds{1} = D0;
    else
        Ls = L0;
        Ds = D0;
    end

    args.impODEobj = impODEobj(args, true);

    telapsed = tic;

    % Using 29th-order Gauss quadrature to approximate the integral IQ(h)
    % (nuke it from orbit, just to be sure)
    % Probably possible to use much less now, the problem seems to have
    % been inaccurate approximations of e^{h*A}L

    if args.print_output
        fprintf('Computing low-rank factorization of integral I_Q. \n');
    end
    timeIQ = tic;
    [IQL, IQD] = compute_integral_factors_LDLT(LQ, DQ, h, M, A, hfactors, args, 29, true);
    timeIQ = toc(timeIQ);

    if args.print_output
        fprintf('Starting time-stepping. \n');
    end
    for j = 1:Nt
        if print_step
            fprintf('Step: %d of %d. (Additive symmetric order %d) \n', j, Nt, 2*s);
        end
        if intermediates
            [Ls{j+1}, Ds{j+1}] = additive_symmetric_step(s, gamma, h, M, A, B, IQL, IQD, Ls{j}, Ds{j}, Rinv, args);
            ms(j+1) = size(Ls{j+1},2);
        else
            [Ls, Ds] = additive_symmetric_step(s, gamma, h, M, A, B, IQL, IQD, Ls, Ds, Rinv, args);
            ms(j+1) = size(Ls,2);
        end
        if args.print_output
            fprintf('Size of solution low-rank factor: %d \n', ms(j+1));
        end
    end
    time = toc(telapsed);

end
