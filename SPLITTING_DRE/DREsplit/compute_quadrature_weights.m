function weights = compute_quadrature_weights(h, nodes)
% Compute the weights for a quadrature formula on [0,h] defined by the
% given nodes in the interval. If length(h) == 2 then use the interval
% [h(1) h(2)] instead.

%
% Copyright 2018 Tony Stillfjord
%
% This file is part of DREsplit.
%
% DREsplit is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% DREsplit is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with DREsplit.  If not, see <http://www.gnu.org/licenses/>.
%

    C = fliplr(vander(nodes))';
    if length(h) > 1
        b = h(2).^(1:length(nodes))' ./ (1:length(nodes))' - h(1).^(1:length(nodes))' ./ (1:length(nodes))';
    else
        b = h.^(1:length(nodes))' ./ (1:length(nodes))';
    end
%     weights = C\b;
    weights = vandersolve(C,b); % Better for high number of nodes (ill-conditioned system)
end
