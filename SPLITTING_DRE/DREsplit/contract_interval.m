function [phats, new_points] = contract_interval(ps, hnew)

%
% Copyright 2018 Tony Stillfjord
%
% This file is part of DREsplit.
%
% DREsplit is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% DREsplit is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with DREsplit.  If not, see <http://www.gnu.org/licenses/>.
%

    jt = find(ps > hnew, 1) - 1; % ps(jt) < hnew but ps(jt+1) > hnew
    if isempty(jt) % do nothing
        phats = ps;
        new_points = [];
        return
    end

    nt = length(ps) - jt; % number of elements in ps that are larger than hnew

    phats = ps(1:jt);
    new_points = [];

    % Redistribute the nt superflous nodes
    for l = 1:nt
        % Find the largest gap
        d = [phats(1); diff([phats; hnew])];
        [~, ind] = max(d);

        if ind == 1 % leftmost gap
            pnew = phats(1)/2;
        elseif ind == jt+l % rightmost gap
            pnew = (hnew + phats(jt+l-1))/2;
        else % a gap between two points
            pnew = (phats(ind) + phats(ind-1))/2;
        end
        new_points = [new_points; pnew];
        phats = [phats; pnew];
        phats = sort(phats);

    end

end

