function [ILas, IDas, xj, Las] = recompute_integral_factors_LDLT(LQ, DQ, M, A, as, xj, Las, h, hnew, args)

%
% Copyright 2018 Tony Stillfjord
%
% This file is part of DREsplit.
%
% DREsplit is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% DREsplit is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with DREsplit.  If not, see <http://www.gnu.org/licenses/>.
%

    order = length(xj)+1;
    N = size(LQ,1);
    m = size(LQ,2);

    % exp(s(A M^{-1})^T)M^{-T} LQ is the solution to
    %     M' \dot{x} + A'x = 0, x(0) = LQ
    % at t = s. Call it f(s):
    f = @(s) args.impODEobj.eval(s, M'\LQ);

    if hnew <= h*0.8 || hnew >= 1.25*h || hnew == h
        % Recompute everything
        [ILas, IDas, xj, Las] = compute_integral_factors_LDLT(LQ, DQ, hnew, M, A, as, args, order, false);

    elseif hnew > h
        [xj, oldind] = expand_interval(xj, hnew);
        weights = compute_quadrature_weights(hnew, xj);

        Las_new = {};
        for k = 1:length(as)
            Ls = {};
            if length(oldind) == order-1 % the new point was not used
                Ls = Las{k};
            else
                for j = 1:length(xj)-1
                    Ls{j} = Las{k}{oldind(j)};
                end
                Ls{end+1} = f(as(k)*xj(end));
            end
            ILas{k} = cell2mat(Ls);
            Las_new{k} = Ls;
            IDas{k} = kron(diag(weights * as(k)), DQ);
        end
        Las = Las_new;

    elseif hnew < h
        [xj, new_points] = contract_interval(xj, hnew);
        weights = compute_quadrature_weights(hnew, xj);

        Las_new = {};
        for k = 1:length(as)
            Ls = {};
            r = 1;
            for j = 1:length(xj)
                if find(new_points == xj(j)) % This is a new point, recompute
                    Ls{j} = f(as(k)*xj(j));
                else % Old point, already computed
                    Ls{j} = Las{k}{r};
                    r = r + 1;
                end
            end
            ILas{k} = cell2mat(Ls);
            Las_new{k} = Ls;
            IDas{k} = kron(diag(weights * as(k)), DQ);
        end
        Las = Las_new;


    end

end
