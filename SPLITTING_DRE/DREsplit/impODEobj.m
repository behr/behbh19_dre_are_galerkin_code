classdef impODEobj < handle

    %
    % Copyright 2018 Tony Stillfjord
    %
    % This file is part of DREsplit.
    %
    % DREsplit is free software: you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation, either version 3 of the License, or
    % (at your option) any later version.
    %
    % DREsplit is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with DREsplit.  If not, see <http://www.gnu.org/licenses/>.
    %

    properties
        transpose
        M
        A
        fun
        keys
        data
        IRKdata % data for the IRK methods
        Krylovdata % data for the (block) Krylov method
    end
    methods
        function obj = impODEobj(args, transpose)

            if nargin < 2
                obj.transpose = false;
            else
                obj.transpose = transpose;
            end

            if obj.transpose
                obj.M = args.M';
                obj.A = args.A';
            else
                obj.M = args.M;
                obj.A = args.A;
            end

            if ~isfield(args, 'impODEmethod')
                method = 'IRK';
            else
                method = args.impODEmethod;
            end

            obj.keys = [];
            obj.data = {};

            switch method
                % IRK
                case 'IRK'
                    obj.fun = @obj.IRK;

                    if ~isfield(args, 'IRKorder')
                        obj.IRKdata.order = 5;
                    else
                        obj.IRKdata.order = args.IRKorder;
                    end

                    if ~isfield(args, 'iterate_ehA')
                        obj.IRKdata.iterate = false;
                    else
                        obj.IRKdata.iterate = args.iterate_ehA;
                    end

                    if obj.IRKdata.iterate
                        if ~isfield(args, 'exp_tol')
                            obj.IRKdata.exp_tol = 1e-6;
                        else
                            obj.IRKdata.exp_tol = args.exp_tol;
                        end
                    end

                    if ~isfield(args, 'disk_cache')
                        obj.IRKdata.disk_cache = false;
                    else
                        obj.IRKdata.disk_cache = args.disk_cache;
                    end

                    if obj.IRKdata.disk_cache
                        if ~isfield(args, 'cachepath')
                            obj.IRKdata.cachepath = './';
                        else
                            obj.IRKdata.cachepath = args.cachepath;
                        end
                    end

                    switch obj.IRKdata.order
                        case 8
                            obj.IRKdata.fun = @impODE_GaussLegendre8; % TODO: coefficients got lost?
                            obj.IRKdata.jacfun = @gauss8_jacobian;
                        case 6
                            obj.IRKdata.fun = @impODE_GaussLegendre6;
                            obj.IRKdata.jacfun = @gauss6_jacobian;
                        case 5
                            obj.IRKdata.fun = @impODE_RadauIA_5;
                            obj.IRKdata.jacfun = @radauIA_5_jacobian;
                        case 4
                            obj.IRKdata.fun = @impODE_GaussLegendre4;
                            obj.IRKdata.jacfun = @gauss4_jacobian;
                        case 3
                            obj.IRKdata.fun = @impODE_RadauIIA_3;
                            obj.IRKdata.jacfun = @radauIIA_3_jacobian;
                        case 2
                            obj.IRKdata.fun = @impODE_ImplicitMidpoint;
                            obj.IRKdata.jacfun = @IM_jacobian;
                        case 1
                            obj.IRKdata.fun = @impODE_ImplicitEuler;
                            obj.IRKdata.jacfun = @IE_jacobian;
                    end

                case 'FFT' % TODO: not yet implemented, needs reformulation of problem
                    obj.fun = @obj.fft;

                case 'exact'
                    obj.fun = @obj.exp;

                case {'Krylov', 'krylov'} % TODO: somewhat experimental
                    obj.fun = @obj.Krylov;
                    if ~isfield(args, 'krylov_tol')
                        obj.Krylovdata.tol = 1e-6;
                    else
                        obj.Krylovdata.tol = args.krylov_tol;
                    end
                    obj.Krylovdata.Afun = true;

                case 'Leja' % TODO: not yet functional
                    obj.fun = @obj.Leja;

                case 'matlab_solver'
                    obj.fun = @obj.matlab_solver;

            end

        end

        function y = eval(obj, h, z)
            y = obj.fun(h, z);
        end


        function y = IRK(obj, h, z)

            if ~obj.IRKdata.iterate
                k = find(obj.keys == h); % check whether we computed this matrix previously
                if isempty(k) % we didn't
                    obj.keys(end+1) = h;
                    if obj.IRKdata.disk_cache
                        J = cache_matrix(jacfun, obj.IRKdata.cachepath, h, obj.M, obj.A);
                    else
                        J = obj.IRKdata.jacfun(h, obj.M, obj.A);
                        obj.data{end+1} = J;
                    end
                    y = obj.IRKdata.fun(h, obj.M, obj.A, z, J);
                else
                    y = obj.IRKdata.fun(h, obj.M, obj.A, z, obj.data{k});
                end
            else
                ynew = z;
                k = 1;
                rerr = inf;
                while rerr > obj.IRKdata.exp_tol  % take k steps of size h/k with the method
                    y = ynew;
                    ynew = z;
                    hh = h/k;
                    % New J matrix
                    if obj.IRKdata.disk_cache
                        J = cache_matrix(obj.IRKdata.jacfun, obj.IRKdata.cachepath, hh, obj.M, obj.A);
                    else
                        i = find(obj.keys == hh);
                        if isempty(i)
                            J = obj.IRKdata.jacfun(hh, obj.M, obj.A);
                            obj.keys(end+1) = hh;
                            obj.data{end+1} = J;
                        else
                            J = obj.data{i};
                        end
                    end

                    for j = 1:k
                        ynew = obj.IRKdata.fun(hh, obj.M, obj.A, ynew, J);
                    end
                    rerr = norm(ynew-y, 'fro') / norm(ynew, 'fro')
                    k = k*2;
                end
                y = ynew;
            end

        end

        function y = exp(obj, h, z)
            k = find(obj.keys == h); % check whether we computed this exponential previously
            if isempty(k) % we didn't
                ehA = expm(h*(obj.M\obj.A));
                obj.keys(end+1) = h;
                obj.data{end+1} = ehA;
                y = ehA*z;
            else
                y = obj.data{k}*z;
            end

            return
        end

        function y = FFT(obj, h, z)
            % Needs some reformulation of the problem
            N = (size(z, 1) - 1) / 2;
            shifted = fftshift(fft(z));
            for j = 1:size(shifted, 2)
                shifted(:,j) = exp(-h*((-N:N)' * 2*pi).^2) .* shifted(:,j);
            end
            y = ifft(ifftshift(shifted));
            return
        end

        function y = Krylov(obj, h, z)
            % Note the minus sign in the A-function... Important, as the
            % Krylov method is set up to compute expm(-A)*X
            [y, converged, errest, kmax, subspace] = expm_block_arnoldi(@(x) -h*(obj.M\(obj.A*x)), z, obj.Krylovdata.tol, obj.Krylovdata);

            if ~converged
                fprintf('Krylov iteration did not converge!\n')
            end
        end

        function y = Leja(obj, h, z)
        end


        function y = matlab_solver(obj, h, z)

            f = @(t, T) obj.A*T;
            jac = @(t,y) obj.A;
            %             opts = odeset('AbsTol', 1e-2, 'RelTol', 1e-4, 'Mass', obj.M, 'OutputFcn', @odeprint, 'OutputSel', 1, 'Jacobian', jac);
            opts = odeset('AbsTol', 1e-2, 'RelTol', 1e-4, 'Mass', obj.M, 'MassSingular', false, 'Jacobian', jac);
            y = zeros(size(z));
            for k = 1:size(z, 2)
                [t, yc] = ode15s(f, [0, h], z(:,k), opts);
                y(:,k) = yc(end,:)';
                fprintf('%d of %d done\n', k, size(z, 2))
            end
            fprintf('Done\n')
        end
    end % methods

end % classdef
