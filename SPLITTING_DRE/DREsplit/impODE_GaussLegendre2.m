function y = impODE_GaussLegendre2(h, M, A, z)
% Approximate the solution to
%    M \dot{x} = Ax, x(0) = z
% at t = h by the Gauss-Legendre 2nd order method (midpoint rule).

%
% Copyright 2018 Tony Stillfjord
%
% This file is part of DREsplit.
%
% DREsplit is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% DREsplit is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with DREsplit.  If not, see <http://www.gnu.org/licenses/>.
%

    J = M - h/2*A;

    k1 = J\(A*z);

    y = z + h*k1;

end
