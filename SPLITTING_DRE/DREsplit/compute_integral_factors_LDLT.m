function [ILas, IDas, xj, Las] = compute_integral_factors_LDLT(LQ, DQ, h, M, A, as, args, order, gauss)
% Compute the low-rank factors LDL^T of the integrals
%
% \int_{0}^{a*h}{ exp(s(A M^{-1})^T)M^{-T} LQ DQ LQ^T M^{-1} exp(s A M^{-1}) ds}.
%
% for all the values of a in the vector as.
%
% Uses a quadrature rule of order 'order', default 29.
% If 'gauss' is true (default), it is Gauss quadrature with n points giving order 2n+1,
% otherwise it uses equidistant points and computes the weights itself; n+1 points gives order n.
%
% Output: ILas, IDas such that ILas{j}IDas{j}ILas{j}' approximates the integral from 0 to as(j)h
%         also returns the quadrature points xj and the list of L-factors Las{j} that are used to form ILas{j}

%
% Copyright 2018 Tony Stillfjord
%
% This file is part of DREsplit.
%
% DREsplit is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% DREsplit is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with DREsplit.  If not, see <http://www.gnu.org/licenses/>.
%

    if nargin < 9
        gauss = true;
    end

    if nargin < 8
        order = 29; % Rather excessive... ?
%         order = 9;
    end

    N = size(LQ,1);
    m = size(LQ,2);

    % exp(s(A M^{-1})^T)M^{-T} L is the solution to
    %     M' \dot{x} + A'x = 0, x(0) = M^{-T}L
    % at t = h. Call it f:
    f = @(s, L) args.impODEobj.eval(s, M'\L);

    if gauss
        [xj, weights] = exact_quadrature_parameters(h, order);
    else % Equidistant nodes - used for the adaptive methods
        xj = linspace(0, h, order+1)';
        weights = compute_quadrature_weights(h, xj);
    end
    % Sort the nodes to allow for iterative computation
    [xj, ind] = sort(xj, 'ascend');
    weights = weights(ind);

    ILas = {};
    Las = {}; % Ls for each a
    IDas = {};

    for k = 1:length(as)
        Ls = {};
        Ls{1} = f(as(k)*xj(1), LQ);
        for j = 2:length(xj)
            dx = as(k)*xj(j) - as(k)*xj(j-1);
            Ls{j} = f(dx, M'*Ls{j-1});
        end
        Las{k} = Ls;
        ILas{k} = cell2mat(Ls);
        IDas{k} = kron(diag(weights * as(k)), DQ); % Block diagonal with weight(j)*as(k)*DQ as blocks
    end

    % Column compress it
    for k = 1:length(as)
        [ILas{k}, IDas{k}] = column_compress_LDLT(ILas{k}, IDas{k}, args.CC_tol);
    end
end
