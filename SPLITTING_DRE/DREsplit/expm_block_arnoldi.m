function [ekAB, converged, errest, kmax, subspace] = expm_block_arnoldi(A, B, tol, args)
% Input:    A:  n x n matrix
%           B : n x p matrix
%           tol: error tolerance
%
%           args.subspace: for restarting,
%               using the previously generated subspace
%
%           args.Afun: set to true if A is a function rather than
%               matrix, e.g. for evaluating (E\A)*x efficiently
%
%           args.kabsmax: absolute maximum number of columns k in an
%               an (n,k) dense matrix that can be allocated with the
%               available memory (the algorithm will return an error
%               rather  than attemp to exceed this).
%
% Computes a block Krylov subspace Vk, Hk, where Vk = [U1, U2, ..., Uk]
% with Uj being n x p matrices. Vk and Hk satisfy the Krylov
% orthogonality condition A*Vk = Vk*Hk + U_{k+1}*H_{k+1, k}*Ek',
% with Ek being the last p columns of the kp x kp identity matrix.
% Hk is a matrix of size kp x kp, with H_{i,j} being a p x p block.
%
% The action of the matrix exponential on B, expm(A)*B, is then
% approximated by
%
%   e_k(A)B = Vk*expm(-Hk)*Vk'*B
%
% and the error norm(e_k(A)B - expm(A)*B) estimated by calculating
% three extra small matrix exponentials expm(tj * Hk).
% A heuristic strategy of increasing the size of the Krylov subspace is
% employed, and the error estimate is not computed for each size k.
%
% Copyright 2018 Tony Stillfjord
%
% This file is part of DREsplit.
%
% DREsplit is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% DREsplit is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with DREsplit.  If not, see <http://www.gnu.org/licenses/>.

    [n, p] = size(B);
    normB = norm(B);

    if ~isfield(args, 'kabsmax')
        mmem = 8*4*1024^3; % 4 GB
        kabsmax = min(mmem / n, sqrt(mmem));
    else
        kabsmax = args.kabsmax;
    end

    if ~isfield(args, 'Afun')
        Afun = false;
    else
        Afun = args.Afun;
    end


    % It feels like we should be able to reuse lots of data from
    % a previous time step if the solution does not change
    % radically. Does not seem to work well in practice, however...
    % Subject to further investigation and therefore currently disabled.
%     if ~isfield(args, 'recycle')
%         recycle = false;
%     else
%         recycle = args.recycle;
%     end
    recycle = false;


    if ~isfield(args, 'subspace')
        kmin = 1;
        kmax = 2;

        [Vk, R] = qr(B, 0); % also, here Vk = U1
        Hk = [];
    else
        Vk = args.subspace.Vk;
        Hk = args.subspace.Hk;
        Ukp1 = args.subspace.Ukp1;
        Hkp1 = args.subspace.Hkp1;
        R = args.subspace.R;

        k = size(Vk, 2) / p;
        kmin = k + 1;
        kmax = kmin + 5;

        if ~recycle
            Vk(:, k*p+1:(k+1)*p) = Ukp1;
            Hk(k*p+1:(k+1)*p, (k-1)*p+1:k*p) = Hkp1;
        else
            % See above.
        end
    end

    if p*kmax > n
        kmax = floor(n/p);
    end

    converged = false;
    while ~converged

        for k = kmin:kmax
            Uk = Vk(:, (k-1)*p+1:k*p);
            if Afun
                Wk = A(Uk);
            else
                Wk = A*Uk;
            end

            for i = 1:k % Orthogonalize
                Ui = Vk(:, (i-1)*p+1:i*p);
                Hik = Ui'*Wk;
                Wk = Wk - Ui*Hik;
                Hk((i-1)*p+1:i*p, (k-1)*p+1:k*p) = Hik;
            end

            [Ukp1, Hkp1] = qr(Wk,0);

            if k == kmax
                % Compute error estimate
                E1 = eye(k*p,k*p); E1 = E1(:, 1:p);
                Hkt = [-Hk, E1; zeros(p, (k+1)*p)];

                eHt = expm(Hkt);
                phiHkE1 = eHt(1:k*p, k*p+1:(k+1)*p);

                errest = normB*norm(Hkp1)*norm(phiHkE1((k-1)*p+1:k*p, :));

                if errest < tol
                    converged = true;
%                     eHk = expm(-Hk);
%                     ekAB =  Vk*expm(-Hk)*(Vk'*B);
                    eHk = eHt(1:k*p, 1:k*p);
                    ekAB = Vk*(eHk(:, 1:p)*R);
                    subspace.Vk = Vk;
                    subspace.Hk = Hk;
                    subspace.Ukp1 = Ukp1;
                    subspace.Hkp1 = Hkp1;
                    subspace.R = R;
                    return
                end
            end

            Vk(:, k*p+1:(k+1)*p) = Ukp1;
            Hk(k*p+1:(k+1)*p, (k-1)*p+1:k*p) = Hkp1;

        end

        kmin = kmax + 1;
        kmax = kmax + 3; % Add two blocks in every step

        if kmax > kabsmax
            ekAB = -1; % TODO: improve error handling
            kmax = kmax - 2; % reset
            subspace.Vk = Vk;
            subspace.Hk = Hk;
            subspace.Ukp1 = Ukp1;
            subspace.Hkp1 = Hkp1;
            subspace.R = R;
            return
        end

    end

end

