function J = gauss8_jacobian(h, M, A)

%
% Copyright 2018 Tony Stillfjord
%
% This file is part of DREsplit.
%
% DREsplit is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% DREsplit is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with DREsplit.  If not, see <http://www.gnu.org/licenses/>.
%

    w1 = 1/8 - sqrt(30)/144;
    w1p = 1/8 + sqrt(30)/144;
    w2 = 1/2*sqrt((15 + 2*sqrt(30))/35);
    w2p = 1/2*sqrt((15 - 2*sqrt(30))/35);
    w3 = w2*(1/6 + sqrt(30)/24);
    w3p = w2p*(1/6 - sqrt(30)/24);
    w4 = w2*(1/21 + 5*sqrt(30)/168);
    w4p = w2p*(1/21 - 5*sqrt(30)/168);
    w5 = w2 - 2*w3;
    w5p = w2p - 2*w3p;

    hA = h*A;
    J = [M - w1*hA,              -(w1p-w3+w4p)*hA,  -(w1p-w3-w4p)*hA,   -(w1-w5)*hA ;
           -(w1-w3p+w4)*hA,    M - w1p*hA,          -(w1p-w5p)*hA,      -(w1-w3p-w4)*hA;
           -(w1 + w3p + w4)*hA,  -(w1p+w5p)*hA,   M - w1p*hA,           -(w1+w3p-w4)*hA;
           -(w1+w5)*hA,          -(w1p+w3+w4p)*hA,  -(w1p+w3-w4p)*hA, M - w1*hA];

end
