function [L, D] = additive_asymmetric_step(s, gamma, h, M, A, B, LQ, DQ, L0, D0, Rinv, args)
% Do a step of size h with the s-order additive asymmetric splitting method
%
%       gamma(1) exp(hF)exp(hG)P0 )
%       gamma(2) (exp(h/2F)exp(h/2G))^2 P0 +
%         ... +
%       gamma(s) (exp(h/sF)exp(h/sG))^s P0
% where
%       P0 = L0*D0*L0'
%
% Here F(P) = M^{-T} A^T P + PAM^{-1} + M^{-T}QM^{-1} and
% G(P) = -P B Rinv B^T P.

% Further, IQL{j}*IQD{j}*IQL{j}^T should be a low-rank factorization of
% \int_{0}^{h/j}{ exp(s(A M^{-1})^T)M^{-T} Q M^{-1} exp(s A M^{-1}) ds}

%
% Copyright 2018 Tony Stillfjord
%
% This file is part of DREsplit.
%
% DREsplit is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% DREsplit is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with DREsplit.  If not, see <http://www.gnu.org/licenses/>.
%


    parfor j = 1:s
        L2{j} = L0; D2{j} = D0;

        for k = 1:j
            [L2{j}, D2{j}] = expF(h / j, M, A, LQ{j}, DQ{j}, L2{j}, D2{j}, args);
            [L2{j}, D2{j}] = expG(h / j, B, Rinv, L2{j}, D2{j});
        end

    end

    % Stack the results
    L = cell2mat(L2);

    for j = 1:s
        D2{j} = gamma(j)*D2{j};
    end
    D = blkdiag(D2{:});

    [L, D] = column_compress_LDLT(L, D, args.CC_tol);

end
