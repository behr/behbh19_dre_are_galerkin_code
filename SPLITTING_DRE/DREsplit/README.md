DREsplit - splitting schemes for differential Riccati equations
===============================================================
Copyright 2018 Tony Stillfjord

DREsplit is licensed under GPLv3 or later, see the file COPYING for details.

Contact: tony.stillfjord@gmail.com


## Description
This package contains code for approximating the solutions to matrix
differential Riccati equations (DREs). It is still under active 
development, and no guarantees are provided as to correctness, 
completeness or backwards compatibility. See the bottom of this file 
for references describing the implemented algorithms.

## Installation
To install, simply download or clone the project and add the resulting 
folder to MATLAB's path.

## Example usage
The file example.m demonstrates how to provide the code with the parameters
of a DRE (e.g. matrices A, B, C, etc.) and approximate its solution.

The file example_error_framework.m additionally demonstrates how to use the
accompanying code for comparing different solvers. This considers the same 
problem for different time steps and different solvers and may compute
different kinds of errors. It also keeps track of the computation times, 
allowing for efficiency plots.

## Available solvers
Currently the following solvers are directly available:
Lie splitting (1st-order)
Strang splitting (2nd-order)
Asymmetric additive splitting of orders 2, 3, 4
Symmetric additive splitting of orders 2, 4, 6, 8
Adaptive symmetric additive splitting of orders 2, 4, 6, 8

Additive schemes of other orders may easily be added by adding the relevant
coefficients to the file additive_(a)symmetric.m and optionally creating a 
convenience function like additive_symmetric4.m. See the file
additive_splitting_order_conditions.m for how to compute the coefficients.

Adaptive time-stepping may be used for the symmetric additive schemes, see
additive_symmetric_adaptive.m.


## Note on parallelization:
The code uses parfor statements in some critical loops, mostly useful in 
the additive schemes. For small problems, the extra overhead of this 
parallelization may dominate the computation cost. To avoid this, make 
sure that MATLAB does not start a parallel pool automatically, and set 
one up manually if necessary.


## References:

Stillfjord, T., Low-rank second-order splitting of large-scale differential
Riccati equations, IEEE Trans. Automat. Control 60(10) (2015), 
pp. 2791-2796.

Stillfjord, T., Adaptive high-order splitting schemes for large-scale 
differential Riccati equations, Numer Algor (2017), https://doi.org/10.1007/s11075-017-0416-8
