function y = impODE(h, M, A, z, args)
% Approximate the solution to
%
%    M \dot{x} = Ax, x(0) = z,
%
% at t = h.
% Default method is one step of an implicit Runge-Kutta method of order 5.
% If args.iterate_ehA is set, h is divided into substeps h/j, where j is
% increased (doubled) until two approximations differ (relatively) less
% than args.exp_tol (default 1e-6)

%
% Copyright 2018 Tony Stillfjord
%
% This file is part of DREsplit.
%
% DREsplit is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% DREsplit is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with DREsplit.  If not, see <http://www.gnu.org/licenses/>.
%

    if ~isfield(args, 'impODEmethod')
        method = 5;
    else
        method = args.impODEmethod;
    end

    if ~isfield(args, 'iterate_ehA')
        iterate = false;
    else
        iterate = args.iterate_ehA;
    end

    if ~isfield(args, 'exp_tol')
        exp_tol = 1e-6;
    else
        exp_tol = args.exp_tol;
    end

    jacfun = 0;

    switch method
        % IRK cases handled below
        case 8
            fun = @impODE_GaussLegendre8;
            jacfun = @gauss8_jacobian;
        case 6
            fun = @impODE_GaussLegendre6;
            jacfun = @gauss6_jacobian;
        case 5
            fun = @impODE_RadauIA_5;
            jacfun = @radauIA_5_jacobian;
        case 4
            fun = @impODE_GaussLegendre4;
            jacfun = @gauss4_jacobian;
        case 3
            fun = @impODE_RadauIIA_3;
            jacfun = @radauIIA_3_jacobian;
        case 2
            fun = @impODE_ImplicitMidpoint;
        case 1
            fun = @impODE_ImplicitEuler;

        % Non-IRK cases handled directly:

        % Needs some reformulation of the problem
        case 'FFT'
            N = (size(z, 1) - 1) / 2;
            shifted = fftshift(fft(z));
            for j = 1:size(shifted, 2)
                shifted(:,j) = exp(-h*((-N:N)' * 2*pi).^2) .* shifted(:,j);
            end
            y = ifft(ifftshift(shifted));
            return

        case 'exact'
            % Some caching for speed (better to do it in RAM, of course,
            % harder to set up):
            fun = @(h,M,A) expm(h*(M\A));
            ehA = cache_matrix(fun, [args.cachepath, '_ehA'], h, M, A);
            y = ehA*z;
            return

        % Not yet functional
        case 'Leja'
            assert(M(1,1) - 1 == 0) % stupid temporary check to make sure M = I
            [y, ~, ~, ~] = expleja(h, A, z, 1e-1);
            return

        % Not yet fully functional
        case 'Krylov'
            Afun = @(x) M\(A*x);
            [V_k,A_k] = arn_block_fun(z,Afun, -2 + floor(size(A, 1) / size(z, 2)), 1e-5 );
            fprintf('%d, %d, %d \n', size(V_k,2), size(z, 2), size(V_k, 2) / size(z,2))
            y = V_k*expm(h*A_k)*V_k'*z;

%             TOL = 1e-1;
%             y = zeros(size(x));
%             for i = 1:size(z,2)
%                 b = z(:,i);
%                 y(:,i) = phipm(h, Afun, b, TOL);
%             end
            return

    end

    % The IRK cases
    if ~iterate
        if isa(jacfun, 'function_handle') && isfield(args, 'cachepath')
            J = cache_matrix(jacfun, [args.cachepath, '_J'], h, M, A);
            y = fun(h, M, A, z, J);
        else
            y = fun(h, M, A, z);
        end
    else
        % Must have jacfun, otherwise this is way too expensive
        ynew = z;
        k = 1;
        rerr = inf;
        while rerr > exp_tol  % take k steps of size h/k with the method
            y = ynew;
            ynew = z;
            hh = h/k;
            J = cache_matrix(jacfun, args.cachepath, hh, M, A);
            for j = 1:k
                ynew = fun(hh, M, A, ynew, J);
            end
            rerr = norm(ynew-y, 'fro') / norm(ynew, 'fro');
            k = k*2;
        end
        y = ynew;
    end

end
