function fig = plot_order(solvers, hs, errs, errortype, linewidth, markersize)
% Plot errors against step sizes (order plot)
% errortype goes into y axis: "errortype error" (so typically 'Relative' or 'Absolute')

%
% Copyright 2018 Tony Stillfjord
%
% This file is part of DREsplit.
%
% DREsplit is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% DREsplit is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with DREsplit.  If not, see <http://www.gnu.org/licenses/>.
%

    if nargin < 6
        markersize = 10;
    end
    if nargin < 5
        linewidth = 1;
    end
    if nargin < 4
        errortype = 'Relative';
    end

    width = 10; % inches
    height = 6;

    fig = figure;
    set(fig, 'units', 'inches')
    set(fig, 'position', [2, 2, width, height]);
    markers = {'*','o','+','x','s','d','^','v','>','<','p','h'};
    for i = 1:length(solvers)
        solver = solvers{i};
        if isstruct(hs)
            h = hs.(solver);
        else
            h = hs;
        end

        loglog(h, errs.(solver), ['-', markers{i}], 'Color', 'k', 'LineWidth', linewidth, 'MarkerSize', markersize)
        hold on
    end
    xlabel('$h$', 'Interpreter', 'latex', 'Fontsize', 14)
    ylabel(['$\textnormal{' errortype, ' errors', '}$'], 'Interpreter', 'latex', 'Fontsize', 14)
    title('$\textnormal{Order plot}$', 'Interpreter', 'latex', 'Fontsize', 16)

    legend(solvers, 'location', 'best')
    grid on
    set(fig,'PaperPositionMode','auto')
    set(fig, 'PaperUnits','inches', 'PaperPosition', [0 0 width height]);
    set(fig, 'PaperUnits','inches', 'PaperSize', [width height]);
end
