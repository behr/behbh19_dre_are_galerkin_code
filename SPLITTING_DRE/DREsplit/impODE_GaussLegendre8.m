function y = impODE_GaussLegendre8(h, M, A, z, J)
% Approximate the solution to
%    M \dot{x} = Ax, x(0) = z
% at t = h by the Gauss-Legendre 8th order method.

%
% Copyright 2018 Tony Stillfjord
%
% This file is part of DREsplit.
%
% DREsplit is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% DREsplit is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with DREsplit.  If not, see <http://www.gnu.org/licenses/>.
%

    if nargin < 5
        J = gauss8_jacobian(h,M,A);
    end

    Az = A*z;

    ks = J\[Az; Az; Az; Az];

    k1 = ks(1 : size(z,1), :);
    k2 = ks(size(z,1)+1 : 2*size(z,1), :);
    k3 = ks(2*size(z,1)+1 : 3*size(z,1), :);
    k4 = ks(3*size(z,1)+1 : end, :);

    w1 = 1/8 - sqrt(30)/144;
    w1p = 1/8 + sqrt(30)/144;

    y = z + h*(2*w1*k1 + 2*w1p*k2 + 2*w1p*k3 + 2*w1*k4);

end
