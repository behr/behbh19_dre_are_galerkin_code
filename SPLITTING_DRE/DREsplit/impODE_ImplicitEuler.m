function y = impODE_ImplicitEuler(h, M, A, z, J)
% Approximate the solution to
%    M \dot{x} = Ax, x(0) = z
% at t = h by the 1st-order implicit Euler method.

%
% Copyright 2018 Tony Stillfjord
%
% This file is part of DREsplit.
%
% DREsplit is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% DREsplit is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with DREsplit.  If not, see <http://www.gnu.org/licenses/>.
%

    if nargin < 5
        J = IE_jacobian(h,M,A);
    end

    k = J\(A*z);

    y = z + h*k;

end
