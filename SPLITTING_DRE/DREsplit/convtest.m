function [Lss, Dss, times, hs, errs, hs_solvers, Ns_solvers] = convtest(args, solvers, Nts, Lref, Dref)
% Solve the problem defined in 'args' by all the solvers in 'solvers' and
% for all the number of time steps in 'Nts'.
% Use Lref*Dref*Lref' as the reference approximation unless Lref, Dref are
% false - in that case use the approximation corresponding to the last
% solver and the maximum number of time steps.

%
% Copyright 2018 Tony Stillfjord
%
% This file is part of DREsplit.
%
% DREsplit is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% DREsplit is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with DREsplit.  If not, see <http://www.gnu.org/licenses/>.
%

    hs = (args.tend - args.t0) ./ Nts;

    % Check inputdata
    if ~isfield(args, 'errnorm')
        args.errnorm = 'final';
    end
    if ~isfield(args, 'relerr')
        args.relerr = false;
    end
    if ~isfield(args, 'plotreflines')
        args.plotreflines = false;
    end
    if ~isfield(args, 'savefigures')
        args.savefigures = false;
    end

    if isfield(args, 'timelimit')
        timelimit = args.timelimit;
    else
        timelimit = inf;
    end

    if ~isfield(args, 'path')
        path = './';
    else
        path = args.path;
    end

    if ~isfield(args, 'cluster')
        CLUSTER = false;
    else
        CLUSTER = args.cluster;
    end

    if ~isfield(args, 'compute_error')
        compute_error = true;
    else
        compute_error = args.compute_error;
    end
    errs = [];

    if ~isfield(args, 'feedback_error')
        args.feedback_error = false;
    end

    if islogical(Lref) && (Lref == false)
        compute_reference = true;
    else
        compute_reference = false;
    end


    fprintf('Starting main solver loop. \n \n');

    if CLUSTER % Set up the parallel pool in the directory of the node
        fprintf('Starting parallel pool \n \n')
        tmpdir = getenv('TMPDIR');
        sched = parcluster('local');
        sched.JobStorageLocation = tmpdir;
        pool = parpool(sched, sched.NumWorkers);
    end

    % Main loops, solve with each solver and with each timestep size
    for solver = solvers
        solver = solver{1}; % I have no idea why this is needed, or why it works
        disp(solver)

        % Basic checkpointing - check if we already computed stuff for this
        % solver
        if exist([path, 'data_', solver, '.mat'], 'file')
            data = load([path, 'data_', solver, '.mat']);
            Ls = data.Ls; Ds = data.Ds;
            if isfield(data, 'ts')
                ts = data.ts;
            elseif isfield(data, 'times')
                ts = data.times;
            end
            if isfield(data, 'hs_solver')
                hs_solver = data.hs_solver;
            else
                hs_solver = hs;
            end
            if isfield(data, 'Ns_solver')
                Ns_solver = data.Ns_solver;
            else
                Ns_solver = Nts;
            end
%             load([path, 'data_', solver, '.mat'], 'Ls', 'Ds', 'ts', 'hs_solver', 'Ns_solver')
            fprintf('Read data from file for solver: %s \n', solver);
        else
            Ls = {};
            Ds = {};
            ts = [];
            hs_solver = hs;
            Ns_solver = Nts;

            if CLUSTER && (~pool.Connected) % Restart parallel pool if necessary
                fprintf('Restarting parallel pool \n \n')
                delete(pool)
                sched = parcluster('local');
                pool = parpool(sched, sched.NumWorkers);
            end

            for i = 1:length(hs)
                Nt = Nts(i);
                h = hs(i);
                args.h = h;
                args.Nt = Nt;
                % If we evaluate the error only at (e.g.) tend, we do not need
                % to store the intermediate values, unless requested
                if args.intermediates == false
                    if strcmp(args.errnorm, 'L2') || strcmp(args.errnorm, 'max')
                        args.intermediates = true;
                    end
                end

                fprintf('N: %d \n', Nt);

                [~, L, D, ~, time] = feval(str2func(solver), args);

                Ls{end+1} = L;
                Ds{end+1} = D;
                ts(i) = time;

                if time > timelimit
                    hs_solver = hs(1:i);
                    Ns_solver = Nts(1:i);
                    break
                end
            end
            save([path, 'data_', solver], 'Ls', 'Ds', 'ts', 'hs_solver', 'Ns_solver')
        end
        Lss.(solver) = Ls;
        Dss.(solver) = Ds;
        times.(solver) = ts;
        hs_solvers.(solver) = hs_solver;
        Ns_solvers.(solver) = Ns_solver;

    end

    if compute_error

        if compute_reference
            fprintf('Reference solution: using solver %s and step size h = %d \n \n', solvers{end}, hs(end));
            Ntref = Ns_solvers.(solvers{end})(end);
            href = (args.tend - args.t0) / Ntref;
            times.('ref') = times.(solvers{end});
            Lref = Lss.(solvers{end});
            Dref = Dss.(solvers{end});
            Lref = Lref{end};
            Dref = Dref{end};
        else
            fprintf('Reference solution: using provided solution \n \n');
            Ntref = size(Lref, 2) - 1; % zref should includes value for t = 0
            href = (args.tend - args.t0) / Ntref;
            times.('ref') = 0;
        end


        % Compute the norm of the reference solution if we need relative errors
        if strcmp(args.errortype, 'Relative')
            fprintf('Computing size of reference solution for relative error computations. \n \n');
            if strcmp(args.errnorm, 'L2') || strcmp(args.errnorm, 'max')
                Pref_norm = 0;
                for j = 1:Ntref
                    if strcmp(args.errnorm, 'L2')
                        Pref_norm = Pref_norm + (args.tend - args.t0) / Ntref * errornorm(Lref{j}, Dref{j})^2;
                    elseif strcmp(args.errnorm, 'max')
                        Pref_norm = max(Pref_norm, errornorm(Lref{j}, Dref{j}));
                    end
                end
            end
            if strcmp(args.errnorm, 'L2')
                Pref_norm = sqrt(Pref_norm);
            elseif strcmp(args.errnorm, 'final')
                if args.intermediates
                    Pref_norm = errornorm(Lref{end}, Dref{end});
                else
                    Pref_norm = errornorm(Lref, Dref);
                end
            end
            fprintf('... Done. \n \n');
        end


        fprintf('Computing errors. \n \n');

        for solver = solvers
            solver = solver{1};
            disp(solver)

            for i = 1:length(hs_solvers.(solver))
                Nt = Ns_solvers.(solver)(i);
                h = hs_solvers.(solver)(i);
                args.h = h;
                args.Nt = Nt;

                L = Lss.(solver){i};
                D = Dss.(solver){i};

                fprintf('N: %d \n', Nt);

                % Error computation
                errs.(solver)(i) = 0;
                if strcmp(args.errnorm, 'L2') || strcmp(args.errnorm, 'max')
                    if Ntref >= Nt % Evaluate the reference solution at every time corresponding to these approximations
                        for j = 2:Nt
                            Lref_tj = Lref{(j-1)*Ntref/Nt+1};
                            Dref_tj = Dref{(j-1)*Ntref/Nt+1};
                            errstep = errornorm_diff(L{j}, D{j}, Lref_tj, Dref_tj);
                            if strcmp(args.errnorm, 'L2')
                                errs.(solver)(i) = errs.(solver)(i) + h*errstep^2;
                            elseif strcmp(args.errnorm, 'max')
                                errs.(solver)(i) = max(errs.(solver)(i), errstep);
                            end
                        end
                    else
                        for j = 2:Ntref % Only use the time points corresponding to the reference solution and therefore also the href time step
                            Lref_tj = Lref{j};
                            Dref_tj = Dref{j};
                            Lj = L{(j-1)*Nt/Ntref+1};
                            Dj = D{(j-1)*Nt/Ntref+1};
                            errstep = errornorm_diff(Lj, Dj, Lref_tj, Dref_tj);
                            if strcmp(args.errnorm, 'L2')
                                errs.(solver)(i) = errs.(solver)(i) + href*errstep^2;
                            elseif strcmp(args.errnorm, 'max')
                                errs.(solver)(i) = max(errs.(solver)(i), errstep);
                            end
                        end
                    end

                    if strcmp(args.errnorm, 'L2')
                        errs.(solver)(i) = sqrt(errs.(solver)(i));
                    end
                elseif strcmp(args.errnorm, 'final')
                    if args.intermediates
                        errs.(solver)(i) = errornorm_diff(L{end}, D{end}, Lref{end}, Dref{end});
                    else
                        errs.(solver)(i) = errornorm_diff(L, D, Lref, Dref);
                    end
                end

                % Compute relative errors or not?
                if strcmp(args.errortype, 'Relative')
                    errs.(solver)(i) = errs.(solver)(i) / Pref_norm;
                end
            end
        end

        if compute_reference
            % This should be 0, but suffer from round-off error
            errs.(solvers{end})(end) = 0;
        end

        fprintf('... Done. \n \n');

    end

    function nrm = errornorm(L, D)
        if args.feedback_error
            nrm = norm(args.Rinv*(args.B'*L) * D * (L'*args.M), 'fro');
        else
            nrm = outerfrobnorm_LDLT(args.M'*L, D);
        end
    end

    function nrm = errornorm_diff(L1, D1, L2, D2)
        if args.feedback_error
            nrm = norm(args.Rinv*(args.B'*L1) * D1 * (L1'*args.M) - ...
                       args.Rinv*(args.B'*L2) * D2 * (L2'*args.M), 'fro');
        else
            nrm = outerfrobnormdiff_LDLT(args.M'*L1, D1, args.M'*L2, D2);
        end
    end

end
