function [t, Ls, Ds, ms, time, errests] = additive_symmetric_adaptive(s, args)
% Approximate the solution to
%
% \dot(P) = A'P + PA + Q - PB Rinv B'P  , P(0) = P0,
%
% by time-stepping with the 2s-order symmetric sum-splitting scheme.
% Use the embedded 2(s-1)-order method for adaptive time-stepping.
% (Or 1st-order if s = 1.)
% See additive_symmetric_step for definition.
%
% Here,
% P0 = L0 D0 L0' and Q = IQL IQD IQL'
%
% All inputs submitted in argument structure args.
%
% Outputs: t, time steps
%          Ls, low-rank factors L at time steps
%          Ds, as above, but for D in LDL^T
%          ms, as above, but for the ranks of L
%          time, estimated total elapsed computation time exclusive
%          unpacking and trivial setup
%          errests, error estimates at time steps

%
% Copyright 2018 Tony Stillfjord
%
% This file is part of DREsplit.
%
% DREsplit is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% DREsplit is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with DREsplit.  If not, see <http://www.gnu.org/licenses/>.
%


    % Unpack most of args for clarity
    A = args.A;  B = args.B; Rinv = args.Rinv;  M = args.M;
    L0 = args.L0; D0 = args.D0;
    LQ = args.LQ; DQ = args.DQ;
    tol = args.tol;
    t0 = args.t0;  tend = args.tend;  Nt = args.Nt;
    intermediates = args.intermediates;  print_step = args.print_step;

    if isfield(args, 'controller')
        controller = args.controller;
    else
        controller = 0;
    end

    if isfield(args, 'epus')
        EPUS = args.epus;
    else
        EPUS = false;
    end

    hj = (tend-t0) / Nt/100; % initial step size
    t = [t0];

    % Method coefficients for both actual method and embedded method
    switch s
        case 1
            gamma = [1/2];
            beta = [1];
        case 2
            gamma = [-1/6, 2/3];
            beta = [1/2];
        case 3
            gamma = [1/48; -8/15; 81/80];
            beta = [-1/6, 2/3];
        case 4
            gamma = [-1/720; 8/45; -729/560; 512/315];
            beta = [1/48; -8/15; 81/80];
    end

    hfactors = 1./(1:s);

    % Tolerance including 'safety factor'
    modtol = 0.9*tol;

    % Order of error estimator
    % Embedded method is order 2(s-1) so error estimate is order 2(s-1)+1 = 2s-1
    % this decreases by one if EPUS (error per unit step) is used rather
    % than EPS (error per step).
    % If s = 1 (method order 2) we have the Lie splitting as error estimate.
    if s > 1
        if EPUS
            errest_order = 2*s-2;
        else % EPS
            errest_order = 2*s-1;
        end

    else
        if EPUS
            errest_order = 1;
        else % EPS
            errest_order = 2;
        end
    end

    ms(1) = size(L0,2);
    errests = [];

    if intermediates
        Ls{1} = L0;
        Ds{1} = D0;
    else
        Ls = L0;
        Ds = D0;
    end

    args.impODEobj = impODEobj(args, true);

    telapsed = tic;

    IQ_quad_order = 9;
    errest = 0;
    reject = 0;
    j = 1;

    [~, ~, xj, IQLs] = compute_integral_factors_LDLT(LQ, DQ, hj, M, A, hfactors, args, IQ_quad_order + 1, false); % 9th-order quadrature on 9 points
    hj1 = hj; % h_{j-1}
    hj2 = hj; % h_{j-2}

    while t(end) + hj <= tend
        if print_step
            fprintf('Step: %d at time %d of %d. Step size: %d. Rank: %d. \n', j, t(end), tend, hj, ms(end));
        end

        if args.reuse_integral_factors
            if reject == 0 % if last step was rejected, IQ has already been recomputed
                [IQL, IQD, xj, IQLs] = recompute_integral_factors_LDLT(LQ, DQ, M, A, hfactors, xj, IQLs, hj1, hj, args);
            end
        else
            [IQL, IQD, xj, IQLs] = compute_integral_factors_LDLT(LQ, DQ, hj, M, A, hfactors, args, IQ_quad_order, true);
        end

        if intermediates
            [Ls{j+1}, Ds{j+1}, errest] = additive_symmetric_with_errest_step(s, gamma, beta, hj, M, A, B, IQL, IQD, Ls{j}, Ds{j}, Rinv, args);
        else
            [Ls, Ds, errest] = additive_symmetric_with_errest_step(s, gamma, beta, hj, M, A, B, IQL, IQD, Ls, Ds, Rinv, args);
        end

        if EPUS
            errest = errest / hj;
        end

        if errest > tol % redo step
            if args.reuse_integral_factors % try to recompute IQ first before changing the step size
                [IQL, IQD, xj, IQLs] = compute_integral_factors_LDLT(LQ, DQ, hj, M, A, hfactors, args, IQ_quad_order + 1, false); % 10th-order quadrature on 9 points
                reject = reject + 1;
                if reject > 1
                    hj = (modtol/errest)^(1/(errest_order)) * hj;
                end
            else
                hj = (modtol/errest)^(1/(errest_order)) * hj;
            end
        else
            reject = 0;
            t(end+1) = t(end) + hj;
            hj2 = hj1;
            hj1 = hj;

            % New step size
            if controller == 0
                hj = (modtol/errest)^(1/errest_order) * hj;
            elseif strcmp(controller, 'PI')
                kI = 0.2/errest_order;
                kP = 0.2/errest_order;
                if j == 1
                    hj = (modtol / errest)^(kI) * hj;
                else
                    hj = (modtol / errest)^(kI) * (errest_old / errest)^(kP) * hj;
                end
            elseif strcmp(controller, 'predictive')
                if j == 1
                    hj = (modtol / errest)^(1/2) * hj;
                else
                    hj = (modtol / errest)^(1/2) * (errest_old / errest)^(1/2) * hj1/hj2 * hj1;
                end
            end

            errest_old = errest;

            if intermediates
                ms(j+1) = size(Ls{j+1},2);
            else
                ms(j+1) = size(Ls,2);
            end
            errests(end+1) = errest;

            j = j+1;

            if abs(t(end) - tend) < eps % We are at the final time
                break
            end

            if t(end) + hj > tend % Ensure that we end up precisely at tend
                hj = tend-t(end);
            end
        end
    end

    if print_step
        fprintf('Step: %d at time %d. Step size %d, rank %d. \n', j, t(end), hj, ms(end));
    end

    time = toc(telapsed);

end
