function [L, D] = Lie_step(h, M, A, B, IQL, IQD, Rinv, L0, D0, args)
% Do a step of size h with the exponential Lie splitting method:
%
%      exp(hF) exp(hG) P
%
% where P = L0 D0 L0^T.
% Here F(P) = M^{-T} A^T P + PAM^{-1} + M^{-T}QM^{-1} and
% G(P) = -P B Rinv B^T P.

% Further, IQL*IQD*IQL^T should be a low-rank factorization of
% \int_{0}^{h}{ exp(s(A M^{-1})^T)M^{-T} Q M^{-1} exp(s A M^{-1}) ds}

%
% Copyright 2018 Tony Stillfjord
%
% This file is part of DREsplit.
%
% DREsplit is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% DREsplit is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with DREsplit.  If not, see <http://www.gnu.org/licenses/>.
%

[L, D] = expG(h, B, Rinv, L0, D0);

[L, D] = expF(h, M, A, IQL, IQD, L, D, args);

end
