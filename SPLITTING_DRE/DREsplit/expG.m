function [L, D] = expG(h, B, Rinv, L0, D0)
% Given G(P) = -P B Rinv B^T P, compute the low-rank factorization LDL^T of
%
%   exp(hG)P = (I + h P B Rinv B^T)^{-1}P
%
% where P = L0 D0 L0^T.
%

%
% Copyright 2018 Tony Stillfjord
%
% This file is part of DREsplit.
%
% DREsplit is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% DREsplit is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with DREsplit.  If not, see <http://www.gnu.org/licenses/>.
%

    I = eye(size(D0));
    L = L0;
    D = (I + h*D0*L0'*(B*(Rinv*(B'*L0))))\D0;

end
