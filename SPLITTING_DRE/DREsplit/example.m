% Copyright 2018 Tony Stillfjord
%
% This file is part of DREsplit.
%
% DREsplit is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% DREsplit is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with DREsplit.  If not, see <http://www.gnu.org/licenses/>.

% clear all
close('all')

%% Set up the problem
Nx = 50;
Nx2 = Nx^2;

% Finite-difference approximation of Laplacian on [0,1]^2
xx = linspace(0,1, Nx+2);
x = xx(2:end-1);
dx = 1/(Nx+1);
e = ones(Nx, 1);
A_1D = 1/dx^2*spdiags([e, -2*e, e], -1:1, Nx, Nx);

[X, Y] = meshgrid(x,x);
I_1D = eye(Nx, Nx);
A_2D = kron(A_1D, I_1D) + kron(I_1D, A_1D);

problem.A = A_2D;
problem.M = speye(Nx2, Nx2); % mass matrix = I in this case

% 3 inputs, control on the squares [j/4, j/4 + 1/8] x [j/4, j/4 + 1/8]
Nb = 3;
B = zeros(Nx2, Nb);
for j = 1:Nb
    B(:, j) = reshape((X > j/4) & (X < j/4 + 1/8) & (Y > j/4) & (Y < j/4 + 1/8), Nx2, 1);
end
problem.B = B;

% 1 output, average of all states
C = 1/Nx2 * ones(1, Nx2);
problem.C = C;

% LDLT-factorization of Q = C'C
problem.LQ = C';
problem.DQ = 1e0 * eye(size(C', 2));

% LDLT-factorization of initial condition P(0) = 0
L0 = zeros(Nx2, 1);
problem.L0 = L0;
problem.D0 = eye(size(L0, 2));

% R^{-1}, inverse of weighting factor for input in cost functional
% With R this small, the control has an impact. It also makes the quadratic
% term more significant.
problem.Rinv = 1e6*eye(size(problem.B, 2));

% Column compression tolerance
problem.CC_tol = Nx*eps;

% Options for computing e^{hA}x terms

% Alternative 1: Implicit Runge-Kutta method
% problem.impODEmethod = 'IRK';
% problem.IRKorder = 5;             % Method order = 5
% problem.iterate_ehA = true;       % Subdivide computation of e^{hA}x into multiple steps hj = h/j, increase j until convergence
% problem.exp_tol = 1e-3;           % Tolerance for above procedure
% % Store resusable matrices on disk rather than in RAM (for big problems)
% % problem.disk_cache = true;
% % problem.cachepath = '.';

% Alternative 2: use expm  (only for small problems)
% problem.impODEmethod = 'exact';

% Alternative 3: use block Krylov method
problem.impODEmethod = 'krylov';
problem.krylov_tol = 1e-8;          % Residual tolerance stopping criterion


% Integration interval
problem.t0 = 0;
problem.tend = 1;
% Numbers of time steps to use
problem.Nt = 2^8;

% % Adaptiveness options (for the additive splitting schemes)
% problem.controller = 'PI'; % PI, unoptimized parameters
% % problem.controller = 0;  % Deadbeat (simplest, most commonly used)
% problem.epus = true;       % Use error per unit step measure rather than error per step
% problem.reuse_integral_factors = false; % Use the updating algorithm for computing the low-rank-factorization of I_Q(h) when h changes. False: recompute it from scratch every time.

% Output options
problem.intermediates = true;       % Store intermediate steps
problem.print_step = true;          % Print number of total steps and steps taken
problem.print_output = false;       % Print extra info every step

% Clean up and summarize arguments
disp(problem)

% Use Strang-splitting to solve the problem
[t, Ls, Ds, ms, time] = Strang(problem);
% NOTE: the code is written with parfor statements in some critical loops,
% mostly useful in the additive schemes. For small problems, the extra
% overhead of this parallellization may dominate the computation cost.
% To avoid this, make sure that MATLAB does not start a parallel pool
% automatically, and set one up manually if necessary.

% The solution approximation is given in factorized form by Ls*Ds*Ls'
% if problem.intermediates is true, then Ls{j}*Ds{j}*Ls{j}' is the
% approximation at t(j)
% ms(j) contains the rank of the approximation at t(j)
% time is the main computation time


% Solutions can be efficiently compared in the Frobenius without forming
% the dense matrices X = LDL' by using the function outerfrobnormdiff_LDLT
% as in the code snippet below. Here it is assumed that the above code was
% run several time with different options to generate LsEx, LsK, and LsIRK.
% (For the different matrix exponential action methods; exact, Krylov,
% IRK.)
% See also the example_error_framework.m file which shows how to use the
% existing code for comparing solutions corresponding to different temporal
% discretizations.

%
% for j = 1:problem.Nt
% %     XIRK{j} = LsIRK{j}*DsIRK{j}*LsIRK{j}';
% %     XEx{j} = LsEx{j}*DsEx{j}*LsEx{j}';
% %     XK{j} = LsK{j}*DsK{j}*LsK{j}';
%     errsK(j) = outerfrobnormdiff_LDLT(LsK{j}, DsK{j}, LsEx{j}, DsEx{j}) / outerfrobnorm_LDLT(LsEx{j}, DsEx{j});
%     errsIRK(j) = outerfrobnormdiff_LDLT(LsIRK{j}, DsIRK{j}, LsEx{j}, DsEx{j}) / outerfrobnorm_LDLT(LsEx{j}, DsEx{j});
% end
% hs = problem.tend ./ (1:problem.Nt);
%
% loglog(hs, errsK)
% hold on
% loglog(hs, errsIRK)
