function x = vandersolve(A,b)
% Solve the Vandermonde system Ax = b according to the method described in
% \r{A}ke Bj\"{o}rck and Victor Pereyra, "Solution of Vandermonde Systems of
% Equations", Mathematics of Computation, Vol. 24, No. 112 (Oct., 1970),
% pp. 893-903, http://www.jstor.org/stable/2004623.
% (Eq. (14)-(15) for the primal system.)

%
% Copyright 2018 Tony Stillfjord
%
% This file is part of DREsplit.
%
% DREsplit is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% DREsplit is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with DREsplit.  If not, see <http://www.gnu.org/licenses/>.
%


n = size(A,1) - 1;
d = b;
for k=1:n
    d(k+1:n+1) = d(k+1:n+1) - A(2,k)*d(k:n);
end

x = d;
for k=n:-1:1
    x(k+1:n+1) = x(k+1:n+1) ./ (A(2,k+1:n+1) - A(2,1:n-k+1))';
    x(k:n) = x(k:n) - x(k+1:n+1);
end

end
