%SPLITTING_DRE_T    Enumeration for the splitting scheme type.
%
%   See also SPLITTING_DRE, SPLITTING_DRE_OPTIONS, PROFILE_SPLITTING_DRE and
%   BENCH_SPLITTING_DRE.
%
%   Author: Maximilian Behr (MPI Magdeburg, CSC)


%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019
%

classdef (Sealed = true) splitting_dre_t < int8
    enumeration
        SPLITTING_LIE(0),
        SPLITTING_STRANG(1),
        SPLITTING_SYMMETRIC2(2),
        SPLITTING_SYMMETRIC4(4),
        SPLITTING_SYMMETRIC6(6),
        SPLITTING_SYMMETRIC8(8),
    end
end
