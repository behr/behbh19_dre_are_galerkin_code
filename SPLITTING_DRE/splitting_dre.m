%SPLITTING_DRE  Wrapper class around the splitting schemes implemention of
%   Tony Stillfjord for autonomous differential Riccati equations:
%
%       M'\dot{X}M = A'*X*M + M'*X*A - M'*X*B*B'*X*M + C'*C, X(0)=Z0*Z0'.
%
%   SPLITTING_DRE wraps the splitting scheme implemention of Tony Stillfjord
%   such that, the approximations of the solution in LDL' format
%   are accessible after each step of the splitting scheme.
%   The reason for this class is the original implementation of Tony
%   Stillfjord holds either all LDL' approximations in memory or
%   returns the only the last LDL' approximation. The implementation
%   of Stillfjord would therefore run out of memory.
%
%   SPLITTING_DRE methods:
%       splitting_dre           - Constructor.
%
%   SPLITTING_DRE protected methods:
%       m_prepare               - Compute the low-rank factor of the intergral.
%       m_time_step             - Perform a time step.
%       m_get_solution          - Return the approximation LDL' at current time.
%       prepare_lie             - Compute the low-rank factor of the integral for Lie scheme.
%       prepare_strang          - Compute the low-rank factor of the integral for Strang scheme.
%       prepare_symmetric       - Compute the low-rank factor of the integral for Symmetric scheme.
%       time_step_lie           - Perform a time step for Lie scheme.
%       time_step_strang        - Perform a time step for Strang scheme.
%       time_step_symmetric     - Perform a time step for Symmetric scheme.
%
%   SPLITTING_DRE private properties:
%       problem - problem structure for Stillfjords codes
%
%   See also SPLITTING_DRE, SPLITTING_DRE_T, PROFILE_SPLITTING_DRE and
%   BENCH_SPLITTING_DRE, SOLVER_DRE.
%
%   Author: Maximilian Behr (MPI Magdeburg, CSC)

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019
%

classdef splitting_dre < solver_dre

    properties(SetAccess = private)
        problem % problem structure for Stillfjords codes
    end

    methods

        function obj = splitting_dre(A, M, B, C, Z0, opt)
            %SPLITTING_DRE  Constructor of class.
            %
            %   SPLIT = SPLITTING_DRE(A, M, B, C, Z0, OPT) creates an
            %   instance of the class. OPT must be an instance
            %   of splitting_dre_options class. The matrices A and M must be
            %   double sparse and quadratic. If M is empty then the
            %   identity matrix is used. B, C and Z0 must be real
            %   and dense.
            %
            %   See also SPLITTING_DRE_OPTIONS and SOLVER_DRE.
            %
            %   Author: Maximilian Behr (MPI Magdeburg, CSC)

            %% check empty E
            if isempty(M)
                M = speye(size(A));
            end

            %% check input matrices
            validateattributes(A, {'double'}, {'real', 'square', '2d', 'finite', 'nonnan'}, mfilename, inputname(1));
            validateattributes(M, {'double'}, {'real', 'square', '2d', 'finite', 'nonnan'}, mfilename, inputname(2));
            validateattributes(B, {'double'}, {'nonsparse', 'real', '2d', 'nonsparse', 'finite', 'nonnan'}, mfilename, inputname(3));
            validateattributes(C, {'double'}, {'nonsparse', 'real', '2d', 'nonsparse', 'finite', 'nonnan'}, mfilename, inputname(4));
            validateattributes(Z0, {'double'}, {'nonsparse', 'real', '2d', 'nonsparse', 'finite', 'nonnan'}, mfilename, inputname(5));

            n = size(A, 1);
            assert(all(size(A) == [n, n]), '%s has wrong size', inputname(1));
            assert(all(size(M) == [n, n]), '%s has wrong size', inputname(2));
            assert(all(size(B, 1) == n), '%s has wrong size', inputname(3));
            assert(all(size(C, 2) == n), '%s has wrong size', inputname(4));
            assert(all(size(Z0, 1) == n), '%s has wrong size', inputname(5));

            %% check input options
            expected_class = 'splitting_dre_options';
            assert(isa(opt, expected_class), '%s - %s has wrong type, expected %s.', inputname(6), class(opt), expected_class);

            %% set input args to fields
            obj.options = opt;

            %% set integration parameters
            obj.t = 0;
            obj.h = 2^obj.options.k;
            obj.T = obj.options.T;
            assert(0 < obj.h && obj.h < obj.T && mod(obj.T, obj.h) == 0, 'Time integration parameters error.');

            %% create problem structure for tonys code
            obj.problem.A = A;
            obj.problem.M = M;
            obj.problem.B = B;
            obj.problem.C = C;
            obj.problem.LQ = C';
            obj.problem.DQ = eye(size(C, 1));
            obj.problem.L0 = Z0;
            obj.problem.D0 = eye(size(Z0, 2));
            obj.problem.Lk = obj.problem.L0;
            obj.problem.Dk = obj.problem.D0;
            obj.problem.Rinv = eye(size(B, 2));
            obj.problem.CC_tol = obj.options.CC_tol;

            obj.problem.impODEmethod = 'krylov';
            obj.problem.krylov_tol = obj.options.krylov_tol;
        end
    end

    methods(Access = protected)

        function m_prepare(obj)
            %M_PREPARE  Prepare the solver for iteration.
            %
            %   MOD.M_PREPARE() calls the corresponding prepare function
            %   for the desired splitting scheme. The low-rank factor
            %   of the integral is computed.
            %
            %   Author: Maximilian Behr (MPI Magdeburg, CSC)

            %% call the prepare function
            switch obj.options.splitting
                case splitting_dre_t.SPLITTING_LIE
                    obj.prepare_lie();
                case splitting_dre_t.SPLITTING_STRANG
                    obj.prepare_strang();
                case {splitting_dre_t.SPLITTING_SYMMETRIC2, splitting_dre_t.SPLITTING_SYMMETRIC4, splitting_dre_t.SPLITTING_SYMMETRIC6, splitting_dre_t.SPLITTING_SYMMETRIC8}
                    obj.prepare_symmetric();
                otherwise
                    error('Splitting Scheme %s is not implemented\n', obj.options.splitting);
            end
        end

        function m_time_step(obj)
            %M_TIME_STEP  Time steppping function for solver.
            %
            %   MOD.M_TIME_STEP() calls the corresponding time_step
            %   for the desired splitting scheme.
            %
            %   Author: Maximilian Behr (MPI Magdeburg, CSC)

            %% perform a time step
            switch obj.options.splitting
                case splitting_dre_t.SPLITTING_LIE
                    obj.time_step_lie();
                case splitting_dre_t.SPLITTING_STRANG
                    obj.time_step_strang();
                case {splitting_dre_t.SPLITTING_SYMMETRIC2, splitting_dre_t.SPLITTING_SYMMETRIC4, splitting_dre_t.SPLITTING_SYMMETRIC6, splitting_dre_t.SPLITTING_SYMMETRIC8}
                    obj.time_step_symmetric();
                otherwise
                    error('Splitting Scheme %s is not implemented\n', obj.options.splitting);
            end

            %% update time
            obj.t = obj.t + obj.h;
        end

        function [L, D] = m_get_solution(obj)
            %M_GET_SOLUTION  Return the current approximation in a LDL' format.
            %
            %   Author: Maximilian Behr (MPI Magdeburg, CSC)
            L = obj.problem.Lk;
            D = obj.problem.Dk;
        end

        function prepare_lie(obj)
            %PREPARE_LIE  Prepare function for Lie splitting scheme.
            %
            %   Author: Maximilian Behr (MPI Magdeburg, CSC)
            obj.problem.impODEobj = impODEobj(obj.problem, true);
            [IQL, IQD] = compute_integral_factors_LDLT(obj.problem.LQ, obj.problem.DQ, obj.h, obj.problem.M, obj.problem.A, 1, obj.problem, 29, true);
            obj.problem.IQL = IQL{1};
            obj.problem.IQD = IQD{1};
        end

        function time_step_lie(obj)
            %TIME_STEP_LIE  Time step function for Lie splitting scheme.
            %
            %   Author: Maximilian Behr (MPI Magdeburg, CSC)
            [obj.problem.Lk, obj.problem.Dk] = Lie_step(obj.h, obj.problem.M, obj.problem.A, obj.problem.B, obj.problem.IQL, obj.problem.IQD, obj.problem.Rinv, obj.problem.Lk, obj.problem.Dk, obj.problem);
        end

        function prepare_strang(obj)
            %PREPARE_STRANG  Prepare function for Strang splitting scheme.
            %
            %   Author: Maximilian Behr (MPI Magdeburg, CSC)
            obj.problem.impODEobj = impODEobj(obj.problem, true);
            [IQL, IQD] = compute_integral_factors_LDLT(obj.problem.LQ, obj.problem.DQ, obj.h, obj.problem.M, obj.problem.A, 1/2, obj.problem, 29, true);
            obj.problem.IQL = IQL{1};
            obj.problem.IQD = IQD{1};
        end

        function time_step_strang(obj)
            %TIME_STEP_STRANG  Time step function for Strang splitting scheme.
            %
            %   Author: Maximilian Behr (MPI Magdeburg, CSC)
            [obj.problem.Lk, obj.problem.Dk] = Strang_step(obj.h, obj.problem.M, obj.problem.A, obj.problem.B, obj.problem.IQL, obj.problem.IQD, obj.problem.Rinv, obj.problem.Lk, obj.problem.Dk, obj.problem);
        end


        function prepare_symmetric(obj)
            %PREPARE_SYMMETRIC  Prepare function for symmetric splitting scheme.
            %
            %   Author: Maximilian Behr (MPI Magdeburg, CSC)
            switch obj.options.splitting
                case splitting_dre_t.SPLITTING_SYMMETRIC2
                    obj.problem.gamma = 1 / 2;
                    obj.problem.s = 1;
                case splitting_dre_t.SPLITTING_SYMMETRIC4
                    obj.problem.gamma = [-1 / 6, 2 / 3];
                    obj.problem.s = 2;
                case splitting_dre_t.SPLITTING_SYMMETRIC6
                    obj.problem.gamma = [1 / 48; -8 / 15; 81 / 80];
                    obj.problem.s = 3;
                case splitting_dre_t.SPLITTING_SYMMETRIC8
                    obj.problem.gamma = [-1 / 720; 8 / 45; -729 / 560; 512 / 315];
                    obj.problem.s = 4;
                otherwise
                    error('Symmetric Splitting %s Scheme not implemented.\n', obj.options.splitting);
            end
            obj.problem.hfactors = 1 ./ (1:obj.problem.s);
            obj.problem.impODEobj = impODEobj(obj.problem, true);
            [IQL, IQD] = compute_integral_factors_LDLT(obj.problem.LQ, obj.problem.DQ, obj.h, obj.problem.M, obj.problem.A, obj.problem.hfactors, obj.problem, 29, true);
            obj.problem.IQL = IQL;
            obj.problem.IQD = IQD;
        end

        function time_step_symmetric(obj)
            %TIME_STEP_SYMMETRIC  Time step function for symmetric splitting scheme.
            %
            %   Author: Maximilian Behr (MPI Magdeburg, CSC)
            [obj.problem.Lk, obj.problem.Dk] = additive_symmetric_step(obj.problem.s, obj.problem.gamma, obj.h, obj.problem.M, obj.problem.A, obj.problem.B, obj.problem.IQL, obj.problem.IQD, obj.problem.Lk, obj.problem.Dk, obj.problem.Rinv, obj.problem);
        end
    end
end
