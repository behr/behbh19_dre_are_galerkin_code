%SPLITTING_DRE_OPTIONS  Options class for splitting_dre solver.
%
%   SPLITTING_DRE_OPTIONS properties:
%       name        - char, name of your options setting
%       k           - int, step size will be 2^k
%       T           - double, final Time
%       splitting   - splitting_dre_t type
%       CC_tol      - column compression tolerance
%       krylov_tol  - tolerance for Krylov subspace matrix expoential approximation
%
%   See also SPLITTING_DRE, SPLITTING_DRE_T, PROFILE_SPLITTING_DRE and
%   BENCH_SPLITTING_DRE, SOLVER_DRE_OPTIONS.
%
% Author: Maximilian Behr (MPI Magdeburg, CSC)

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019
%

classdef splitting_dre_options < solver_dre_options

    properties
        name % char, name of your options setting
        k % int, step size will be 2^k
        T % double, final Time
        splitting % splitting_dre_t type
        CC_tol % column compression tolerance
        krylov_tol % tolerance for krylov
    end

    methods
        function obj = splitting_dre_options()
            %SPLITTING_DRE_OPTIONS  Constructor of class.
            %
            %   SPLIT_OPT = SPLITTING_DRE_OPTIONS() creates an instance of
            %   the class.
            %
            %   See also SOLVER_DRE_OPTIONS and SPLITTING_DRE.
            %
            %   Author: Maximilian Behr (MPI Magdeburg, CSC)
            obj.name = 'empty';
            obj.k = -8;
            obj.T = 1.0;
            obj.splitting = splitting_dre_t.SPLITTING_LIE;
            obj.CC_tol = eps;
            obj.krylov_tol = 1e-9;
        end
    end
end
