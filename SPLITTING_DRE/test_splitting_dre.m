%TEST_SPLITTING_DRE   Test for splitting_dre.
%
%   TEST_SPLITTING_DRE compares the splitting_dre solver with
%   modified_davison_maki_dre solver. The rail example and several
%   examples from the slicot benchmark collection are used.
%
%   See also SPLITTING_DRE and MODIFIED_DAVISON_MAKI_DRE.
%
%   Author: Maximilian Behr (MPI Magdeburg, CSC)

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019
%

classdef test_splitting_dre < matlab.unittest.TestCase

    properties(TestParameter)
        %struct('instance','rail_371','T',4,'k',-4, 'reltol', 1e-5),
        %struct('instance','rail_371','T',4,'k',-5, 'reltol', 1e-5),
        %struct('instance','rail_371','T',4,'k',-6, 'reltol', 1e-5),
        %struct('instance','rail_1357','T',4,'k',-4),
        %struct('instance','beam','T',1/2,'k',-7, 'reltol', 5e-1),
        %struct('instance','beam','T',1/2,'k',-8, 'reltol', 5e-1),
        %struct('instance','beam','T',1/2,'k',-9, 'reltol', 5e-1),
        %struct('instance','build','T',20,'k',-3, 'reltol', 1e-4),
        %struct('instance','build','T',20,'k',-4, 'reltol', 1e-4),
        %struct('instance','build','T',20,'k',-5, 'reltol', 1e-4),
        %struct('instance','CDplayer','T',1/2^8,'k',-13, 'reltol', 5e-1),
        %struct('instance','CDplayer','T',1/2^8,'k',-14, 'reltol', 5e-1),
        %struct('instance','CDplayer','T',1/2^8,'k',-15, 'reltol', 5e-1),
        %struct('instance','eady','T',1/2^5,'k',-10, 'reltol', 5e-1),
        %struct('instance','eady','T',1/2^5,'k',-11, 'reltol', 5e-1),
        %struct('instance','eady','T',1/2^5,'k',-12, 'reltol', 5e-1),
        %struct('instance','fom','T',1/2^6,'k',-11, 'reltol', 5e-1),
        %struct('instance','fom','T',1/2^6,'k',-12, 'reltol', 5e-1),
        %struct('instance','fom','T',1/2^6,'k',-13. 'reltol', 5e-1),
        %struct('instance','heat-cont','T',65,'k',-7, 'reltol', 1e-5),
        %struct('instance','heat-cont','T',65,'k',-8, 'reltol', 1e-5),
        %struct('instance','heat-cont','T',65,'k',-9, 'reltol', 1e-5),
        %struct('instance','heat-cont','T',65,'k',-10, 'reltol', 1e-5),
        %struct('instance','iss','T',2,'k',-1, 'reltol', 1e-2),
        %struct('instance','iss','T',2,'k',-2, 'reltol', 1e-2),
        %struct('instance','iss','T',2,'k',-3, 'reltol', 1e-2),
        %struct('instance','iss','T',2,'k',-4, 'reltol', 1e-2),
        %struct('instance','pde','T',1/2^6,'k',-12, 'reltol', 5e-1),
        %struct('instance','pde','T',1/2^6,'k',-10, 'reltol', 5e-1),
        %struct('instance','pde','T',1/2^6,'k',-11, 'reltol', 5e-1),
        %struct('instance','pde','T',1/2^6,'k',-12, 'reltol', 5e-1),
        %struct('instance','random','T',1/2^16,'k',-21, 'reltol', 5e-1),
        %struct('instance','random','T',1/2^16,'k',-21, 'reltol', 5e-1),
        %struct('instance','random','T',1/2^16,'k',-22, 'reltol', 5e-1),
        %struct('instance','random','T',1/2^16,'k',-21, 'reltol', 5e-1),
        problem = { ... ,
            struct('instance', 'rail_371', 'T', 4, 'k', -3, 'reltol', 1e-5), ...
            struct('instance', 'beam', 'T', 1/2, 'k', -6, 'reltol', 5e-1), ...
            struct('instance', 'build', 'T', 20, 'k', -2, 'reltol', 1e-4), ...
            };%#ok<*COMNL>

        split_type = { ...
            splitting_dre_t.SPLITTING_LIE, ...
            splitting_dre_t.SPLITTING_STRANG, ...
            splitting_dre_t.SPLITTING_SYMMETRIC2, ...
            splitting_dre_t.SPLITTING_SYMMETRIC4, ...
            splitting_dre_t.SPLITTING_SYMMETRIC6, ...
            splitting_dre_t.SPLITTING_SYMMETRIC8};
    end

    properties
        verbose = false;
    end

    methods(Test)

        function test(obj, problem, split_type) %#ok<*INUSL>

            %% load data
            fprintf('\n');
            fprintf('----------------------------------------------\n');
            instance = problem.instance;
            T = problem.T;
            k = problem.k;
            reltol = problem.reltol;
            data = load(instance);
            A = data.A;
            B = full(data.B);
            C = full(data.C);
            n = size(A, 1);
            Z0 = zeros(n, 1);

            if isfield(data, 'E')
                E = data.E;
            else
                E = speye(n, n);
            end

            %% turn parfor off
            control_parfor(false);

            %% create splitting_solver
            solver_split_opt = splitting_dre_options();
            solver_split_opt.T = T;
            solver_split_opt.k = k;
            solver_split_opt.splitting = split_type;
            solver_split = splitting_dre(A, E, B, C, Z0, solver_split_opt);
            solver_split.prepare();

            %% create modified davison maki solver
            solver_mod_opt = modified_davison_maki_dre_options();
            solver_mod_opt.T = T;
            solver_mod_opt.k = k;
            solver_mod_opt.symmetric = true;
            solver_mod = modified_davison_maki_fac_sym_dre(full(A), full(E), full(B), full(C), full(Z0), solver_mod_opt);
            solver_mod.prepare();

            %% solve and compare solution
            while solver_mod.t < T

                % make time step
                solver_split.time_step();
                solver_mod.time_step();

                % get solution
                [L, D] = solver_split.get_solution();
                Xt = solver_mod.get_solution();

                % compare
                nrm2X_mod = two_norm_sym_handle(@(x) Xt*x, n);
                nrm2X_split = two_norm_sym_handle(@(x) (L * (D * (L' * x))), n);
                absnrm2err = two_norm_sym_handle(@(x) Xt*x-(L * (D * (L' * x))), n);
                relnrm2err = absnrm2err / nrm2X_mod;

                if obj.verbose
                    % print results
                    fprintf('instance=%s, k=%d, splitting=%s, t=%.8f, %6.2f %%, abs./rel. 2-norm error %e / %e, ||mod||_2=%e, ||split||_2=%e \n', ...
                        instance, k, char(split_type), solver_mod.t, solver_mod.t/T*100, absnrm2err, relnrm2err, nrm2X_mod, nrm2X_split);
                end

                % check tolerance for relative error
                obj.fatalAssertLessThan(relnrm2err, reltol);

            end
            fprintf('Time %-35s = %f\n', char(split_type), solver_split.wtime_time_step);
            fprintf('Time %-35s = %f\n', upper(class(solver_mod)), solver_mod.wtime_time_step);
            fprintf('----------------------------------------------\n\n');

        end
    end
end
