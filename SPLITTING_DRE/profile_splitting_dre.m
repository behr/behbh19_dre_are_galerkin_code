%PROFILE_SPLITTING_DRE  Profile for splitting_dre solvers.
%
%   See also SPLITTING_DRE and SPLITTING_DRE_OPTIONS.
%
%   Author: Maximilian Behr (MPI Magdeburg, CSC)

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019
%

%% clear all
clearvars, close all;
clc

%% parameter
instance = 'rail_1357';
T = 1;
k = -4;
split_type = 'SPLITTING_SYMMETRIC2';
parallel_for = false;

%% load data
data = load(instance);
A = data.A;
E = data.E;
B = data.B;
C = data.C;
n = size(A, 1);

%% prepare solver
control_parfor(parallel_for);
solver_opt = splitting_dre_options();
solver_opt.T = T;
solver_opt.k = k;
solver_opt.splitting = split_type;
solver = splitting_dre(A, E, B, C, zeros(n, 1), solver_opt);
solver.prepare();

%% profile solver
profile on;
fprintf('%s - Start integration to T = %.4f with solver = %s, k = %d and parfor = %d.\n', datestr(now), T, split_type, k, parallel_for);
tic;
solver.time_step_until(T);
wtime = toc;
fprintf('%s - Finished integration to T = %.4f with solver = %s, k = %d and parfor = %d.\n', datestr(now), T, split_type, k, parallel_for);
fprintf('Computational Time = %f.\n\n', wtime);
profile viewer;
