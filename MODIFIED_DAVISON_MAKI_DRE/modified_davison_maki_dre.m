%MODIFIED_DAVISON_MAKI_DRE  Implements modified Davison Maki for
% autonomous nonsymmetric differential Riccati equations:
%
%   Nonsymmetric differential Riccati equation:
%
%   \dot{X} = M22 X - X M11 - X M12 X + M21, X(0) = M0       (NDRE)
%
%   The class solves the NDRE using the modified Davison Maki method.
%   The base class is solver_dre.
%
%   MODIFIED_DAVISON_MAKI_DRE methods:
%       modified_davison_maki_dre   - Constructor.
%       m_prepare                   - Compute the matrix exponential and check the 1-norm.
%       m_time_step                 - Perform a time step.
%       m_get_solution              - Return the approximation at current time.
%
%   MODIFIED_DAVISON_MAKI properties:
%       M11             - double, dense, n-x-n matrix M11 of NDRE
%       M12             - double, dense, n-x-m matrix M12 of NDRE
%       M21             - double, dense, m-x-n matrix M21 of NDRE
%       M22             - double, dense, m-x-m matrix M22 of NDRE
%       M0              - double, dense, m-x-n matrix M0  of NDRE
%       exp_hM11        - submatrix 1,1 of matrix exponential
%       exp_hM12        - submatrix 1,2 of matrix exponential
%       exp_hM21        - submatrix 2,1 of matrix exponential
%       exp_hM22        - submatrix 2,2 of matrix exponential
%       nrm1_exp_hM     - the 1-norm of the matrix exponential of h*M
%       opt             - modified_davison_maki_dre_options instance
%       Xk              - current approximation for X(t)
%
%   See also BENCH_MODIFIED_DAVISON_MAKI_DRE,
%   MODIFIED_DAVISON_MAKI_DRE_OPTIONS, MODIFIED_DAVISON_MAKI_FAC_SYM_DRE,
%   MODIFIED_DAVISON_MAKI_SYM_DRE, SOLVER_DRE and SOLVER_DRE_OPTIONS.
%
%   Author: Maximilian Behr (MPI Magdeburg, CSC)

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019
%

classdef modified_davison_maki_dre < solver_dre

    properties(SetAccess = private)
        M11 % double, dense, n-x-n matrix M11 of DRE
        M12 % double, dense, n-x-n matrix M12 of DRE
        M21 % double, dense, n-x-n matrix M21 of DRE
        M22 % double, dense, n-x-n matrix M22 of DRE
        M0 % double, dense, n-x-n matrix M0 of DRE
        exp_hM11 % submatrix 1,1 of matrix exponential
        exp_hM12 % submatrix 1,2 of matrix exponential
        exp_hM21 % submatrix 2,1 of matrix exponential
        exp_hM22 % submatrix 2,2 of matrix exponential
        nrm1_exp_hM % the 1-norm of the matrix exponential of h*M
        opt % modified_davison_maki_dre_options instance
        Xk % current approximation for X(t)
    end

    methods

        function obj = modified_davison_maki_dre(M11, M12, M21, M22, M0, opt)
            %MODIFIED_DAVISON_MAKI_DRE  Constructor of class.
            %
            %   MOD = MODIFIED_DAVISON_MAKI_DRE(M11, M12, M21, M22, M0, OPT)
            %   creates an instance of the class. OPT must be an instance
            %   of modified_davison_maki_dre_options class.
            %   The matrices M11, M12, M21, M22, M0 must be dense, double
            %   and of suiteable size.
            %
            %   See also SOLVER_DRE.
            %
            %   Author: Maximilian Behr (MPI Magdeburg, CSC)

            %% check input matrices
            validateattributes(M11, {'double'}, {'real', '2d', 'nonsparse', 'finite', 'nonnan'}, mfilename, inputname(1));
            validateattributes(M12, {'double'}, {'real', '2d', 'nonsparse', 'finite', 'nonnan'}, mfilename, inputname(2));
            validateattributes(M21, {'double'}, {'real', '2d', 'nonsparse', 'finite', 'nonnan'}, mfilename, inputname(3));
            validateattributes(M22, {'double'}, {'real', '2d', 'nonsparse', 'finite', 'nonnan'}, mfilename, inputname(4));
            validateattributes(M0, {'double'}, {'real', '2d', 'nonsparse', 'finite', 'nonnan'}, mfilename, inputname(5));

            [n, m] = size(M0);
            assert(all(size(M11) == [n, n]), '%s has wrong size', inputname(1));
            assert(all(size(M12) == [n, m]), '%s has wrong size', inputname(2));
            assert(all(size(M21) == [m, n]), '%s has wrong size', inputname(3));
            assert(all(size(M22) == [m, m]), '%s has wrong size', inputname(4));
            assert(all(size(M0) == [n, m]), '%s has wrong size', inputname(5));

            %% check input options
            expected_class = 'modified_davison_maki_dre_options';
            assert(isa(opt, expected_class), '%s - %s has wrong type, expected %s.', inputname(6), class(opt), expected_class);
            obj.opt = opt;

            %% set integration parameters
            obj.t = 0;
            obj.h = 2^obj.opt.k;
            obj.T = obj.opt.T;
            assert(0 < obj.h && obj.h < obj.T && mod(obj.T, obj.h) == 0, 'Time integration parameters error.');

            %% set matrices
            obj.M11 = M11;
            obj.M12 = M12;
            obj.M21 = M21;
            obj.M22 = M22;
            obj.M0 = M0;
            obj.Xk = M0;
        end
    end

    methods(Access = protected)
        function m_prepare(obj)
            %M_PREPARE  Prepare the solver for iteration.
            %
            %   MOD.M_PREPARE() computes the matrix exponential h*M and
            %   its 1-norm if the 1-norm is too large a warning is printed
            %   or an error message it returned.
            %
            %   See also SOLVER_DRE and MODIFIED_DAVISON_MAKI_DRE_OPTIONS.
            %
            %   Author: Maximilian Behr (MPI Magdeburg, CSC)

            %% compute the matrix exponential
            n = size(obj.M0, 1);
            exp_hM = expm(obj.h*full([obj.M11, obj.M12; obj.M21, obj.M22]));
            obj.exp_hM11 = exp_hM(1:n, 1:n);
            obj.exp_hM12 = exp_hM(1:n, n+1:end);
            obj.exp_hM21 = exp_hM(n+1:end, 1:n);
            obj.exp_hM22 = exp_hM(n+1:end, n+1:end);

            %% check if matrix exponential is too large
            nrm1exphM = norm(exp_hM, 1);
            fprintf('%s - %s, h = %e, ||expm(h*M)||_1 = %e.\n', datestr(now), upper(class(obj)), obj.h, nrm1exphM);
            if obj.opt.warning_bound_expmM <= nrm1exphM && nrm1exphM < obj.opt.error_bound_expmM
                warning('%s:1-norm of matrix exponential is large %e.\n', upper(class(obj)), nrm1exphM);
            end
            if obj.opt.error_bound_expmM <= nrm1exphM || isinf(nrm1exphM) || isnan(nrm1exphM)
                error('%s:1-norm of matrix exponential is too large %e, use smaller step sizes.\n', upper(class(obj)), nrm1exphM);
            end
            obj.nrm1_exp_hM = nrm1exphM;
        end

        function m_time_step(obj)
            %M_TIME_STEP  Time steppping function for solver.
            %
            %   MOD.M_TIME_STEP() performes one time step using the
            %   modified Davison Maki algorithm.
            %
            %   See also SOLVER_DRE and MODIFIED_DAVISON_MAKI_DRE_OPTIONS.
            %
            %   Author: Maximilian Behr (MPI Magdeburg, CSC)

            %% perform time step
            U = obj.exp_hM11 + obj.exp_hM12 * obj.Xk;
            V = obj.exp_hM21 + obj.exp_hM22 * obj.Xk;
            obj.Xk = V / U;

            %% make solution symmetric
            if obj.opt.symmetric
                obj.Xk = 1 / 2 * (obj.Xk + obj.Xk');
            end

            %% update time
            obj.t = obj.t + obj.h;
        end

        function Xt = m_get_solution(obj)
            %M_GET_SOLUTION  Return the current approximation as a matrix Xt.
            %
            %   See also SOLVER_DRE and MODIFIED_DAVISON_MAKI_DRE_OPTIONS.
            %
            %   Author: Maximilian Behr (MPI Magdeburg, CSC)
            Xt = obj.Xk;
        end
    end

end
