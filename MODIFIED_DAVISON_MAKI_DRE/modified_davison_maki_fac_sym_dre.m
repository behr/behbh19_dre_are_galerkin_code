%MODIFIED_DAVISON_MAKI_FAC_SYM_DRE  Subclass of modified_davison_maki_dre
%   for autonomous factorized symmetric differential Riccati equations:
%
%   M' \dot{X} M = A' X M + M X A - M' X B B' X M + C' C, X(0) = Z0 Z0' (DRE)
%
%  The class solves the DRE using the modified Davison Maki method.
%
%   MODIFIED_DAVISON_MAKI_FAC_SYM_DRE methods:
%       modified_davison_maki_fac_sym_dre   - Constructor.
%
%   See also BENCH_MODIFIED_DAVISON_MAKI_DRE,
%   MODIFIED_DAVISON_MAKI_DRE_OPTIONS, MODIFIED_DAVISON_MAKI_FAC_SYM_DRE,
%   MODIFIED_DAVISON_MAKI_SYM_DRE, SOLVER_DRE and SOLVER_DRE_OPTIONS.
%
%   Author: Maximilian Behr (MPI Magdeburg, CSC)

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019
%

classdef modified_davison_maki_fac_sym_dre < modified_davison_maki_dre

    methods

        function obj = modified_davison_maki_fac_sym_dre(A, M, B, C, Z0, opt)
            %MODIFIED_DAVISON_MAKI_FAC_SYM_DRE  Constructor of class.
            %
            %   MOD = MODIFIED_DAVISON_MAKI_FAC_SYM_DRE(A, M, B, C, Z0, opt)
            %   creates an instance of the class. OPT must be an instance
            %   of modified_davison_maki_dre_options class.
            %   The matrices A, M, B, C, Z0 must be dense, double
            %   and of suiteable size. If M is empty then the identity
            %   matrix is used. M must be nonsingular.
            %
            %   See also SOLVER_DRE and MODIFIED_DAVISON_MAKI_DRE.
            %
            %   Author: Maximilian Behr (MPI Magdeburg, CSC)

            %% check input matrices
            validateattributes(A, {'double'}, {'real', '2d', 'nonsparse', 'finite', 'nonnan', 'square'}, mfilename, inputname(1));
            validateattributes(B, {'double'}, {'real', '2d', 'nonsparse', 'finite', 'nonnan'}, mfilename, inputname(3));
            validateattributes(C, {'double'}, {'real', '2d', 'nonsparse', 'finite', 'nonnan'}, mfilename, inputname(4));
            validateattributes(Z0, {'double'}, {'real', '2d', 'nonsparse', 'finite', 'nonnan'}, mfilename, inputname(5));

            assert(all(size(A, 1) == size(B, 1)), '%s has wrong size', inputname(2));
            assert(all(size(B, 1) == size(C, 2)), '%s has wrong size', inputname(3));
            assert(all(size(C, 2) == size(Z0, 1)), '%s has wrong size', inputname(4));

            if ~isempty(M)
                validateattributes(M, {'double'}, {'real', '2d', 'square', 'nonsparse', 'finite', 'nonnan', 'square'}, mfilename, inputname(2));
                assert(all(size(M) == size(A)), '%s has wrong size', inputname(2));
            end

            %% check input options
            expected_class = 'modified_davison_maki_dre_options';
            assert(isa(opt, expected_class), '%s - %s has wrong type, expected %s.', inputname(6), class(opt), expected_class);
            assert(opt.symmetric == true, 'symmetric has to be true in options.');

            %% call constructor
            if isempty(M)
                A2 = A;
                C2 = C;
            else
                dM = decomposition(M);
                A2 = A / dM;
                C2 = C / dM;
            end
            obj@modified_davison_maki_dre(-A2, B*B', C2'*C2, A2', Z0*Z0', opt);

        end
    end
end
