% BENCH_MODIFIED_DAVISON_MAKI_DRE
%
%   Benchmark for modified_davison_maki_dre solvers.
%
%   Author: Maximilian Behr (MPI Magdeburg, CSC)

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019
%

%% clear
clearvars, close all;
clc

%% parameter
instance = 'rail_371';
T = 45;
ks = 0:-1:-4;
symmetric = true;

%% load data
data = load(instance);
A = full(data.A);
E = full(data.E);
B = full(data.B);
C = full(data.C);
n = size(A, 1);
Z0 = zeros(n, 1);

%% create filenames and directories for results
mybasename = sprintf('%s_T%.2f', instance, T);
[mydir, ~, ~] = fileparts(mfilename('fullpath'));
mydir = fullfile(mydir, 'results', mfilename, mybasename);
[status, message, messageid] = rmdir(mydir, 's');
if ~status
    fprintf('%s - %s:%s-%s\n', datestr(now), mfilename, message, messageid);
end
mydiary = fullfile(mydir, sprintf('%s.log', mybasename));
mymat = fullfile(mydir, sprintf('%s.mat', mybasename));
mycsv = fullfile(mydir, sprintf('%s.csv', mybasename));
mkdir(mydir);

%% delete old diary and create new
delete(mydiary);
diary(mydiary);

%% prepare table
dummy = zeros(length(ks), 1);
tab = table();
tab = addvars(tab, ks', 'NewVariableNames', 'StepSize');
tab = addvars(tab, dummy, 'NewVariableNames', 'modified_davison_maki_dre');

%% benchmark solver
for i_k = 1:length(ks)

    % get variable
    k = ks(i_k);

    % solver options
    solver_opt = modified_davison_maki_dre_options();
    solver_opt.T = T;
    solver_opt.k = k;
    solver_opt.symmetric = symmetric;

    % solve
    solver = modified_davison_maki_fac_sym_dre(A, E, B, C, Z0, solver_opt);
    solver.prepare();

    fprintf('%s - Start integration,    T = %.4f, k = %d.\n', datestr(now), T, k);
    solver.time_step_until(T);
    fprintf('%s - Finished integration, T = %.4f, k = %d.\n', datestr(now), T, k);
    fprintf('Computational Time = %f.\n\n', solver.wtime_time_step_until);

    % store in table
    tab(i_k, 2) = {solver.wtime_time_step_until};
end

%% display results
display(tab)

%% write results to files
% write csv file from table
writetable(tab, mycsv, 'Delimiter', ',');

% write mat file
hostname = getComputerName();
timenow = datestr(now);
gitinfo = GitInfo();
save(mymat, 'instance', 'ks', 'tab', 'hostname', 'timenow', 'gitinfo', 'T');

%% turn diary off
diary('off');
