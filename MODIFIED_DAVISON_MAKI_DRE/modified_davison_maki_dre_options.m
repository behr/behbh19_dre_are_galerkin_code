%MODIFIED_DAVISON_MAKI_DRE_OPTIONS  Options class for
%   modified_davison_maki_dre.
%
%   MODIFIED_DAVISON_MAKI_DRE_OPTIONS properties:
%       name        - char, name of your options settings
%       symmetric   - logical, symmetric solution
%       k           - int, time step will be 2^k
%       T           - double, final Time
%
%   See also BENCH_MODIFIED_DAVISON_MAKI_DRE,
%   MODIFIED_DAVISON_MAKI_DRE, MODIFIED_DAVISON_MAKI_FAC_SYM_DRE,
%   MODIFIED_DAVISON_MAKI_SYM_DRE, SOLVER_DRE_OPTIONS and TEST_SPLITTING_DRE.
%
%   Author: Maximilian Behr (MPI Magdeburg, CSC)

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019
%

classdef modified_davison_maki_dre_options < solver_dre_options

    properties
        name % char, name of your options settings
        symmetric % logical, symmetric equation or not
        k % int, step size will be h = 2^k
        T % double, final time
    end

    properties(SetAccess = immutable)
        warning_bound_expmM = 1e+8; % warning if matrix exponential gets too large
        error_bound_expmM = 1e+10; % error if matrix exponential gets too large
    end

    methods
        function obj = modified_davison_maki_dre_options()
            %MODIFIED_DAVISON_MAKI_DRE_OPTIONS  Constructor of class.
            %
            %   MOD_OPT = MODIFIED_DAVISON_MAKI_DRE_OPTIONS()
            %   creates an instance of the class.
            %
            %   See also SOLVER_DRE_OPTIONS.
            %
            %   Author: Maximilian Behr (MPI Magdeburg, CSC)
            obj.name = 'empty';
            obj.symmetric = false;
            obj.k = -8;
            obj.T = 1.0;
        end
    end
end
