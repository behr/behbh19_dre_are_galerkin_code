%MODIFIED_DAVISON_MAKI_SYM_DRE      Subclass of modified_davison_maki_dre
%   for autonomous symmetric differential Riccati equations:
%
%   M' \dot{X} M = A' X M + M X A - M' X S X M + Q, X(0) = X0 (DRE)
%
%   The class solves the DRE using the modified Davison Maki method:
%
%   MODIFIED_DAVISON_MAKI_SYM_DRE methods:
%       modified_davison_maki_sym_dre   - Constructor.
%
%   Author: Maximilian Behr (MPI Magdeburg, CSC)

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019
%

classdef modified_davison_maki_sym_dre < modified_davison_maki_dre

    methods

        function obj = modified_davison_maki_sym_dre(A, M, S, Q, X0, options)
            %MODIFIED_DAVISON_MAKI_SYM_DRE  Constructor of class.
            %
            %   MOD = MODIFIED_DAVISON_MAKI_SYM_DRE(A, M, B, C, Z0, opt)
            %   creates an instance of the class. OPT must be an instance
            %   of modified_davison_maki_dre_options class.
            %   The matrices A, M, S, Q and X0 must be dense, double, square
            %   and of same size. If M is empty then the identity
            %   matrix is used. M must be nonsingular.
            %
            %   See also SOLVER_DRE and MODIFIED_DAVISON_MAKI_DRE.
            %
            %   Author: Maximilian Behr (MPI Magdeburg, CSC)

            %% check input matrices
            validateattributes(A, {'double'}, {'real', '2d', 'square'}, mfilename, inputname(1));
            validateattributes(S, {'double'}, {'real', '2d', 'square'}, mfilename, inputname(3));
            validateattributes(Q, {'double'}, {'real', '2d', 'square'}, mfilename, inputname(4));
            validateattributes(X0, {'double'}, {'real', '2d', 'square'}, mfilename, inputname(5));

            assert(all(size(A) == size(S)), '%s has wrong size', inputname(1));
            assert(all(size(S) == size(Q)), '%s has wrong size', inputname(3));
            assert(all(size(Q) == size(X0)), '%s has wrong size', inputname(4));

            if ~isempty(M)
                validateattributes(M, {'double'}, {'real', '2d', 'square', 'finite', 'nonnan', 'nonsparse'}, mfilename, inputname(2));
                assert(all(size(M) == size(A)), '%s has wrong size', inputname(2));
            end

            %% check input options
            expected_class = 'modified_davison_maki_dre_options';
            assert(isa(options, expected_class), '%s - %s has wrong type, expected %s.', inputname(6), class(options), expected_class);
            assert(options.symmetric == true, 'symmetric has to be true in options.');

            %% call constructor
            if isempty(M)
                A2 = A;
                Q2 = 1 / 2 * (Q + Q');
                S2 = 1 / 2 * (S + S');
                X02 = 1 / 2 * (X0 + X0');
            else
                dM = decomposition(M);
                A2 = A / dM;
                Q2 = Q / dM;
                Q2 = 1 / 2 * (Q2 + Q2');
                S2 = 1 / 2 * (S + S');
                X02 = 1 / 2 * (X0 + X0');
            end
            obj@modified_davison_maki_dre(-A2, S2, Q2, A2', X02, options);

        end
    end
end
