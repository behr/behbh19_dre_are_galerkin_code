% TEST_EXAMPLE_ENTRIES_DECAY_DRE    Test for example_entries_decay_dre.
%
%   TEST_EXAMPLE_ENTRIES_DECAY_DRE calls the function
%   example_entries_decay_dre simply with different parameters.
%
%   See also EXAMPLE_ENTRIES_DECAY_DRE and RUN_EXAMPLE_ENTRIES_DECAY_DRE.
%
%   Author: Maximilian Behr (MPI Magdeburg, CSC)

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019
%

classdef test_example_entries_decay_dre < matlab.unittest.TestCase

    properties(TestParameter)
        alpha = {1, 2, 5};
        n = {2, 5};
        multiprec = {false, (~verLessThan('matlab', '9.0') && license('test', 'Symbolic_Toolbox'))};
        prec = {32, 64};
        T = {2};
        k = {-4};
    end

    methods(Test)
        function test(obj, alpha, n, T, k, multiprec, prec) %#ok<*INUSL>
            example_entries_decay_dre(alpha, n, T, k, multiprec, prec, false);
        end
    end
end
