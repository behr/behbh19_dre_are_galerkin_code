function example_entries_decay_dre(alpha, n, T, k, multiprec, prec, plot_visible)
%EXAMPLE_ENTRIES_DECAY_DRE      Shows the behaviour of the entries of the
%   solution of the differential Riccati equation after a change of basis.
%
%   EXAMPLE_ENTRIES_DECAY_DRE visualises the behaviour of the entries of
%   the solution of the Differential Riccati Equation
%
%       \dot{X} = A'X + XA - XBB'X + C'C, X(0)=0
%
%   after a change of basis for tridiagonal matrices
%   A = tridiag(alpha, -1, alpha), B = ones(n, 1) and C = ones(1,n)
%   on the interval [0,1]. The basis consists of the eigenvectors of the
%   solution of the algebraic Riccati equation.
%
%   EXAMPLE_ENTRIES_DECAY_DRE(ALPHA, N, T, MULTIPREC, PREC, PLOT_VISIBLE)
%
%   Arguments:
%   ALPHA               - double, scalar number
%   N                   - nonnegative integer, size of A
%   T                   - double, scalar positive number the final time
%   K                   - double, integer, the step size will be 2^(K)
%   MULTIPREC           - logical, turn multiprecision on/off
%   PREC                - nonegative integer, control precision
%   PLOTTING_VISIBLE    - logical, turn visible plotting on/off
%
%   If MULTIPREC is false, then standard IEEE double precision is used,
%   otherwise variable precision arithmetic (vpa) is used.
%   The number of significant digits can be controlled by PREC.
%
%   See also TEST_EXAMPLE_ENTRIES_DECAY_DRE and RUN_EXAMPLE_ENTRIES_DECAY_DRE.
%
%   Author: Maximilian Behr (MPI Magdeburg, CSC)

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019
%

%% check input arguments
validateattributes(alpha, {'double'}, {'real', 'scalar', 'nonnan', 'finite', 'nonsparse'}, mfilename, inputname(1));
validateattributes(n, {'double'}, {'real', '>', 0 ,'scalar', 'integer', 'nonnan', 'finite', 'nonsparse'}, mfilename, inputname(2));
validateattributes(T, {'double'}, {'real', '>', 0 ,'scalar', 'nonnan', 'finite', 'nonsparse'}, mfilename, inputname(3));
validateattributes(k, {'double'}, {'real', 'scalar', 'integer', 'nonnan', 'finite', 'nonsparse'}, mfilename, inputname(4));
assert(isa(multiprec, 'logical'), '%s: %s is no logical.', mfilename, inputname(5));
validateattributes(prec, {'double'}, {'real', '>', 0 ,'scalar', 'integer', 'nonnan', 'finite', 'nonsparse'}, mfilename, inputname(6));
assert(isa(plot_visible, 'logical'), '%s: %s is no logical.', mfilename, inputname(7));

%% turn multiprec on/off, if multiprec is on it takes significantly longer
if multiprec
    multiprec_handle = @(x) vpa(x);
    digits(prec);
else
    multiprec_handle = @(x) x;
    prec = 16;
end

%% check matlab version, because of fprintf incompatbility with symbolic
if verLessThan('matlab', '9.0') && multiprec
    error('Matlab Version is too old. fprintf does not work with symbolic inputs.');
end

if ~license('test', 'Symbolic_Toolbox') && multiprec
    error('Symbolic Toolbox not available.');
end

%% set data for differential Riccati equation \dot{X} = A' X + X A - X B B' X + C'C, X(0)=0
A = multiprec_handle(full(gallery('tridiag', n, alpha, -1, -alpha)));
B = multiprec_handle(ones(n, 1));
C = multiprec_handle(ones(1, n));
X0 = multiprec_handle(zeros(n, n));

% Parameters for modified Davison Maki method
T = multiprec_handle(T);
k = multiprec_handle(k);
t = multiprec_handle(0);
h = multiprec_handle(2^k);

store_data_at_t = 1:T;
store_pics_at_t = 1:T;
meps = multiprec_handle(eps);

%% create filenames and directories for results
mybasename = sprintf('%s_alpha%d_T%d_n%d_prec%d_multiprec%d', mfilename, alpha, T, n, prec, multiprec);
[mydir, ~, ~] = fileparts(mfilename('fullpath'));
mydir = fullfile(mydir, 'results', mfilename, mybasename);
[status, message, messageid] = rmdir(mydir, 's');
if ~status
    fprintf('%s - %s:%s-%s\n', datestr(now), mfilename, message, messageid);
end
mydiary = fullfile(mydir, sprintf('%s.log', mybasename));
mymat = fullfile(mydir, sprintf('%s.mat', mybasename));
myvid = fullfile(mydir, mybasename);
mypic = fullfile(mydir, mybasename);
mkdir(mydir);

%% delete old diary and create new
delete(mydiary);
diary(mydiary);

%% setup parameters for modified Davison Maki method
fprintf('%s - Start Matrix Exponential Computation of Hamiltonian\n', datestr(now));
expmhH = expm(-h*[A, -B * B'; -C' * C, -A']);
fprintf('%s - Finished Matrix Exponential Computation of Hamiltonian\n', datestr(now));
expmhH11 = expmhH(1:n, 1:n);
expmhH12 = expmhH(1:n, (n + 1):end);
expmhH21 = expmhH((n + 1):end, 1:n);
expmhH22 = expmhH((n + 1):end, (n + 1):end);
Xk = X0;

%% solve ARE
fprintf('%s - Solve algebraic Riccati equation\n', datestr(now));
if multiprec
    [V, D] = eig([A, -B * B'; -C' * C, -A']);
    [~, I] = sort(real(diag(D)), 'ascend');
    I = I(1:n);
    V = V(:, I);
    Xinf = V((n + 1):end, 1:n) / V(1:n, 1:n);
    Xinf = real(Xinf);
    Xinf = 0.5 * (Xinf + Xinf');
else
    Xinf = care(A, B, C'*C);
end
fprintf('%s - Finished solving algebraic Riccati equation\n', datestr(now));

fprintf('%s - Compute eigenvalues of solution of algebraic Riccati equation\n', datestr(now));
[Vinf, Dinf] = eig(Xinf);
[Dinf, Iinf] = sort(diag(abs(Dinf)), 'descend');
Vinf = Vinf(:, Iinf);
fprintf('%s - Finished computing eigenvalues of solution of algebraic Riccati equation\n', datestr(now))

%% create plot
close all;
if ~plot_visible
    set(0, 'DefaultFigureVisible', 'off');
end

t0 = t;
hs = surf(double(abs(Vinf'*Xk*Vinf)), 'Facecolor', 'interp', 'LineStyle', 'none');
view([-20, 40]);
colorbar;
colormap(jet);
shading interp;
%set(gca, 'ZScale', 'log', 'colorscale', 'log');
set(gca, 'ZScale', 'log');
cb = colorbar();
cb.Ruler.Scale = 'log';
cb.Ruler.MinorTick = 'on';

caxis([double(meps), 1e+1]);
set(gcf, 'Position', [100, 100, 1024, 1024]);
zlim([double(meps), 1e+1]);
ax = gca;
ax.YDir = 'reverse';
ax.XDir = 'reverse';
xlabel('j');
ylabel('i');
zlabel('|(V^TX(t)V))_{i,j}|');
title(sprintf('t = %.5f, t \\in [%.1f, %.1f]', t, t0, T));

%% create video, solve equation and save matrices
v = VideoWriter(myvid, 'Motion JPEG AVI');
v.FrameRate = double(round(1/h));
open(v)

ABS_Vk_T_Xk_V = {};
tstore = [];
i = 1;
j = 1;

while t < T

    % time step modified davison maki
    Xk = (expmhH21 + expmhH22 * Xk) / (expmhH11 + expmhH12 * Xk);
    Xk = 0.5 * (Xk + Xk');
    t = t + h;
    fprintf('%s - Modified Davison Maki t = %f\n', datestr(now), t);

    % transform solution and take absolute values
    Xtemp = abs(Vinf'*Xk*Vinf);
    doubleXtemp = double(Xtemp);
    fprintf('%s - Modified Davison Maki t = %f min = %e max = %e\n', datestr(now), t, min(min(Xtemp)), max(max(Xtemp)));
    doubleXtemp(doubleXtemp <= double(meps)) = double(meps);

    % update plot
    set(hs, 'ZData', doubleXtemp);
    title(sprintf('t = %.5f, t \\in [%.1f, %.1f]', t, t0, T));
    writeVideo(v, getframe(gcf));
    i = i + 1;

    % capture picture
    if ismember(t, store_pics_at_t)
        fprintf('%s - Store Picture at t = %f\n', datestr(now), t);
        saveas(gcf, sprintf('%s_t_%f.eps', mypic, t), 'epsc');
        saveas(gcf, sprintf('%s_t_%f.jpg', mypic, t));
    end

    % save matrices
    if ismember(t, store_data_at_t)
        fprintf('%s - Collect Data at t = %f\n', datestr(now), t);

        %% truncate values below machine eps
        tstore = [tstore, t]; %#ok<*AGROW>
        Xtemp(Xtemp <= meps) = meps;
        ABS_Vk_T_Xk_V{j} = Xtemp;
        j = j + 1;
    end

end

close(v);

% reset setting
if ~plot_visible
    set(0, 'DefaultFigureVisible', 'on');
end

%% write results to tikz
fprintf('%s - Start writing data to tikz file\n', datestr(now));
hostname = getComputerName();
timenow = datestr(now);
gitinfo = GitInfo();
mycpuinfo = cpuinfo();

% store time data
for ti = 1:length(tstore)
    fprintf('%s - Writing data to tikz file for t = %f\n', datestr(now), tstore(ti));

    mydat = fullfile(mydir, sprintf('%s_t%.3f.dat', mybasename, tstore(ti)));

    dlmwrite(mydat, sprintf('mfilename              = %s', mfilename), 'delimiter', '');
    dlmwrite(mydat, sprintf('Time                   = %s', timenow), 'delimiter', '', '-append');
    dlmwrite(mydat, sprintf('Host                   = %s', hostname), 'delimiter', '', '-append');
    dlmwrite(mydat, sprintf('CPU-Name               = %s', mycpuinfo.Name), 'delimiter', '', '-append');
    dlmwrite(mydat, sprintf('CPU-Clock              = %s', mycpuinfo.Clock), 'delimiter', '', '-append');
    dlmwrite(mydat, sprintf('CPU-Cache              = %s', mycpuinfo.Cache), 'delimiter', '', '-append');
    dlmwrite(mydat, sprintf('CPU-NumProcessors      = %d', mycpuinfo.NumProcessors), 'delimiter', '', '-append');
    dlmwrite(mydat, sprintf('CPU-OSType             = %s', mycpuinfo.OSType), 'delimiter', '', '-append');
    dlmwrite(mydat, sprintf('CPU-OSVersion          = %s', mycpuinfo.OSVersion), 'delimiter', '', '-append');
    dlmwrite(mydat, sprintf('Version                = %s', version()), 'delimiter', '', '-append');
    dlmwrite(mydat, sprintf('Project remote-url     = %s', gitinfo.remote_url), 'delimiter', '', '-append');
    dlmwrite(mydat, sprintf('Project git-sha        = %s', gitinfo.sha), 'delimiter', '', '-append');
    dlmwrite(mydat, sprintf('Project git-branch     = %s', gitinfo.branch), 'delimiter', '', '-append');
    dlmwrite(mydat, sprintf('n                      = %e', n), 'delimiter', '', '-append');
    dlmwrite(mydat, sprintf('alpha                  = %e', alpha), 'delimiter', '', '-append');
    dlmwrite(mydat, sprintf('T                      = %e', T), 'delimiter', '', '-append');
    dlmwrite(mydat, sprintf('h                      = %e', h), 'delimiter', '', '-append');
    dlmwrite(mydat, sprintf('t                      = %f', t), 'delimiter', '', '-append');
    dlmwrite(mydat, sprintf('prec                   = %e', prec), 'delimiter', '', '-append');
    dlmwrite(mydat, sprintf('multiprec              = %d', multiprec), 'delimiter', '', '-append');

    HEADER = 'i,j,ABS_Vk_T_Xk_Vk';
    dlmwrite(mydat, HEADER, 'delimiter', '', '-append');

    for i = 1:n
        for j = 1:n
            dlmwrite(mydat, [i, j, ABS_Vk_T_Xk_V{ti}(i, j)], 'delimiter', ',', '-append', 'precision', prec);
        end
    end

end

% store eigenvalues
mydat_eval = fullfile(mydir, sprintf('%s_Xinf_evals.dat', mybasename));

dlmwrite(mydat_eval, sprintf('mfilename              = %s', mfilename), 'delimiter', '');
dlmwrite(mydat_eval, sprintf('Time                   = %s', timenow), 'delimiter', '', '-append');
dlmwrite(mydat_eval, sprintf('Host                   = %s', hostname), 'delimiter', '', '-append');
dlmwrite(mydat_eval, sprintf('CPU-Name               = %s', mycpuinfo.Name), 'delimiter', '', '-append');
dlmwrite(mydat_eval, sprintf('CPU-Clock              = %s', mycpuinfo.Clock), 'delimiter', '', '-append');
dlmwrite(mydat_eval, sprintf('CPU-Cache              = %s', mycpuinfo.Cache), 'delimiter', '', '-append');
dlmwrite(mydat_eval, sprintf('CPU-NumProcessors      = %d', mycpuinfo.NumProcessors), 'delimiter', '', '-append');
dlmwrite(mydat_eval, sprintf('CPU-OSType             = %s', mycpuinfo.OSType), 'delimiter', '', '-append');
dlmwrite(mydat_eval, sprintf('CPU-OSVersion          = %s', mycpuinfo.OSVersion), 'delimiter', '', '-append');
dlmwrite(mydat_eval, sprintf('Version                = %s', version()), 'delimiter', '', '-append');
dlmwrite(mydat_eval, sprintf('Project remote-url     = %s', gitinfo.remote_url), 'delimiter', '', '-append');
dlmwrite(mydat_eval, sprintf('Project git-sha        = %s', gitinfo.sha), 'delimiter', '', '-append');
dlmwrite(mydat_eval, sprintf('Project git-branch     = %s', gitinfo.branch), 'delimiter', '', '-append');
dlmwrite(mydat_eval, sprintf('n                      = %e', n), 'delimiter', '', '-append');
dlmwrite(mydat_eval, sprintf('alpha                  = %e', alpha), 'delimiter', '', '-append');
dlmwrite(mydat_eval, sprintf('T                      = %e', T), 'delimiter', '', '-append');
dlmwrite(mydat_eval, sprintf('h                      = %e', h), 'delimiter', '', '-append');
dlmwrite(mydat_eval, sprintf('t                      = %f', t), 'delimiter', '', '-append');
dlmwrite(mydat_eval, sprintf('prec                   = %e', prec), 'delimiter', '', '-append');
dlmwrite(mydat_eval, sprintf('multiprec              = %d', multiprec), 'delimiter', '', '-append');

HEADER = 'i,Xinf_lambda';
dlmwrite(mydat_eval, HEADER, 'delimiter', '', '-append');

for i = 1:n
    dlmwrite(mydat_eval, [i, Dinf(i)], 'delimiter', ',', '-append', 'precision', prec);
end

fprintf('%s - Finished writing tikz file\n', datestr(now));
fprintf('%s - Start writing data to mat file\n', datestr(now));
save(mymat);
fprintf('%s - Finished writing data to mat file\n', datestr(now));

%% turn diary off
diary('off');
