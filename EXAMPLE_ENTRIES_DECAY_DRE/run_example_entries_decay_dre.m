%RUN_EXAMPLE_ENTRIES_DECAY_DRE
%
%   Calls example_entries decay_dre for different parameters.
%
%   See also TEST_EXAMPLE_ENTRIES_DECAY_DRE and EXAMPLE_ENTRIES_DECAY_DRE.
%
%   Author: Maximilian Behr (MPI Magdeburg, CSC)

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019
%

%% clear
clearvars, clc, close all

%% parameters
alpha = [1, 5, 10, 20, 50];
n = 10;
T = 15;
k = -5;
multiprec = false;
prec = 32;
plot_visible = false;

%% calls
for alpha_ = alpha
    for n_ = n
        for T_ = T
            for k_ = T
                for multiprec_ = multiprec
                    for prec_ = prec
                        for plotting_ = plot_visible
                            example_entries_decay_dre(alpha_, n_, T_, k_, multiprec_, prec_, plotting_);
                        end
                    end
                end
            end
        end
    end
end
