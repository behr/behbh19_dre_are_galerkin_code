#!/bin/bash
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#
# Copyright (C) Maximilian Behr
#               2019
#

# SETUP Your MATLAB environment
MATLAB=matlab2018a

# PARAMETER COMBINATIONS
ALPHAS=(5)
NS=(100)
TS=(15)
KS=(-5)
PRECS=(128)

for ALPHA in "${ALPHAS[@]}"; do
    for N in "${NS[@]}"; do
        for T in "${TS[@]}"; do
            for K in "${KS[@]}"; do
                for PREC in "${PRECS[@]}"; do
                    # execute matlab command
                    ${MATLAB} -nodesktop -nosplash -r "run ../add_to_path; example_entries_decay_dre(${ALPHA}, ${N}, ${T}, ${K}, true, ${PREC}, false); quit();" &
                done
            done
        done
    done
done
