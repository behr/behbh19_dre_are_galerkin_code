#!/bin/bash

#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#
# Copyright (C) Maximilian Behr
#               2016-2018
#

function trim_trailing_space() {
  local file=$1
  unamestr=$(uname)
  if [[ $unamestr == 'Darwin' ]]; then
    #specific case for Mac OSX
    sed -E -i ''  's/[[:space:]]*$//' $file
  else
    sed -i  's/[[:space:]]*$//' $file
  fi
}

# remove trailing whitespaces from m files
for f in $(find . -type f -name "*.m" ! -path "*cmess*" ! -path "*mess_build**"); do
    echo "Remove trailing whitespaces from $f"
    trim_trailing_space $f
done

# remove trailing whitespaces from sh files
for f in $(find . -type f -name "*.sh" ! -path "*cmess*" ! -path "*mess_build**"); do
    echo "Remove trailing whitespaces from $f"
    trim_trailing_space $f
done

# remove trailing whitespaces from job files
for f in $(find . -type f -name "*.job" ! -path "*cmess*" ! -path "*mess_build**"); do
    echo "Remove trailing whitespaces from $f"
    trim_trailing_space $f
done

