%RUN_COMPARE_DRE_SOLVERS
%
%   Calls compare_dre_solvers for different parameters.
%
%   See also COMPARE_DRE_SOLVERS and RUN_COMPARE_DRE_SOLVERS_REFERENCE_SOLUTION.
%
%   Author: Maximilian Behr (MPI Magdeburg, CSC)

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019
%

%% clear
clearvars, clc, close all

%% parameters
%instances = {'rail_371', 'rail_1357', 'rail_5177', 'rail_20209', 'rail_79841'};
instances = {'rail_371'};
T = 1;
k_ref = -6;
k_tests = [-2, -4];

%% parfor on/off
control_parfor(false);

for k_test = k_tests

    error_at = 2^k_test:2^k_test:T;
    parallel_for = false;


    for i_instance = 1:length(instances)
        instance = instances{i_instance};
        data = load(instances{i_instance});
        A = data.A;
        E = data.E;
        B = full(data.B);
        C = full(data.C);
        n = size(A, 1);

        % reference solver
        solver_ref_opt = splitting_dre_options();
        solver_ref_opt.T = T;
        solver_ref_opt.k = k_ref;
        solver_ref_opt.krylov_tol = 1e-9;
        solver_ref_opt.CC_tol = n * eps;
        solver_ref_opt.splitting = splitting_dre_t.SPLITTING_SYMMETRIC8;

        solver_test_opt = splitting_dre_options();
        solver_test_opt.T = T;
        solver_test_opt.k = k_test;
        solver_test_opt.krylov_tol = 1e-9;
        solver_test_opt.CC_tol = n * eps;
        solver_test_opt.splitting = splitting_dre_t.SPLITTING_LIE;

        %% name
        name = sprintf('%s_%s_k%d_%s_k%d_T%.2f', instance, char(solver_ref_opt.splitting), k_ref, class(solver_test_opt), k_test, T);
        name = regexprep(name, 'options_', ''); % remove options_ part from name

        %% calls
        compare_dre_solvers(A, E, B, C, solver_ref_opt, solver_test_opt, error_at, name);
    end

end
