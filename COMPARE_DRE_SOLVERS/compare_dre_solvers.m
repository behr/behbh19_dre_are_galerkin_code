function compare_dre_solvers(A, E, B, C, solver_ref_opt, solver_test_opt, error_at, name)
%COMPARE_DRE_SOLVERS    Compare two differential Riccati equation solvers.
%
%   The differential Riccati equation is given by
%
%       E' \dot{X} E = A' X E + E' X A - E' X B B' X E + C' C,  X(0)=0.
%
%   COMPARE_DRE_SOLVER(A, E, B, C, SOLVER_REF_OPT, SOLVER_TEST_OPT, ERROR_AT, NAME)
%   The matrices A, E, B, C defines the differential Riccati equation.
%   A and E must be doule, real, sparse and quadratic.
%   If E is empty the identity matrix is assumed. B and C must be real
%   and dense.
%   SOLVER_REF_OPT must be either an instance of read_reference_solution_dre
%   or an instance of a subclass of solver_dre_options.
%   SOLVER_TEST_OPT must be an instance of a subclass
%   of solver_dre_options. The solver is then created from the given
%   options. ERROR_AT is a vector which contains the time points
%   for comparision both solution. At these points the norm
%   of the difference of both solution and the norm of the solutions
%   itself is computed.
%   ERROR_AT must contain only points multiple of the step size of
%   both solver.  NAME is the name of the output directory.
%
%   See also RUN_COMPARE_DRE_SOLVERS and RUN_COMPARE_DRE_SOLVERS_REFERENCE_SOLUTION.
%
%   Author: Maximilian Behr (MPI Magdeburg, CSC)

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019
%

%% some input args checks
assert(ischar(name));

% class checks
isa_reader = isa(solver_ref_opt, 'read_reference_solution_dre');
assert(isa(solver_ref_opt, 'solver_dre_options') || isa_reader);
assert(isa(solver_test_opt, 'solver_dre_options'));

assert(isnumeric(error_at) && isrow(error_at) && issorted(error_at));

% check consistency of error_at
if isa_reader
    assert(max(solver_ref_opt.store_at) >= solver_test_opt.T, 'Time Horizon for precomputed solution too short.');
    T = solver_test_opt.T;
    k_test = solver_test_opt.k;
    assert(~any(mod(error_at, 2^k_test)), 'error_at must only contain multiples of stepsize');
    assert(all(ismember(error_at, solver_ref_opt.store_at)), 'error_at must be a subset of store_at for error computations');
else
    assert(solver_ref_opt.T == solver_test_opt.T, 'Different Time Intervalls');
    T = solver_test_opt.T;
    k_ref = solver_ref_opt.k;
    k_test = solver_test_opt.k;
    assert(~any(mod(error_at, 2^k_ref)), 'error_at must only contain multiples of stepsize');
    assert(~any(mod(error_at, 2^k_test)), 'error_at must only contain multiples of stepsize');
end
assert(all(error_at <= T), 'error_at be smaller than T');
assert(all(0 < error_at), 'error_at larger than 0');

n = size(A, 1);

%% create necessary filenames and create directory for results
mybasename = sprintf('%s', name);
[mydir, ~, ~] = fileparts(mfilename('fullpath'));
mydir = fullfile(mydir, 'results', mfilename, mybasename);
[status, message, messageid] = rmdir(mydir, 's');
if ~status
    fprintf('%s - %s:%s-%s\n', datestr(now), mfilename, message, messageid);
end

mydiary = fullfile(mydir, sprintf('%s.log', mybasename));
mydat = fullfile(mydir, sprintf('%s.dat', mybasename));
mymat = fullfile(mydir, sprintf('%s.mat', mybasename));
mkdir(mydir);

%% delete old diary and create new one
delete(mydiary);
diary(mydiary);

%% call mess_version to get its output logged by diary
mess_version();

%% generate solver for reference solution
fprintf('%s - Generate solver for reference solution.\n', datestr(now));
if ~isa_reader
    if isa(solver_ref_opt, 'splitting_dre_options')
        solver_ref = splitting_dre(A, E, B, C, zeros(n, 1), solver_ref_opt);
        solver_ref_descr = sprintf('%s-%s-k%d', upper(class(solver_ref)), solver_ref_opt.splitting, solver_ref_opt.k);
    elseif isa(solver_ref_opt, 'are_galerkin_dre_options')
        solver_ref = are_galerkin_dre(A, E, B, C, solver_ref_opt);
        solver_ref_descr = sprintf('%s-k%d', upper(class(solver_ref)), solver_ref_opt.k);
    end
    fprintf('%s - Prepare Solver %s for reference solution.\n', datestr(now), solver_ref_descr);
    solver_ref.prepare();
else
    fprintf('%s - Got a read_reference_solution_dre instance, nothing to do.\n', datestr(now));
    solver_ref = solver_ref_opt;
    solver_ref_descr = solver_ref.mymat;
end

%% generate solver for test solution
fprintf('%s - Generate solver for test solution.\n', datestr(now));
if isa(solver_test_opt, 'splitting_dre_options')
    solver_test = splitting_dre(A, E, B, C, zeros(n, 1), solver_test_opt);
    solver_test_descr = sprintf('%s-%s-k%d', upper(class(solver_test)), solver_test_opt.splitting, solver_test_opt.k);
elseif isa(solver_test_opt, 'are_galerkin_dre_options')
    solver_test = are_galerkin_dre(A, E, B, C, solver_test_opt);
    solver_test_descr = sprintf('%s-k%d', upper(class(solver_test)), solver_test_opt.k);
end
fprintf('%s - Prepare Solver %s for test solution.\n', datestr(now), solver_test_descr);
solver_test.prepare();

%% solve algebraic riccati equation to monitor convergence of the reference solution
Zinf = mess_care(A, E, B, C);

%% prepare for error computations
t_absnrm2_err = zeros(length(error_at), 1);
t_relnrm2_err = zeros(length(error_at), 1);
t_absnrmF_err = zeros(length(error_at), 1);
t_relnrmF_err = zeros(length(error_at), 1);

t_nrm2_Xref = zeros(length(error_at), 1);
t_nrm2_Xtest = zeros(length(error_at), 1);
t_nrmF_Xref = zeros(length(error_at), 1);
t_nrmF_Xtest = zeros(length(error_at), 1);

t_absnrmF_err_Xbest = zeros(length(error_at), 1);
t_relnrmF_err_Xbest = zeros(length(error_at), 1);
t_nrmF_Xbest = zeros(length(error_at), 1);

t_cols_ref = zeros(length(error_at), 1);
t_cols_test = zeros(length(error_at), 1);

t_diffnrm2_Xinf_ref = zeros(length(error_at), 1);

%% get length of both solver descr for pretty priting
length_solver_descr = max(length(solver_ref_descr), length(solver_test_descr));

%% solve and compare solutions
i = 1;
for t_error = error_at
    
    % integrate to desired time with reference solver
    if ~isa_reader
        fprintf('%s - Integrate with reference solver: %*s, n=%d, t=%.10f, T=%.10f, %6.2f %%.\n', datestr(now), length_solver_descr, solver_ref_descr, n, t_error, T, t_error/T*100);
        solver_ref.time_step_until(t_error);
    end
    
    % intergrate to desired time with solver to test
    fprintf('%s - Integrate with test solver:      %*s, n=%d, t=%.10f, T=%.10f, %6.2f %%.\n', datestr(now), length_solver_descr, solver_test_descr, n, t_error, T, t_error/T*100);
    solver_test.time_step_until(t_error);
    
    % check if time is equal, get solutions from solvers
    if ~isa_reader
        assert(solver_ref.t == solver_test.t, 'Solvers are not synchron anymore.');
    end
    
    % get solution
    fprintf('%s - t = %.10f get solutions.\n', datestr(now), solver_test.t);
    if isa_reader
        [Q_ref, X_ref] = solver_ref.get_solution_at(solver_test.t);
    else
        [Q_ref, X_ref] = solver_ref.get_solution();
    end
    [Q_test, X_test] = solver_test.get_solution();
    
    % compute 2-norm error
    fprintf('%s - t = %.10f compare solutions.\n', datestr(now), solver_test.t);
    nrm2_Xref = two_norm_sym_handle(@(x) (Q_ref * (X_ref * (Q_ref' * x))), n);
    nrm2_Xtest = two_norm_sym_handle(@(x) (Q_test * (X_test * (Q_test' * x))), n);
    absnrm2_err = two_norm_sym_handle(@(x) (Q_test * (X_test * (Q_test' * x)))-(Q_ref * (X_ref * (Q_ref' * x))), n);
    relnrm2_err = absnrm2_err / nrm2_Xref;
    
    % compute F-norm error
    nrmF_Xref = fronorm_outer(Q_ref, X_ref);
    nrmF_Xtest = fronorm_outer(Q_test, X_test);
    absnrmF_err = fronorm_outer_diff(Q_ref, X_ref, Q_test, X_test);
    relnrmF_err = absnrmF_err / nrmF_Xtest;
    
    % compute best Approximation of reference in solution in Frobenius norm
    % if the test solver is a galerkin based solver, i.e. are_galerkin_dre
    if isa(solver_test_opt, 'are_galerkin_dre_options')
        temp = Q_test' * Q_ref;
        X_best = temp * X_ref * temp';
        nrmF_Xbest = fronorm_outer(Q_test, X_best);
        absnrmF_err_Xbest = fronorm_outer_diff(Q_test, X_best, Q_ref, X_ref);
        relnrmF_err_Xbest = absnrmF_err_Xbest / nrmF_Xref;
        t_absnrmF_err_Xbest(i) = absnrmF_err_Xbest;
        t_relnrmF_err_Xbest(i) = relnrmF_err_Xbest;
        t_nrmF_Xbest(i) = nrmF_Xbest;
    end
    
    % get size of low rank solution
    cols_ref = size(Q_ref, 2);
    cols_test = size(Q_test, 2);
    
    % compute ||X_ref - X_inf ||_2, distance to stationary point
    fprintf('%s - t = %.10f compute distance to stationary point.\n', datestr(now), solver_test.t);
    diffnrm2_Xinf_ref = two_norm_sym_handle(@(x) (Q_ref * (X_ref * (Q_ref' * x))) - (Zinf*(Zinf'*x)), n);
    
    % print info
    fprintf('%s - t = %.10f absnrm2_err = %e, relnrm2_err = %e, nrm2_Xref  = %e, nrm2_Xtest = %e.\n', datestr(now), t_error, absnrm2_err, relnrm2_err, nrm2_Xref, nrm2_Xtest);
    fprintf('%s - t = %.10f absnrmF_err = %e, relnrmF_err = %e, nrmF_Xref  = %e, nrmF_Xtest = %e.\n', datestr(now), t_error, absnrmF_err, relnrmF_err, nrmF_Xref, nrmF_Xtest);
    if isa(solver_test_opt, 'are_galerkin_dre_options')
        fprintf('%s - t = %.10f absnrmF_err = %e, relnrmF_err = %e, nrmF_Xbest = %e (Best Approximation in Frobenius Norm of Reference solution).\n', datestr(now), t_error, absnrmF_err_Xbest, relnrmF_err_Xbest, nrmF_Xbest);
    end
    fprintf('%s - cols_ref = %d, cols_test = %d\n', datestr(now), cols_ref, cols_test);
    fprintf('%s - || X_ref - Xinf ||_2 = %e\n', datestr(now), diffnrm2_Xinf_ref);
    
    % log errors
    t_absnrm2_err(i) = absnrm2_err;
    t_relnrm2_err(i) = relnrm2_err;
    t_nrm2_Xref(i) = nrm2_Xref;
    t_nrm2_Xtest(i) = nrm2_Xtest;
    
    t_absnrmF_err(i) = absnrmF_err;
    t_relnrmF_err(i) = relnrmF_err;
    t_nrmF_Xref(i) = nrmF_Xref;
    t_nrmF_Xtest(i) = nrmF_Xtest;
    
    t_cols_ref(i) = cols_ref;
    t_cols_test(i) = cols_test;
    
    t_diffnrm2_Xinf_ref(i) = diffnrm2_Xinf_ref;
    i = i + 1;
    
end

%% Write to file for tikz and mat
fprintf('%s - Start writing data to tikz file\n', datestr(now));
hostname = getComputerName();
timenow = datestr(now);
gitinfo = GitInfo();
mycpuinfo = cpuinfo();
cmess_git_branch = mess_git_branch();
cmess_git_id = mess_git_id();

% meta data
dlmwrite(mydat, sprintf('mfilename                  = %s', mfilename), 'delimiter', '');
dlmwrite(mydat, sprintf('Time                       = %s', timenow), 'delimiter', '', '-append');
dlmwrite(mydat, sprintf('Host                       = %s', hostname), 'delimiter', '', '-append');
dlmwrite(mydat, sprintf('CPU-Name                   = %s', mycpuinfo.Name), 'delimiter', '', '-append');
dlmwrite(mydat, sprintf('CPU-Clock                  = %s', mycpuinfo.Clock), 'delimiter', '', '-append');
dlmwrite(mydat, sprintf('CPU-Cache                  = %s', mycpuinfo.Cache), 'delimiter', '', '-append');
dlmwrite(mydat, sprintf('CPU-NumProcessors          = %d', mycpuinfo.NumProcessors), 'delimiter', '', '-append');
dlmwrite(mydat, sprintf('CPU-OSType                 = %s', mycpuinfo.OSType), 'delimiter', '', '-append');
dlmwrite(mydat, sprintf('CPU-OSVersion              = %s', mycpuinfo.OSVersion), 'delimiter', '', '-append');
dlmwrite(mydat, sprintf('Version                    = %s', version()), 'delimiter', '', '-append');
dlmwrite(mydat, sprintf('Project remote-url         = %s', gitinfo.remote_url), 'delimiter', '', '-append');
dlmwrite(mydat, sprintf('Project git-sha            = %s', gitinfo.sha), 'delimiter', '', '-append');
dlmwrite(mydat, sprintf('Project git-branch         = %s', gitinfo.branch), 'delimiter', '', '-append');
dlmwrite(mydat, sprintf('C-M.E.S.S. git-sha         = %s', cmess_git_branch), 'delimiter', '', '-append');
dlmwrite(mydat, sprintf('C-M.E.S.S. git-branch      = %s', cmess_git_id), 'delimiter', '', '-append');
dlmwrite(mydat, sprintf('n                          = %d', n), 'delimiter', '', '-append');
dlmwrite(mydat, sprintf('name                       = %s', name), 'delimiter', '', '-append');
dlmwrite(mydat, sprintf('T                          = %e', T), 'delimiter', '', '-append');

% data for reference and test solver
for i_solver = 1:2
    if i_solver == 1
        dlmwrite(mydat, 'REFRENCE SOLVER', 'delimiter', '', '-append');
        temp_solver = solver_ref;
        descr = solver_ref_descr;
    else
        dlmwrite(mydat, 'TEST SOLVER', 'delimiter', '', '-append');
        temp_solver = solver_test;
        descr = solver_test_descr;
    end
    
    k = inf;
    wtime_prepare = 0;
    wtime_time_step_until = 0;
    wtime_solve_are = 0;
    wtime_prepare_solver_mod = 0;
    trunc_abs2res_are = 0;
    trunc_rel2res_are = 0;
    Zcols = 0;
    nrm1_exp_hM = 0;
    
    if ~isa(temp_solver, 'read_reference_solution_dre')
        k = temp_solver.options.k;
        wtime_prepare = temp_solver.wtime_prepare;
        wtime_time_step_until = temp_solver.wtime_time_step_until;
    end
    
    if isa(temp_solver, 'are_galerkin_dre')
        wtime_solve_are = temp_solver.time_solve_are;
        wtime_prepare_solver_mod = temp_solver.time_prepare_solver_mod;
        trunc_abs2res_are = temp_solver.trunc_abs2res_are;
        trunc_rel2res_are = temp_solver.trunc_rel2res_are;
        Zcols = size(temp_solver.Z, 2);
        nrm1_exp_hM = temp_solver.nrm1_exp_hM;
    end
    
    dlmwrite(mydat, sprintf('k                          = %d', k), 'delimiter', '', '-append');
    dlmwrite(mydat, sprintf('class                      = %s', upper(class(temp_solver))), 'delimiter', '', '-append');
    dlmwrite(mydat, sprintf('description                = %s', descr), 'delimiter', '', '-append');
    dlmwrite(mydat, sprintf('wtime_prepare              = %e', wtime_prepare), 'delimiter', '', '-append');
    dlmwrite(mydat, sprintf('wtime_time_step_until      = %e', wtime_time_step_until), 'delimiter', '', '-append');
    dlmwrite(mydat, sprintf('wtime_solve_are            = %e', wtime_solve_are), 'delimiter', '', '-append');
    dlmwrite(mydat, sprintf('wtime_prepare_solver_mod   = %e', wtime_prepare_solver_mod), 'delimiter', '', '-append');
    dlmwrite(mydat, sprintf('trunc_abs2res_are          = %e', trunc_abs2res_are), 'delimiter', '', '-append');
    dlmwrite(mydat, sprintf('trunc_rel2res_are          = %e', trunc_rel2res_are), 'delimiter', '', '-append');
    dlmwrite(mydat, sprintf('Zcols                      = %d', Zcols), 'delimiter', '', '-append');
    dlmwrite(mydat, sprintf('nrm1_exp_hM                = %e', nrm1_exp_hM), 'delimiter', '', '-append');
end

% data of tracked error
HEADER = 't, ABSNRM2_ERR, RELNRM2_ERR, NRM2_XREF, NRM2_XTEST, ABSNRMF_ERR, RELNRMF_ERR, NRMF_XREF, NRMF_XTEST, ABSNRMF_ERR_XBEST, RELNRMF_ERR_XBEST,  NRMF_XBEST, COLS_REF, COLS_TEST, DIFFNRM2_XREF_XINF';
dlmwrite(mydat, HEADER, 'delimiter', '', '-append');

for i = 1:length(error_at)
    dlmwrite(mydat, [ ...
        error_at(i), ...
        t_absnrm2_err(i), t_relnrm2_err(i), t_nrm2_Xref(i), t_nrm2_Xtest(i), ...
        t_absnrmF_err(i), t_relnrmF_err(i), t_nrmF_Xref(i), t_nrmF_Xtest(i), ...
        t_absnrmF_err_Xbest(i), t_relnrmF_err_Xbest(i), t_nrmF_Xbest(i), ...
        t_cols_ref(i), t_cols_test(i), ...
        t_diffnrm2_Xinf_ref(i)], ...
        'delimiter', ',', '-append', 'precision', 16);
end

fprintf('%s - Finished writing tikz file\n\n', datestr(now));

fprintf('%s - Start writing mat file\n', datestr(now));
mymatfile = matfile(mymat);

% meta data
mymatfile.name = name;
mymatfile.hostname = hostname;
mymatfile.timenow = timenow;
mymatfile.gitinfo = gitinfo;
mymatfile.mycpuinfo = mycpuinfo;
mymatfile.version = version();
mymatfile.cmess_git_id = cmess_git_id;
mymatfile.cmess_git_branch = cmess_git_branch;
mymatfile.error_at = error_at;

% 2-norm errors
mymatfile.t_abs2nrm_err = t_absnrm2_err;
mymatfile.t_rel2nrm_err = t_relnrm2_err;
mymatfile.t_nrm2_Xref = t_nrm2_Xref;
mymatfile.t_nrm2_Xtest = t_nrm2_Xtest;

% F-norm errors
mymatfile.t_absFnrm_err = t_absnrm2_err;
mymatfile.t_relFnrm_err = t_relnrm2_err;
mymatfile.t_nrmF_Xref = t_nrm2_Xref;
mymatfile.t_nrmF_Xtest = t_nrm2_Xtest;

% Best Approximation in Frobenius Norm of Reference Solution
mymatfile.t_absnrmF_err_Xbest = t_absnrmF_err_Xbest;
mymatfile.t_relnrmF_err_Xbest = t_relnrmF_err_Xbest;
mymatfile.t_nrmF_Xbest = t_nrmF_Xbest;

% Low Rank Faktor Sizes
mymatfile.t_cols_ref = t_cols_ref;
mymatfile.t_cols_test = t_cols_test;

% 2-norm distance to stationary points
mymatfile.t_diffnrm2_Xinf_ref = t_diffnrm2_Xinf_ref;

% Solver Options
if isa_reader
    mymatfile.solver_ref = solver_ref;
    mymatfile.solver_ref_struct = struct(solver_ref);
else
    mymatfile.solver_ref_opt = solver_ref.options;
    mymatfile.solver_ref_opt_struct = struct(solver_ref.options);
end

mymatfile.solver_test_opt = solver_test.options;
mymatfile.solver_test_opt_struct = struct(solver_test.options);

fprintf('%s - Finished writing mat file\n\n', datestr(now));

%% turn diary off
diary('off');

end
