%RUN_COMPARE_DRE_SOLVERS
%
%   Calls compare_dre_solvers for different given reference solution.
%
%   See also COMPARE_DRE_SOLVERS and RUN_COMPARE_DRE_SOLVERS.
%
%   Author: Maximilian Behr (MPI Magdeburg, CSC)


% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019
%

%% clear
clearvars, clc, close all

%% parameters
instance = 'conv_diff3_100';
reference_solution = 'conv_diff3_100_SPLITTING_SYMMETRIC8_k-15_T0.125000.mat';
T = 1/2^10;
k_test = -14;
error_at = 2^(k_test):2^(k_test):T;

%% prepare compare_dre_solver
data = load(instance);
A = data.A;
E = speye(size(A));
if isfield(data,'E')
    E = data.E;    
end
B = full(data.B);
C = full(data.C);
n = size(A, 1);

% reference solution
solver_ref = read_reference_solution_dre(reference_solution);

% comparison solver
solver_test_opt = splitting_dre_options();
solver_test_opt.T = T;
solver_test_opt.k = k_test;
solver_test_opt.krylov_tol = 1e-9;
solver_test_opt.CC_tol = n * eps;
solver_test_opt.splitting = splitting_dre_t.SPLITTING_SYMMETRIC8;

% name for example
name = sprintf('%s_%s_VS_%s_k%d_T%.8f', instance, char(solver_ref.name), char(solver_test_opt.splitting), k_test, T);
name = regexprep(name, 'options_', ''); % remove options_ part from name

% comparison solver
%solver_test_opt = are_galerkin_dre_options();                                                                   
%solver_test_opt.T = T;                                                                                          
%solver_test_opt.k = k_test;                                                                                     

% name for example
%name = sprintf('%s_%s_VS_%s_k%d_T%.8f', instance, char(solver_ref.name), class(solver_test_opt), k_test, T);    
%name = regexprep(name, 'options_', '');                                                                         
 
%% calls
control_parfor(false);
compare_dre_solvers(A, E, B, C, solver_ref, solver_test_opt, error_at, name);
