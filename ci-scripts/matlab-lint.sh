#!/bin/bash

#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#
# Copyright (C) Maximilian Behr
#               2016-2019
#


set -ex

# install packages
apt-get update --yes -qq
apt-get install --yes -qq cmake libopenblas-dev libsuitesparse-dev zlib1g-dev libbz2-dev liblzma-dev
apt-get install --yes -qq libarpack2-dev libmatio-dev libsuperlu-dev
apt-get install --yes -qq libfontconfig1 libxcomposite1

apt-get install --yes -qq  software-properties-common
add-apt-repository --yes --update  ppa:ubuntu-toolchain-r/test

# get matlab root
MROOT=$(matlab -n | grep -P "MATLAB\s+=" | awk '{print $5}')
echo "MROOT = ${MROOT}"

# get matlab version
MVERSION=$(matlab -nojvm -nodesktop -debug -nosplash -r "disp(sprintf('MATLABVERSION = %s',version('-release')));exit()" | grep -P "MATLABVERSION\s+=" | awk '{print $3}')
echo "MVERSION = ${MVERSION}"

# set gcc and g++
if [[ $MVERSION == *"2016b"* ]] || [[ $MVERSION == *"2017"* ]]; then
    apt-get install --yes -qq cmake gcc-4.9 g++-4.9 gfortran-4.9
    echo "Export gcc and g++ for 2016b/2017a/2017b"
    export CC=gcc-4.9
    export CXX=g++-4.9
    export FC=gfortran-4.9
fi

if [[ $MVERSION == *"2018"* ]] || [[ $MVERSION == *"2019"* ]]; then
    apt-get install --yes -qq cmake gcc-6 g++-6 gfortran-6
    echo "Export gcc and g++ for 2018a/2018b/2019a/2019b"
    export CC=gcc-6
    export CXX=g++-6
    export FC=gfortran-6
fi


# compile mex-mess
CMESS_SRC=cmess-releases
CMESS_BUILD=mess_build
CMAKE=cmake
ls -hla
pwd
rm -rf ${CMESS_BUILD}
mkdir ${CMESS_BUILD}
cd ${CMESS_BUILD}
${CMAKE} ../${CMESS_SRC} -DDEBUG=OFF -DSUPERLU=OFF -DARPACK=ON -DOPENMP=OFF -DMATLAB=ON -DMATLAB_ROOT=${MROOT}
make
cd ..

#run lint code checker for project
${MROOT}/bin/matlab -nodesktop -nosplash -r "addpath('$HOME'); run add_to_path; ret=lint_my_code(); exit(ret)"
