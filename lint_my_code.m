%LINT_MY_CODE   Call generates a lint report.
%
%   This function has internal use only.
%   Adapt blacklist cell array in lint_my_code to exclude files.
%
%   Author: Maximilian Behr (MPI Magdeburg, CSC)

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019
%


function ret = lint_my_code()

%% check version
if verLessThan('matlab', '9.2')
    error('lint_my_code needs at least MATLAB 2017a');
end

%% exclude files from lint checking
blacklist = {'SPLITTING_DRE/DREsplit/*', 'HELPERS/*', 'cmess-releases/*', 'mess_build/*', 'mmread/*', 'lint_my_code\.m', 'fdm_2d_matrix\.m', 'fdm_2d_vector\.m'};

%% get all m-files in subdirectories
lint_dir = fileparts(mfilename('fullpath'));
mfiles = dir(fullfile(lint_dir, '**', '*.m'));

%% create full file name
mfilesfull = cell(0);
k = 1;
for i = 1:numel(mfiles)
    
    % get file name
    file = fullfile(mfiles(i).folder, mfiles(i).name);
    
    %% check if file is blacklisted and add if not
    onblacklist = false;
    for j = 1:numel(blacklist)
        if ~isempty(regexpi(file, blacklist{j}))
            % mark this file as blacklisted
            onblacklist = true;
            fprintf('lint_my_code: File on blacklist, not included for report: %s\n', file);
        end
    end
    
    if ~onblacklist
        mfilesfull{k} = file;
        k = k + 1;
    end
end

%% create lint report as printable string
report = checkcode(mfilesfull, '-string');

%% create lint report as cell array
report_data = checkcode(mfilesfull);

%% show lint report
fprintf('============= %s: checkcode report for Project=============\n', datestr(now));
fprintf('\n');
fprintf('%s', report);

%% check report for errors / warnings
ret = 0;
for i = 1:numel(report_data)
    if ~isempty(report_data{i})
        ret = 1;
        return
    end
end
end
