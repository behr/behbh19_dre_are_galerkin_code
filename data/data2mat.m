%DATA2MAT   Read matrices from mtx file in data directory
%   and write to mat binary files.
%
%   Author: Maximilian Behr (MPI Magdeburg, CSC)

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019
%

%% clear all
clearvars, clc, close all

%% read rail matrices
basenames = {'rail_371', 'rail_1357', 'rail_5177', 'rail_20209'};
for i_b = 1:length(basenames)
    basename = basenames{i_b};

    mat = @(name) sprintf('%s.%s', basename, name);
    A = mmread(which(mat('A')));
    E = mmread(which(mat('E')));
    B = mmread(which(mat('B')));

    if strcmp(basename, 'spiral_inductor_peec')
        B = full(B);
        C = full(B)';
    else
        C = mmread(which(mat('C')));
        B = full(B);
        C = full(C);
    end

    % create filename for mat file
    [datadir, dataname, datapext] = fileparts(which(mat('A')));
    datamatname = fullfile(datadir, sprintf('%s.mat', basename));
    save(datamatname, 'A', 'E', 'B', 'C');
end

%% convection diffusion problem 1-4 with differenz sizes
sizes = 5:5:100;
problems = [1, 2, 3, 4];
[datadir, ~, ~] = fileparts(mfilename('fullpath'));
datadir = fullfile(datadir, 'conv_diff');
for problem = problems
    for size = sizes
        [A, B, C] = conv_diff(problem, size);
        B = full(B);
        C = full(C);
        datamatname = fullfile(datadir, sprintf('conv_diff%d_%d.mat', problem, size));
        save(datamatname, 'A', 'B', 'C');
    end
end
