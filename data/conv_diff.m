%CONV_DIFF   Generates matrices for a convection-diffusion
%       problem.
%
%   [A, B, C] = conv_diff(PROBLEM, N) uses finite-difference
%   method to discretice a convection-diffusion problem on
%   the unit square. N is the number of grid nodes in each
%   dimension. PROBLEM describes which preconfigures
%   problem setting is wanted.
%   Possible values for Problem are: 1.
%
%   References:
%   https://morwiki.mpi-magdeburg.mpg.de/morwiki/index.php/Convection-Diffusion
%
%   See also FDM_2D_MATRIX and FDM_2D_VECTOR.
%
%   Author: Maximilian Behr (MPI Magdeburg, CSC)

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019
%

function [A, B, C] = conv_diff(problem, N)

%% check input arguments
validateattributes(problem, {'double'}, {'real', 'scalar', 'integer', 'nonnan', 'finite', 'nonsparse'}, mfilename, inputname(1));
validateattributes(N, {'double'}, {'real', '>', 0, 'scalar', 'nonnan', 'finite', 'nonsparse'}, mfilename, inputname(2));

%% check problem number and discretize problem
if problem == 1
    A = fdm_2d_matrix(N, '1*x', '10*y', '0');
    B = fdm_2d_vector(N, '.1<x<=.3');
    C = fdm_2d_vector(N, '.7<x<=.9')';
elseif problem == 2
    A = fdm_2d_matrix(N, '5*x', '50*y', '0');
    B = fdm_2d_vector(N, '.1<x<=.3');
    C = fdm_2d_vector(N, '.7<x<=.9')';
elseif problem == 3
    A = fdm_2d_matrix(N, '10*x', '100*y', '0');
    B = fdm_2d_vector(N, '.1<x<=.3');
    C = fdm_2d_vector(N, '.7<x<=.9')';
elseif problem == 4
    A = fdm_2d_matrix(N, '20*x', '200*y', '0');
    B = fdm_2d_vector(N, '.1<x<=.3');
    C = fdm_2d_vector(N, '.7<x<=.9')';
else
    error('Problem %d is not implemented', problem);
end
end
