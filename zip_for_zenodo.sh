#!/bin/bash

#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#
# Copyright (C) Maximilian Behr
#               2016-2019
#

# Create a zip file from repo for zenodo upload
zip -r behbh19_dre_are_galerkin.zip \
    ABSTRACT                    \
    ARE_GALERKIN_DRE            \
    build_cmess.sh              \
    clear_results.sh            \
    COMPARE_DRE_SOLVERS         \
    data                        \
    data_reference_solution     \
    EXAMPLE_DAVISON_MAKI_FAIL   \
    EXAMPLE_EIG_DECAY_ARE       \
    EXAMPLE_EIG_DECAY_DRE       \
    EXAMPLE_ENTRIES_DECAY_DRE   \
    HELPERS                     \
    LICENSE                     \
    lint_my_code.m              \
    mmread                      \
    MODIFIED_DAVISON_MAKI_DRE   \
    README.md                   \
    REFERENCE_SOLUTION          \
    run_my_tests.m              \
    SPLITTING_DRE
