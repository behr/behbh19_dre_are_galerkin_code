#!/bin/bash

#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#
# Copyright (C) Maximilian Behr
#               20016-2019
#

scriptdir="$(dirname "$0")"
cd "$scriptdir"

CMESS_SRC=cmess-releases
CMESS_BUILD=mess_build
CMAKE=cmake3

# create build folder
rm -rf ${CMESS_BUILD}
mkdir ${CMESS_BUILD}

if [ "$HOSTNAME" == "mechthild01.service" ]; then
    MATLAB=matlab
elif [ "$HOSTNAME" == "jack" ]; then
    MATLAB=mpi_matlab2018a
    CMAKE=cmake
elif [ "$HOSTNAME" == "editha" ]; then
    MATLAB=matlab2018a
    CMAKE=cmake
elif [ "$HOSTNAME" == "pc1184" ]; then
    MATLAB=matlab2018a
    CMAKE=cmake
else

    MATLAB=matlab2018a
fi

# get matlab root
MROOT=$(${MATLAB} -n | grep -P "MATLAB\s+=" | awk '{print $5}')

#configure build and compile mexmess
cd ${CMESS_BUILD}

if [ "$HOSTNAME" == "mechthild01.service" ]; then
    ${CMAKE} ../${CMESS_SRC} -DDEBUG=OFF -DSUPERLU=OFF -DARPACK=ON -DOPENMP=OFF -DMATLAB=ON -DMATLAB_ROOT=${MROOT} -DSUITESPARSE=~/soft/
else
    ${CMAKE} ../${CMESS_SRC} -DDEBUG=OFF -DSUPERLU=OFF -DARPACK=ON -DOPENMP=OFF -DMATLAB=ON -DMATLAB_ROOT=${MROOT}
fi

make


