#!/bin/bash

#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#
# Copyright (C) Maximilian Behr
#               2016-2019
#

# Script adds Symlinks to the *.mat files, which contains the precomputed reference solutions.
#
# Usage:    ./add_reference_solutions.sh <PATH_TO_THE_REFERENCE_SOLUTIONS>
# Example:  ./add_reference_solutions.sh /vol1/reference_solution_dre_new/
#

SEARCH_DIR=$1
echo "Search for reference solutions (*.mat files) in ${SEARCH_DIR}"

REFERENCE_SOLUTIONS=($(find ${SEARCH_DIR} -name "*.mat" -type f))
echo "Found the following *.mat files:"
for REF in "${REFERENCE_SOLUTIONS[@]}"; do
    echo -e "\t${REF}"
done

WORK_DIR=$(pwd)
read -p "Should all listed files be added as symlinks to the directory ${WORKDIR}[Y/N]?" -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then

    for REF in "${REFERENCE_SOLUTIONS[@]}"; do
        REF_FILENAME=$(basename ${REF})
        echo -e "\tCreated symlink ${REF_FILENAME} -> ${REF}"
        ln -s ${REF} ${REF_FILENAME}
    done

fi


