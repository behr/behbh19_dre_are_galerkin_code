%TEST_ARE_GALERKIN_DRE  Test for are_galerkin_dre.
%
%   TEST_ARE_GALERKIN_DRE compares the are_galerkin_dre solver with
%   the modified_davison_maki_dre and the splitting_dre solver.
%   The rail example is used.
%
%   See also ARE_GALERKIN_DRE. SPLITTING_DRE and MODIFIED_DAVISON_MAKI_DRE.
%   Author: Maximilian Behr (MPI Magdeburg, CSC)

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019
%

classdef test_are_galerkin_dre < matlab.unittest.TestCase

    properties(TestParameter)
        problem = { ... 
            % REMOVED SOME TESTS DUE TO RUNTIME ON CI SERVER
            %struct('instance', 'conv_diff1_20', 'T', 2^(-6), 'k', -12, 'reltol', 1e-1, 'CC_tol', 1e-15, 'krylov_tol', 1e-9) ...
            %struct('instance', 'conv_diff2_20', 'T', 2^(-6), 'k', -12, 'reltol', 1e-1, 'CC_tol', 1e-15, 'krylov_tol', 1e-9) ...
            struct('instance', 'conv_diff3_20', 'T', 2^(-6), 'k', -12, 'reltol', 1e-1, 'CC_tol', 1e-15, 'krylov_tol', 1e-9) ...
            %struct('instance', 'conv_diff4_20', 'T', 2^(-6), 'k', -12, 'reltol', 1e-1, 'CC_tol', 1e-15, 'krylov_tol', 1e-9) ...
            struct('instance', 'rail_371', 'T', 1, 'k', -4, 'reltol', 1e-1, 'CC_tol', 1e-15, 'krylov_tol', 1e-9) ...
            ...
            };
    end

    properties
        verbose = false;
    end

    methods(Test)

        function test(obj, problem)

            %% load data
            fprintf('\n');
            fprintf('----------------------------------------------\n');
            instance = problem.instance;
            T = problem.T;
            k = problem.k;
            reltol = problem.reltol;
            data = load(instance);
            A = data.A;
            B = full(data.B);
            C = full(data.C);
            n = size(A, 1);
            Z0 = zeros(n, 1);

            if isfield(data, 'E')
                E = data.E;
            else
                E = speye(n, n);
            end

            %% create are_galerkin_dre_solver
            solver_are_gal_opt = are_galerkin_dre_options();
            solver_are_gal_opt.T = T;
            solver_are_gal_opt.k = k;
            solver_are_gal_opt.lrnm_options.adi.output = obj.verbose;
            solver_are_gal_opt.lrnm_options.nm.output = obj.verbose;

            solver_are_gal = are_galerkin_dre(A, E, B, C, solver_are_gal_opt);
            solver_are_gal.prepare();

            %% create modified davison maki solver
            solver_mod_opt = modified_davison_maki_dre_options();
            solver_mod_opt.T = T;
            solver_mod_opt.k = k;
            solver_mod_opt.symmetric = true;

            solver_mod = modified_davison_maki_fac_sym_dre(full(A), full(E), B, C, Z0, solver_mod_opt);
            solver_mod.prepare();

            %% create symmetric splitting scheme solver
            solver_split_opt = splitting_dre_options();
            solver_split_opt.T = T;
            solver_split_opt.k = k;
            solver_split_opt.splitting = splitting_dre_t.SPLITTING_SYMMETRIC8;
            solver_split_opt.CC_tol = problem.CC_tol;
            solver_split_opt.krylov_tol = problem.krylov_tol;

            solver_split = splitting_dre(A, E, B, C, Z0, solver_split_opt);
            solver_split.prepare();

            %% solve algebraic riccati equation to monitor convergence
            Zinf = mess_care(A, E, B, C);

            %% measure time by hand to check if it is working
            time_are_gal = 0;
            time_mod = 0;
            time_split = 0;

            %% solve and compare solution
            while solver_mod.t < T

                % make time step
                temp = tic;
                solver_are_gal.time_step();
                time_are_gal = time_are_gal + toc(temp);

                temp = tic;
                solver_mod.time_step();
                time_mod = time_mod + toc(temp);

                temp = tic;
                solver_split.time_step();
                time_split = time_split + toc(temp);

                % check if solvers are synchronous
                assert(solver_are_gal.t == solver_mod.t);
                assert(solver_split.t == solver_mod.t);

                % get solutions
                [Q_gal, Xt_gal] = solver_are_gal.get_solution();
                Xt = solver_mod.get_solution();
                [L, D] = solver_split.get_solution();

                %% compare
                nrm2X_gal = two_norm_sym_handle(@(x) Q_gal*(Xt_gal * (Q_gal' * x)), n);
                nrm2X_mod = two_norm_sym_handle(@(x) Xt*x, n);
                nrm2X_split = two_norm_sym_handle(@(x) L*(D * (L' * x)), n);

                absnrm2_gal_mod = two_norm_sym_handle(@(x) Q_gal*(Xt_gal * (Q_gal' * x))-Xt*x, n);
                absnrm2_gal_split = two_norm_sym_handle(@(x) Q_gal*(Xt_gal * (Q_gal' * x))-L*(D * (L' * x)), n);
                absnrm2_mod_split = two_norm_sym_handle(@(x) Xt*x-L*(D * (L' * x)), n);

                diffXinf_gal = two_norm_sym_handle(@(x) Q_gal*(Xt_gal * (Q_gal' * x))-Zinf*(Zinf' * x), n);
                diffXinf_mod = two_norm_sym_handle(@(x) Xt*x-Zinf*(Zinf' * x), n);
                diffXinf_split = two_norm_sym_handle(@(x) L*(D * (L' * x))-Zinf*(Zinf' * x), n);

                % print results
                if obj.verbose
                    fprintf('%s instance=%s, k=%d, t=%.8f, %6.2f %%, size(gal) = %d, size(split) = %d\n', datestr(now), instance, k, solver_mod.t, solver_mod.t/T*100, size(Xt_gal,1), size(D,1));
                    fprintf('\t ||gal||_2                   = %e, ||mod||_2                     = %e, ||split||_2                   = %e\n', nrm2X_gal, nrm2X_mod, nrm2X_split);
                    fprintf('\t ||gal-mod||_2               = %e, ||gal-split||_2               = %e, ||mod-split||_2               = %e\n', absnrm2_gal_mod, absnrm2_gal_split, absnrm2_mod_split);
                    fprintf('\t ||gal-mod||_2 / ||gal||_2   = %e, ||gal-split||_2 / ||gal||_2   = %e, ||mod-split||_2 / ||gal||_2   = %e\n', absnrm2_gal_mod/nrm2X_gal, absnrm2_gal_split/nrm2X_gal, absnrm2_mod_split/nrm2X_gal);
                    fprintf('\t ||gal-mod||_2 / ||mod||_2   = %e, ||gal-split||_2 / ||mod||_2   = %e, ||mod-split||_2 / ||mod||_2   = %e\n', absnrm2_gal_mod/nrm2X_mod, absnrm2_gal_split/nrm2X_mod, absnrm2_mod_split/nrm2X_mod);
                    fprintf('\t ||gal-mod||_2 / ||split||_2 = %e, ||gal-split||_2 / ||split||_2 = %e, ||mod-split||_2 / ||split||_2 = %e\n', absnrm2_gal_mod/nrm2X_split, absnrm2_gal_split/nrm2X_split, absnrm2_mod_split/nrm2X_split);
                    fprintf('\t ||gal-Xinf||_2              = %e, ||mod-Xinf||_2                = %e, ||split-Xinf||_2              = %e\n', diffXinf_gal, diffXinf_mod, diffXinf_split);
                end

                % check tolerance for relative error
                obj.fatalAssertLessThan(absnrm2_gal_mod/nrm2X_gal, reltol);
                obj.fatalAssertLessThan(absnrm2_gal_split/nrm2X_gal, reltol);
                obj.fatalAssertLessThan(absnrm2_mod_split/nrm2X_gal, reltol);

                obj.fatalAssertLessThan(absnrm2_gal_mod/nrm2X_mod, reltol);
                obj.fatalAssertLessThan(absnrm2_gal_split/nrm2X_mod, reltol);
                obj.fatalAssertLessThan(absnrm2_mod_split/nrm2X_mod, reltol);

                obj.fatalAssertLessThan(absnrm2_gal_mod/nrm2X_split, reltol);
                obj.fatalAssertLessThan(absnrm2_gal_split/nrm2X_split, reltol);
                obj.fatalAssertLessThan(absnrm2_mod_split/nrm2X_split, reltol);

            end
            fprintf('Time %-35s Class / Measured %f / %f\n', upper(class(solver_are_gal)), solver_are_gal.wtime_time_step, time_are_gal);
            fprintf('Time %-35s Class / Measured %f / %f\n', upper(class(solver_mod)), solver_mod.wtime_time_step, time_mod);
            fprintf('Time %-35s Class / Measured %f / %f\n', upper(class(solver_split)), solver_split.wtime_time_step, time_split);
            fprintf('----------------------------------------------\n\n');

        end
    end
end
