%ARE_GALERKIN_DRE   Implements a Galerkin solver for the symmetric autonomous
%   differential Riccati equation based on the solution of the algebraic
%   Riccati equation.
%
%   Differential Riccati Equation:
%
%       \dot{X}    = A' X   +    X A -    X B B' X   + C'C, X(0)=0  (DRE)
%       E'\dot{X}E = A' X E + E' X A - E' X B B' X E + C'C, X(0)=0  (GDRE)
%
%   The class first solves the algebraic Riccati equation:
%
%       0 = A' X      +    X A   -    X B B' X   + C'C    (ARE)
%       0 = A' X E    + E' X A   - E' X B B' X E + C'C    (GARE)
%
%   for X = Z Z' and Z is a low-rank factor of the solution.
%   The trialspace for X is then formed from the compact truncated singular
%   value decomposition of Z.
%
%   ARE_GALERKIN_DRE methods:
%       are_galerkin_dre    - Constructor.
%
%   ARE_GALERKIN_DRE protected methods:
%       m_prepare                               - Prepare the solver, solve ARE, compute truncated svd and setup small-scale system.
%       m_time_step                             - Perform a time step.
%       m_get_solution                          - Return the approximation QXQ' at current time.
%       solve_are                               - Solve the algebraic Riccati equation.
%       prepare_modified_davison_maki_solver    - Prepare the modified Davison Maki solver.
%
%   ARE_GALERKIN_DRE private properties:
%       E                       - real, sparse, regular or empty n-by-n matrix or empty
%       A                       - real, sparse, n-by-n matrix
%       B                       - real, dense, n-by-b matrix
%       C                       - real, dense, c-by-n matrix
%       Z                       - truncated low-rank solution of ARE/GARE
%       S                       - singular values of truncated svd of Z
%       S2                      - squared singular values of truncated svd of Z
%       Q                       - orthonormal basis of truncated svd of ZQ
%       AF                      - small scale system
%       BF                      - small scale system
%       X0F                     - small scale system
%       abs2res_are             - abs. 2-norm residual of solution ARE
%       rel2res_are             - rel. 2-norm residual of solution ARE
%       status_are              - status from mess_lrnm
%       trunc_abs2res_are       - abs. 2-norm residual of truncated solution of ARE
%       trunc_rel2res_are       - rel. 2-norm residual of truncated solution of ARE
%       time_solve_are          - elapsed time to solve the ARE
%       time_prepare_solver_mod - elapsed time to prepare modified davison maki solver
%       solver_mod              - modified davison maki solver
%       solver_mod_opt          - options modified davison maki solver
%
%   See also SOLVER_DRE, ARE_GALERKIN_DRE_OPTIONS and TEST_ARE_GALERKIN_DRE.
%
%   Author: Maximilian Behr (MPI Magdeburg, CSC)

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019
%

classdef are_galerkin_dre < solver_dre

    properties(SetAccess = private)
        E % real, sparse, regular or empty n-by-n matrix or empty
        A % real, sparse, n-by-n matrix
        B % real, dense, n-by-b matrix
        C % real, dense, c-by-n matrix
        Z % truncated low-rank solution of ARE/GARE
        S % singular values of truncated svd of Z
        S2 % squared singular values of truncated svd of Z
        Q % orthonormal basis of truncated svd of ZQ
        AF % small scale system
        BF % small scale system
        X0F % small scale system
        abs2res_are = 0; % abs. 2-norm residual of solution ARE
        rel2res_are = 0; % rel. 2-norm residual of solution ARE
        status_are = []; % status from mess_lrnm
        trunc_abs2res_are = 0; % abs. 2-norm residual of truncated solution of ARE
        trunc_rel2res_are = 0; % rel. 2-norm residual of truncated solution of ARE
        nrm1_exp_hM = 0; % norm of the matrix exponential of modified davison maki solver
        time_solve_are = 0; % elapsed time to solve the ARE
        time_prepare_solver_mod = 0; % elapsed time to prepare modified davison maki solver
    end

    properties(Access = private)
        solver_mod % modified davison maki solver
        solver_mod_opt % options modified davison maki solver
    end

    methods

        function obj = are_galerkin_dre(A, E, B, C, opt)
            %ARE_GALERKIN_DRE  Constructor of class.
            %
            %   GAL = ARE_GALERKIN_DRE(A, E, B, C, OPT) creates an
            %   instance of the class. OPT must be an instance
            %   of are_galerkin_dre_options class. The matrices A and M must be
            %   double sparse and quadratic. If M is empty then the
            %   identity matrix is used. B, C and Z0 must be real
            %   and dense.
            %
            %   See also ARE_GALERKIN_DRE_OPTIONS and SOLVER_DRE.
            %
            %   Author: Maximilian Behr (MPI Magdeburg, CSC)

            %% check input arguments
            if ~isempty(E)
                validateattributes(E, {'double'}, {'square', 'real', 'finite', 'nonnan', '2d'}, mfilename, inputname(2));
            end
            validateattributes(A, {'double'}, {'square', 'real', 'finite', 'nonnan', '2d'}, mfilename, inputname(1));
            validateattributes(B, {'double'}, {'nonsparse', 'real', 'nonsparse', 'finite', 'nonnan', '2d'}, mfilename, inputname(3));
            validateattributes(C, {'double'}, {'nonsparse', 'real', 'nonsparse', 'finite', 'nonnan', '2d'}, mfilename, inputname(4));

            expected_class = 'are_galerkin_dre_options';
            assert(isa(opt, expected_class), '%s - %s has wrong type, expected %s.', inputname(5), class(opt), expected_class);
            assert(opt.lrnm_options.type == mess_operation_t.MESS_OP_TRANSPOSE, 'mess_operation type has to be MESS_OP_TRANSPOSE.');

            %% set arguments to properties
            obj.E = E;
            obj.A = A;
            obj.B = B;
            obj.C = C;
            obj.options = opt;

            %% set integration parameters to solver
            obj.solver_mod_opt = modified_davison_maki_dre_options();
            obj.solver_mod_opt.T = opt.T;
            obj.solver_mod_opt.k = opt.k;
            obj.solver_mod_opt.symmetric = true;
            obj.solver_mod_opt.name = opt.name;

            %% set integration parameters to instance
            obj.t = 0;
            obj.h = 2^opt.k;
            obj.T = obj.options.T;
            assert(0 < obj.h && obj.h < obj.T && mod(obj.T, obj.h) == 0, 'Time integration parameters error.');

        end
    end

    methods(Access = protected)

        function m_prepare(obj)
            %M_PREPARE  Prepare the solver for iteration.
            %
            %   MOD.M_PREPARE() solves the ARE and prepares the
            %   modified Davison Maki solver.
            %
            %   Author: Maximilian Behr (MPI Magdeburg, CSC)

            temp = tic;
            obj.solve_are();
            obj.time_solve_are = toc(temp);
            temp = tic;
            obj.prepare_modified_davison_maki_solver();
            obj.time_prepare_solver_mod = toc(temp);
        end

        function m_time_step(obj)
            %M_TIME_STEP  Time steppping function for solver.
            %
            %   MOD.M_TIME_STEP() calls the time_step function
            %   of the modified Davison Maki solver.
            %   The time of the are_galerkin_dre solver is updated
            %   accordingly.
            %
            %   Author: Maximilian Behr (MPI Magdeburg, CSC)

            obj.solver_mod.time_step();
            obj.t = obj.solver_mod.t;
        end

        function [Q, Xt] = m_get_solution(obj)
            %M_GET_SOLUTION  Return the current approximation as Q*Xt*Q'.
            %
            %   Author: Maximilian Behr (MPI Magdeburg, CSC)

            Q = obj.Q;
            Xt = obj.S2 - obj.solver_mod.get_solution();
        end

        function solve_are(obj)
            %SOLVE_ARE  Solves the algebraic Riccati equations.
            %
            %   GAL.SOLVE_ARE() solves the algebraic Riccati equation
            %   using MEX-M.E.S.S. using Newton-ADI.
            %   The low-rank factor is truncated using compact svd.
            %   The remaining left singular vectors are taken as
            %   trial space for the Galerkin ansatz.
            %
            %   Author: Maximilian Behr (MPI Magdeburg, CSC)

            %% create algebraic riccati equation and solve
            eqn = mess_equation_griccati(obj.options.lrnm_options, obj.A, obj.E, obj.B, obj.C);
            [obj.Z, obj.status_are] = mess_lrnm(eqn, obj.options.lrnm_options);
            obj.abs2res_are = obj.status_are.res2_norm;
            obj.rel2res_are = obj.status_are.res2_norm / obj.status_are.res2_0;
            fprintf('%s %s (%s): abs. / rel. 2-norm residual = %e / %e\n', datestr(now), upper(class(obj)), obj.options.name, obj.abs2res_are, obj.rel2res_are);
            fprintf('%s %s (%s): Newton Iterations           = %d\n', datestr(now), upper(class(obj)), obj.options.name, obj.status_are.it);
            fprintf('%s %s (%s): Size(Z)                     = %d-x-%d\n', datestr(now), upper(class(obj)), obj.options.name, size(obj.Z));

            %% perform svd truncation
            [obj.Z, obj.Q, obj.S, colsZ_new, colsZ_old] = truncate_ZZT(obj.Z, obj.options.trunc_tol);
            obj.S2 = obj.S.^2;
            tol = obj.S(1,1)*obj.options.trunc_tol;
            fprintf('%s %s (%s): Truncated columns of Z from %d to %d with bound %e\n', datestr(now), upper(class(obj)), obj.options.name, colsZ_old, colsZ_new, tol);
            obj.trunc_abs2res_are = res2_ric(obj.A, obj.E, obj.B, obj.C, obj.Z, mess_operation_t.MESS_OP_TRANSPOSE);
            obj.trunc_rel2res_are = obj.trunc_abs2res_are / norm(obj.C)^2;
            fprintf('%s %s (%s): Truncated abs. / rel. 2-norm residual = %e / %e\n', datestr(now), upper(class(obj)), obj.options.name, obj.trunc_abs2res_are, obj.trunc_rel2res_are);
        end

        function prepare_modified_davison_maki_solver(obj)
            %PREPARE_MODIFIED_DAVISON_MAKI_SOLVER  Prepare solver for dense DRE.
            %
            %   GAL.PREPARE_MODIFIED_DAVISON_MAKI_SOLVER() computes
            %   the system matrices for the Galerkin system and prepares
            %   the modified Davison Maki solver.
            %
            %   Author: Maximilian Behr (MPI Magdeburg, CSC)

            %% compute small scale system
            if isempty(obj.E)
                obj.AF = obj.Q' * obj.A * obj.Q - (obj.Q' * obj.B) * (obj.B' * (obj.Z * (obj.Z' * obj.Q)));
                obj.BF = obj.Q' * obj.B;
                obj.X0F = obj.S2;
            else
                EinvQinf = obj.E \ obj.Q;
                obj.AF = obj.Q' * obj.A * EinvQinf - (obj.Q' * obj.B) * (obj.B' * (obj.Z * (obj.Z' * obj.Q)));
                obj.BF = obj.Q' * obj.B;
                obj.X0F = obj.S2;
            end

            %% prepare solver for small scale system
            n = size(obj.X0F, 1);
            obj.solver_mod = modified_davison_maki_sym_dre(obj.AF, [], -obj.BF*obj.BF', zeros(n, n), obj.X0F, obj.solver_mod_opt);
            obj.solver_mod.prepare();
            
            %% get the norm of the matrix exponential
            obj.nrm1_exp_hM = obj.solver_mod.nrm1_exp_hM;
        end
    end
end
