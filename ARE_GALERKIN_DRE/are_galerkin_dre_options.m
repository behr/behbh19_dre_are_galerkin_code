%ARE_GALERKIN_DRE_OPTIONS    Options class for are_galerkin_dre solver.
%
%   ARE_GALERKIN_DRE_OPTIONS properties:
%       name            - char, name of your options settings
%       k               - int, stepsize will be 2^k
%       T               - double, final Time
%       trunc_tol       - double, positive, scalar, relative tolerance for compact svd truncation
%       lrnm_options    - options for mex-mess lrnm
%
%   See also ARE_GALERKIN_DRE, SOLVER_DRE_OPTIONS and TEST_ARE_GALERKIN_DRE.
%
%   Author: Maximilian Behr (MPI Magdeburg, CSC)

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019
%

classdef are_galerkin_dre_options < solver_dre_options

    properties
        name % char, name of your options settings
        k % int, stepsize will be 2^k
        T % double, final Time
        trunc_tol % double, positive, scalar, relative tolerance for compact svd truncation
        lrnm_options % options for mex-mess lrnm
    end

    methods
        function obj = are_galerkin_dre_options()
            %ARE_GALERKIN_DRE_OPTIONS  Constructor of class.
            %
            %   GAL_OPT = ARE_GALERKIN_OPTIONS() creates an instance of
            %   the class.
            %
            %   See also SOLVER_DRE_OPTIONS, ARE_GALERKIN_DRE and TEST_ARE_GALERKIN_DRE.
            %
            %   Author: Maximilian Behr (MPI Magdeburg, CSC)
            obj.name = 'empty';
            obj.k = -8;
            obj.T = 1.0;
            obj.trunc_tol = eps;
            obj.lrnm_options = mess_options;
            obj.lrnm_options.type = mess_operation_t.MESS_OP_TRANSPOSE;
            obj.lrnm_options.adi.res2_tol = 1e-16;
            obj.lrnm_options.adi.res2c_tol = 1e-14;
            obj.lrnm_options.adi.rel_change_tol = 1e-14;
            obj.lrnm_options.nm.res2_tol = 1e-16;
            obj.lrnm_options.nm.gpStep = 0;
            obj.lrnm_options.adi.shifts.paratype = mess_parameter_t.MESS_LRCFADI_PARA_ADAPTIVE_V;
            obj.lrnm_options.adi.output = true;
            obj.lrnm_options.nm.output = false;
        end
    end
end
