%SOLVER_DRE_OPTIONS     Abstract options class for differential Riccati
%   equation solver.
%
%   SOLVER_DRE_OPTIONS has the properties k and T.
%   T is the final time and k is for the step size h=2^k.
%
%   See also SPLITTING_DRE_OPTIONS, ARE_GALERKIN_DRE_OPTIONS and MODIFIED_DAVISON_MAKI_DRE_OPTIONS.
%
%   Author: Maximilian Behr (MPI Magdeburg, CSC)

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019
%

classdef (Abstract) solver_dre_options < handle
    properties(Abstract)
        k % step size will be 2^k
        T % final Time
    end
end
