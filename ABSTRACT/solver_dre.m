%SOLVER_DRE     Base class for deriving differential Riccati equation
%   solvers.
%
%   SOLVER_DRE implements functionality to measure number
%   of function calls and wall clock time.
%
%   SOLVER_DRE is a base class for single-step methods with constant
%   step size.
%
%   SOLVER_DRE methods to implement:
%       m_prepare       - Prepare function for solver.
%       m_time_step     - Time step function for solver.
%       m_get_solution  - Returns the approximation at current time.
%
%   SOLVER_DRE methods:
%       prepare             - Prepare function for solver.
%       time_step           - Time step function for solver.
%       time_step_until     - Time step function for solver.
%       get_solution        - Return the approximation at the current time.
%       print_status        - Print status information about ths solver.
%
%   See also ARE_GALERKIN_DRE, MODIFIED_DAVISON_MAKI_DRE and SPLITTING_DRE.
%
%   Author: Maximilian Behr (MPI Magdeburg, CSC)

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019
%

classdef (Abstract) solver_dre < handle

    properties(SetAccess = protected)
        t = 0; % double, current time
        h = 0; % double, positive step size
        T = 0; % double, final time
        options; % options of solver
        calls_prepare = int64(0); % number of calls of prepare function
        wtime_prepare = 0; % elapsed time for prepare function calls
        calls_time_step = int64(0); % number of calls of time_step function
        wtime_time_step = 0; % elapsed time for time_step function
        calls_time_step_until = int64(0); % number fo calls of time_step_until function
        wtime_time_step_until = 0; % elapsed time for time_step_until function
        calls_get_solution = int64(0); % number of calls of get_solution function
        wtime_get_solution = 0; % elapsed time for
    end

    methods(Abstract, Hidden = true, Access = protected)
        %M_PREPARE  Prepare function for solver.
        %
        %   M_PREPARE function is called before the time stepping
        %   functions are used.
        %
        %   Author: Maximilian Behr (MPI Magdeburg, CSC)
        m_prepare(obj);

        %M_TIME_STEP  Time step function for solver.
        %
        %   M_TIME_STEP performs one time step.
        %
        %   Author: Maximilian Behr (MPI Magdeburg, CSC)
        m_time_step(obj);

        %M_GET_SOLUTION  Returns the approximation at current time.
        %
        %   M_GET_SOLUTION returns the approximation at current time.
        %
        %   Author: Maximilian Behr (MPI Magdeburg, CSC)
        m_get_solution(obj);
    end

    methods(Sealed = true)

        function prepare(obj)
            %PREPARE  Prepare function for solver.
            %
            %   PREPARE wraps m_prepare to measure elapsed time and counts
            %   the number of calls of m_prepare.
            %
            %   Author: Maximilian Behr (MPI Magdeburg, CSC)
            temp = tic;
            obj.m_prepare();
            obj.wtime_prepare = obj.wtime_prepare + toc(temp);
            obj.calls_prepare = obj.calls_prepare + 1;
        end

        function time_step(obj)
            %TIME_STEP  Time step function for solver.
            %
            %   TIME_STEP wraps m_time_step to measure elapsed time and counts
            %   the number of calls of m_time_step.
            %
            %   Author: Maximilian Behr (MPI Magdeburg, CSC)
            assert(obj.t+obj.h <= obj.T, 'Cannot make time step, final Time reached, t = %f, h = %f, T = %f.', obj.t, obj.h, obj.T);
            temp = tic;
            obj.m_time_step();
            obj.wtime_time_step = obj.wtime_time_step + toc(temp);
            obj.calls_time_step = obj.calls_time_step + 1;
        end

        function time_step_until(obj, t)
            %TIME_STEP_UNTIL  Time step function for solver.
            %
            %   TIME_STEP_UNTIL calls m_time_step until the desired time
            %   t has reached. The time t must be a multiple of the
            %   step size. The elapsed time is measured.
            %
            %   Author: Maximilian Behr (MPI Magdeburg, CSC)
            assert(obj.t <= t, 'Desired t = %e is smaller the actual t = %e.', t, obj.t);
            assert(t <= obj.T, 'Desired t = %e is lager than the final T = %e.', t, obj.T);
            assert(mod(t, obj.h) == 0, 'Desired t = %e must be a multiple of h = %e\n', t, obj.h);
            temp = tic;
            while obj.t + obj.h <= t
                obj.m_time_step();
            end
            obj.wtime_time_step_until = obj.wtime_time_step_until + toc(temp);
            obj.calls_time_step_until = obj.calls_time_step_until + 1;
        end

        function varargout = get_solution(obj)
            %GET_SOLUTION  Return the approximation at the current time.
            %
            %   GET_SOLUTION returns the approximation at the current time.
            %
            %   Author: Maximilian Behr (MPI Magdeburg, CSC)
            temp = tic;
            [varargout{1:nargout}] = obj.m_get_solution();
            obj.wtime_get_solution = obj.wtime_get_solution + toc(temp);
            obj.calls_get_solution = obj.calls_get_solution + 1;
        end

    end

    methods
        function print_status(obj)
            %PRINT_STATUS  Print status information about ths solver.
            %
            %   Author: Maximilian Behr (MPI Magdeburg, CSC)
            fprintf('%s %s (%s): t = %7.6f, h = %7.6f, T = %7.6f, %.2f %%\n', datestr(now), upper(class(obj)), obj.options.name, obj.t, obj.h, obj.T, (obj.t / obj.T)*100);
        end
    end
end
