%RUN_EXAMPLE_EIG_DECAY_ARE
%
%   Calls example_eig_decay_are for different parameters.
%
%   See also TEST_EXAMPLE_EIG_DECAY_ARE and EXAMPLE_EIG_DECAY_ARE.
%
%   Author: Maximilian Behr (MPI Magdeburg, CSC)

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019
%

%% clear
clearvars, clc, close all

%% parameters
n = 10;
multiprec = true;
prec = 32;
plot_visible = false;

%% calls
for n_ = n
    for multiprec_ = multiprec
        for prec_ = prec
            for plot_visible_ = plot_visible
                example_eig_decay_are(n_, multiprec_, prec_, plot_visible_);
            end
        end
    end
end
