function example_eig_decay_are(n, multiprec, prec, plot_visible)
%EXAMPLE_EIG_DECAY_ARE     Shows the eigenvalue decay of the symmetric
%   positive semidefinite solution of the algebraic Riccati equation.
%
%   EXAMPLE_EIG_DECAY_ARE solves several algebraic Riccati equations
%
%       A' X + X A - X B B' X + C' C = 0
%
%   for tridiagonal matrices A = tridiag(alpha, -1, alpha), B = ones(n, 1)
%   and C = ones(1,n). The parameters alpha are hard coded. The eigenvalues
%   of the solution is computed numerically and the results are written to disk.
%
%   EXAMPLE_EIG_DECAY_ARE(N, MULTIPREC, PREC, PLOT_VISIBLE)
%
%   Arguments:
%   ALPHA               - double, scalar number
%   N                   - nonnegative integer, size of A
%   MULTIPREC           - logical, turn multiprecision on/off
%   PREC                - nonegative integer, control precision
%   PLOTTING_VISIBLE    - logical, turn visible plotting on/off
%
%   If MULTIPREC is false, then standard IEEE double precision is used,
%   otherwise variable precision arithmetic (vpa) is used.
%   The number of significant digits can be controlled by PREC.
%
%   See also TEST_EXAMPLE_EIG_DECAY_ARE and RUN_EXAMPLE_EIG_DECAY_ARE.
%
%   Author: Maximilian Behr (MPI Magdeburg, CSC)

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019
%

%% turn multiprec on/off, if multiprec is on it takes significantly longer
if multiprec
    multiprec_handle = @(x) vpa(x);
    digits(prec);
else
    multiprec_handle = @(x) x;
    prec = 16;
end

%% check matlab version and license, because of fprintf incompatbility with symbolic
if verLessThan('matlab', '9.0') && multiprec
    error('Matlab Version is too old. fprintf does not work with symbolic inputs.');
end

if ~license('test', 'Symbolic_Toolbox') && multiprec
    error('Symbolic Toolbox not available.');
end

%% set data for algebraic Riccati equation A' X + X A - X B B' X + C' C
alphas = [1, 5, 1e1, 1e2, 2e2, 5e2, 1e3, 1e4];
Afun = @(n, alpha) multiprec_handle(full(gallery('tridiag', n, alpha, -1, -alpha)));
Bfun = @(n) multiprec_handle(ones(n, 1));
Cfun = @(n) multiprec_handle(ones(1, n));

%% create filenames and directories for results
mybasename = sprintf('%s_n%d_prec%d_multiprec%d', mfilename, n, prec, multiprec);
[mydir, ~, ~] = fileparts(mfilename('fullpath'));
mydir = fullfile(mydir, 'results', mfilename, mybasename);
[status, message, messageid] = rmdir(mydir, 's');
if ~status
    fprintf('%s - %s:%s-%s\n', datestr(now), mfilename, message, messageid);
end
mydiary = fullfile(mydir, sprintf('%s.log', mybasename));
mydat = fullfile(mydir, sprintf('%s.dat', mybasename));
mymat = fullfile(mydir, sprintf('%s.mat', mybasename));
mypic = fullfile(mydir, mybasename);
mkdir(mydir);

%% delete old diary and create new one
delete(mydiary);
diary(mydiary);

%% solve Riccati equation and compute eigenvalues
nb = length(alphas);
evals(n, nb) = 0;
abs2nrm(1, nb) = 0;
rel2nrm(1, nb) = 0;

for j = 1:nb

    %% set up matrices
    fprintf('%s - Set up Matrices n = %d alpha = %.2f\n', datestr(now), n, alphas(j));
    A = Afun(n, alphas(j));
    B = Bfun(n);
    C = Cfun(n);

    %% compute solution of A'X+XA -XBB'X +C'C =0
    fprintf('%s - Solve algebraic Riccati equation\n', datestr(now));
    if multiprec
        [V, D] = eig([A, -B * B'; -C' * C, -A']);
        [~, I] = sort(real(diag(D)), 'ascend');
        I = I(1:n);
        V = V(:, I);
        Xinf = V((n + 1):end, 1:n) / V(1:n, 1:n);
        Xinf = real(Xinf);
        Xinf = 0.5 * (Xinf + Xinf');
    else
        Xinf = care(A, B, C'*C);
    end

    fprintf('%s - Compute abs./rel. 2-norm residual of solution of solution of algebraic Riccati equation\n', datestr(now));
    abs2nrm(j) = norm(A'*Xinf+Xinf*A-(Xinf * B)*(B' * Xinf)+C'*C);
    t = norm(C)^2;
    rel2nrm(j) = abs2nrm(j) / t;
    fprintf('%s - alpha = %.2f - 2-norm residual: abs./rel. %e / %e\n', datestr(now), double(alphas(j)), double(abs2nrm(j)), double(rel2nrm(j)));

    %% compute eigenvalues values
    fprintf('%s - Compute eigenvalues of solution of algebraic Riccati equation\n', datestr(now));
    [~, t] = eig(Xinf);
    t = abs(diag(t));
    t = sort(t, 'descend');
    evals(:, j) = t;
end

%% write to file for tikz and save mat
hostname = getComputerName();
timenow = datestr(now);
gitinfo = GitInfo();
mycpuinfo = cpuinfo();

dlmwrite(mydat, sprintf('mfilename              = %s', mfilename), 'delimiter', '');
dlmwrite(mydat, sprintf('Time                   = %s', timenow), 'delimiter', '', '-append');
dlmwrite(mydat, sprintf('Host                   = %s', hostname), 'delimiter', '', '-append');
dlmwrite(mydat, sprintf('CPU-Name               = %s', mycpuinfo.Name), 'delimiter', '', '-append');
dlmwrite(mydat, sprintf('CPU-Clock              = %s', mycpuinfo.Clock), 'delimiter', '', '-append');
dlmwrite(mydat, sprintf('CPU-Cache              = %s', mycpuinfo.Cache), 'delimiter', '', '-append');
dlmwrite(mydat, sprintf('CPU-NumProcessors      = %d', mycpuinfo.NumProcessors), 'delimiter', '', '-append');
dlmwrite(mydat, sprintf('CPU-OSType             = %s', mycpuinfo.OSType), 'delimiter', '', '-append');
dlmwrite(mydat, sprintf('CPU-OSVersion          = %s', mycpuinfo.OSVersion), 'delimiter', '', '-append');
dlmwrite(mydat, sprintf('Version                = %s', version()), 'delimiter', '', '-append');
dlmwrite(mydat, sprintf('Project remote-url     = %s', gitinfo.remote_url), 'delimiter', '', '-append');
dlmwrite(mydat, sprintf('Project git-sha        = %s', gitinfo.sha), 'delimiter', '', '-append');
dlmwrite(mydat, sprintf('Project git-branch     = %s', gitinfo.branch), 'delimiter', '', '-append');
dlmwrite(mydat, sprintf('n                      = %e', n), 'delimiter', '', '-append');
dlmwrite(mydat, sprintf('prec                   = %e', prec), 'delimiter', '', '-append');
dlmwrite(mydat, sprintf('multiprec              = %d', multiprec), 'delimiter', '', '-append');

alphasstr = sprintf('alpha_%d,', alphas);
alphasstr = alphasstr(1:end-1);
alphastridx = sprintf('%d,', alphas);
alphastridx = alphastridx(1:end-1);
dlmwrite(mydat, alphastridx, 'delimiter', '', '-append');
dlmwrite(mydat, alphasstr, 'delimiter', '', '-append');
dlmwrite(mydat, evals, 'delimiter', ',', '-append', 'precision', prec);

save(mymat, 'n', 'alphas', 'evals', 'abs2nrm', 'rel2nrm', 'timenow');

%% turn diary off
diary('off');

%% plot results
close all;
close all hidden;
if ~plot_visible
    set(0, 'DefaultFigureVisible', 'off');
end

list = hsv(length(alphas));
for j = 1:size(list, 1)
    semilogy(evals(:, j), 'o', 'DisplayName', sprintf('\\alpha = %d\n', alphas(j)), 'color', list(j, :))
    hold on
    set(gcf, 'Position', [100, 100, 1024, 1024]);
end
title('Eigenvalues of X for different values of \alpha.');
legend('show')
hold off

saveas(gcf, sprintf('%s_n_%d.eps', mypic, n), 'epsc');
saveas(gcf, sprintf('%s_n_%d.jpg', mypic, n));

% reset setting
if ~plot_visible
    set(0, 'DefaultFigureVisible', 'on');
end

end
