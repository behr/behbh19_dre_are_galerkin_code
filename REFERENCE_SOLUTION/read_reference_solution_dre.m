%READ_REFERENCE_SOLUTION_DRE    Class reads a mat binary files that
%   contain a precompouted solution of a differential Riccati equation
%   from create_reference_solution_dre function.
%
%   READ_REFERENCE_SOLUTION_DRE methods:
%       read_reference_solution_dre - Constructor.
%       get_solution_at             - Return the loaded solution at time t.
%
%   READ_REFERENCE_SOLUTION_DRE properties:
%       mymat       - string, path to mat file which contains the solution
%       mymatfile   - matfile object to mymat
%       store_at    - array of time points where the solution is available
%       name        - name of the loaded solution
%
%   See also CREATE_REFERENCE_SOLUTION_DRE and RUN_REFERENCE_SOLUTION_DRE.
%
%   Author: Maximilian Behr (MPI Magdeburg, CSC)

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019
%

classdef read_reference_solution_dre < handle

    properties
        mymat % string, path to mat file which contains the solution
        mymatfile % matfile object to mymat
        store_at % array of time points where the solution is available
        name % name of the loaded solution
    end

    methods
        function obj = read_reference_solution_dre(mymat)
            %READ_REFERENCE_SOLUTION_DRE    Constructor of class.
            %
            %   REF = READ_REFERENCE_SOLUTION_DRE(MYMAT)
            %   creates an instance of the class. MYMAT must be a string
            %   of the path of the mat file, which contains the reference
            %   solution.
            %
            %   See also RUN_REFERENCE_SOLUTION_DRE.
            %
            %   Author: Maximilian Behr (MPI Magdeburg, CSC)

            %% check if file exists
            assert(exist(mymat, 'file') == 2, 'File %s does not exists or is not a mat binary file.', mymat)

            %% load file and check
            fprintf('%s %s\n', datestr(now), upper(mfilename));
            obj.mymat = mymat;
            obj.mymatfile = matfile(mymat, 'Writable', false);

            %% check the loaded file
            fprintf('%s %s - Check consistency of reference solution and field names.\n', datestr(now), upper(mfilename));
            p = properties(obj.mymatfile);
            fields_check = {'name', 'mfilename', 'solver_name', 'timenow', 'store_at', 'T', 'solver_options', 'solver_options_struct', 'gitinfo', 'cpu', 'k', 'L_1', 'D_1'};
            for i = 1:length(fields_check)
                field_check = fields_check{i};
                assert(any(strcmp(p, field_check)), 'Could not read field %s from %s.', field_check, mymat);
            end

            %% get some fields
            obj.store_at = obj.mymatfile.store_at;
            assert(~isempty(obj.store_at), 'store_at seems is empty.');
            obj.name = obj.mymatfile.name;

            %% check consistency
            for i = 1:length(obj.store_at)
                Lvar = sprintf('L_%d', i);
                Dvar = sprintf('D_%d', i);
                assert(any(strcmp(p, Lvar)), 'Could not read field %s from %s.', Lvar, mymat);
                assert(any(strcmp(p, Dvar)), 'Could not read field %s from %s.', Dvar, mymat);
            end

            %% print info about loaded file
            fprintf('%s %s - Information about generated Reference Solution File %s\n', datestr(now), upper(mfilename), obj.mymat);
            fprintf('\t name                = %s\n', obj.mymatfile.name);
            fprintf('\t solver_name         = %s\n', obj.mymatfile.solver_name);
            fprintf('\t time                = %s\n', obj.mymatfile.timenow);
            fprintf('\t T                   = %f\n', obj.mymatfile.T);
            fprintf('\t k                   = %d\n', obj.mymatfile.k);
            fprintf('\t h                   = %e\n', 1/2^obj.mymatfile.k);
            if length(obj.store_at) >= 3
                fprintf('\t store_at            = [%f, %f, ..., %f]\n', obj.store_at(1), obj.store_at(2), obj.store_at(end));
            end
            fprintf('\t length(store_at)    = %d\n', length(obj.store_at));
            fprintf('\t hostname            = %s\n', obj.mymatfile.hostname);
            mycpuinfo = obj.mymatfile.cpu;
            fprintf('\t CPU-Name            = %s\n', mycpuinfo.Name);
            fprintf('\t CPU-Clock           = %s\n', mycpuinfo.Clock);
            fprintf('\t CPU-Cache           = %s\n', mycpuinfo.Cache);
            fprintf('\t CPU-NumProcessors   = %d\n', mycpuinfo.NumProcessors);
            fprintf('\t CPU-OSType          = %s\n', mycpuinfo.OSType);
            fprintf('\t CPU-OSVersion       = %s\n', mycpuinfo.OSVersion);

            mygitinfo = obj.mymatfile.gitinfo;
            fprintf('\t Project remote-url  = %s\n', mygitinfo.remote_url);
            fprintf('\t Project git-sha     = %s\n', mygitinfo.sha);
            fprintf('\t Project git-branch  = %s\n', mygitinfo.branch);
            fprintf('\t version             = %s\n', obj.mymatfile.version);

        end

        function [L, D] = get_solution_at(obj, t)
            %GET_SOLUTION_AT    Return the solution at a specific time point.
            %
            %   [L,D] = REF.GET_SOLUTION_AT(T) returns the approximation
            %   L*D*L' of X(T).
            %
            %   See also RUN_REFERENCE_SOLUTION_DRE.
            %
            %   Author: Maximilian Behr (MPI Magdeburg, CSC)

            assert(ismember(t, obj.store_at), 'Reference Solution not available for t = %e', t);
            idx = find(obj.store_at == t);
            assert(length(idx) == 1, 'Cannot find index for t = %e', t);
            Lfield = sprintf('L_%d', idx);
            Dfield = sprintf('D_%d', idx);
            fprintf('%s %s - Load %s and %s at time t = %e\n', datestr(now), upper(mfilename), Lfield, Dfield, t);
            L = obj.mymatfile.(Lfield);
            D = obj.mymatfile.(Dfield);
        end
    end
end
