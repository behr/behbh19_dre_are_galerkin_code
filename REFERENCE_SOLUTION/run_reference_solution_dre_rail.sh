#!/bin/bash
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#
# Copyright (C) Maximilian Behr
#               2019
#


# SETUP Your MATLAB environment
MATLAB=matlab2018a

host=$(hostname)

# PARAMETER COMBINATIONS FOR RAIL EXAMPLE
INSTANCES=("rail_371" "rail_1357" "rail_5177" "rail_20209")
KS=(-5)
SOLVER_TYPE="SPLITTING_SYMMETRIC8"

for INSTANCE in "${INSTANCES[@]}"; do
    for K in "${KS[@]}"; do
        echo "START K=${K} INSTANCE=${INSTANCE} HOST=${host}" |  mail -s "RAIL REFERENCE SOLUTION ${INSTANCE} - ${host} START" behr@mpi-magdeburg.mpg.de

        # check for instance
        if [ "${INSTANCE}" == "rail_5177" -o "${INSTANCE}" == "rail_20209" -o "${INSTANCE}" == "rail_79841" ]; then
            T=470
            SUFFIX="T_470"
        else
            T=470
            SUFFIX="T_470"
        fi

        # CODE to execute
        CODE="\
            run ../add_to_path;                                                                                             \
            instance                    = '${INSTANCE}';                                                                    \
            data                        = load(instance);                                                                   \
            A                           = data.A;                                                                           \
            E                           = data.E;                                                                           \
            B                           = full(data.B);                                                                     \
            C                           = full(data.C);                                                                     \
            Z0                          = zeros(size(A,1),1);                                                               \
            n                           = size(A,1);                                                                        \
            solver_options              = splitting_dre_options();                                                          \
            solver_options.k            = ${K};                                                                             \
            solver_options.T            = ${T};                                                                             \
            solver_options.splitting    = splitting_dre_t.${SOLVER_TYPE};                                                   \
            solver_options.CC_tol       = n*eps;                                                                            \
            solver_options.krylov_tol   = 1e-9;                                                                             \
            suffix                      = '${SUFFIX}';                                                                      \
            store_at                    = 0:1:${T};                                                                         \
            name                        = sprintf('%s_%s_k%d_%s', instance, char(solver_options.splitting), ${K}, suffix);  \
            control_parfor(false);                                                                                          \
            create_reference_solution_dre(name, A, E, B, C, Z0, solver_options, store_at);                                  \
            quit();                                                                                                         \
            "
        ${MATLAB} -nodesktop -nosplash -r "${CODE}";
        echo "FINISHED K=${K} INSTANCE=${INSTANCE}" HOST=${host} |  mail -s "RAIL REFERENCE SOLUTION ${INSTANCE} - ${host} FINISHED" behr@mpi-magdeburg.mpg.de
    done
done
echo "FINISHED ALL" |  mail -s "RAIL REFERENCE SOLUTION ${host} FINISHED ALL" behr@mpi-magdeburg.mpg.de
