function create_reference_solution_dre(name, A, M, B, C, Z0, solver_options, store_at)
%CREATE_REFERENCE_SOLUTION_DRE  Computes a reference solution for a
%   differential Riccati equation. The reference solution is written to
%   a mat file and can be later loaded using read_reference_solution.
%   The differential Riccati equation is given by
%
%       M'\dot{X}M = A' X M + M' X A - M' X B B' X M + C' C, X(0) = Z0 Z0'.
%
%   CREATE_REFERENCE_SOLUTION_DRE(NAME, A, M, B, C, Z0, SOLVER_OPTIONS, STORE_AT)
%   NAME must be a string and should name your instance. If M is empty
%   then the identity matrix is used. SOLVER_OPTIONS must be an
%   instance of splitting_dre_options. STORE_AT is a sorted vector, which
%   contains the time points where the solution should be stored.
%
%   See also CREATE_REFERENCE_SOLUTION_DRE, RUN_REFERENCE_SOLUTION_DRE and
%   READ_REFERENCE_SOLUTION_DRE.
%
%   Author: Maximilian Behr (MPI Magdeburg, CSC)

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019
%

%% some input parameter checks
assert(ischar(name));
assert(isnumeric(store_at) && isrow(store_at) && issorted(store_at));
assert(isa(solver_options, 'splitting_dre_options'));

k = solver_options.k;
T = solver_options.T;

assert(~any(mod(store_at, 2^k)), 'store_at must only contain multiples of stepsize');
assert(all(store_at <= T), 'store_at be smaller than T');
assert(all(0 <= store_at), 'store_at larger equals 0');

%% create necessary filenames and create directory for results
mybasename = sprintf('%s', name);
[mydir, ~, ~] = fileparts(mfilename('fullpath'));
mydir = fullfile(mydir, 'results', mfilename, mybasename);
[status, message, messageid] = rmdir(mydir, 's');
if ~status
    fprintf('%s - %s:%s-%s\n', datestr(now), mfilename, message, messageid);
end
mkdir(mydir);

mydiary = fullfile(mydir, sprintf('%s.log', mybasename));
mydat = fullfile(mydir, sprintf('%s.dat', mybasename));
mymat = fullfile(mydir, sprintf('%s.mat', mybasename));
mymatfile = matfile(mymat, 'Writable', true);

%% delete old diary and create new one
delete(mydiary);
diary(mydiary);

%% generate and prepare solver for reference solution
fprintf('%s - Prepare %s splitting scheme for reference solution.\n', datestr(now), char(solver_options.splitting));
solver = splitting_dre(A, M, B, C, Z0, solver_options);
solver.prepare();

%% solve and store solution
for i_store = 1:length(store_at)

    % integrate to desired time with reference solver
    t = store_at(i_store);
    fprintf('%s - Integrate with %s solver, %s, k=%d, t=%.10f, T=%.10f, %6.2f %%.\n', datestr(now), char(solver_options.splitting), name, k, t, T, t/T*100);
    solver.time_step_until(t);

    % get solution from refrence solver
    [L, D] = solver.get_solution(); %#ok<ASGLU>

    % store solution
    fprintf('%s - Store solution at t=%.10f, cols(L) = %d.\n', datestr(now), t, size(L, 2));
    eval(sprintf('mymatfile.L_%d = L;', i_store));
    eval(sprintf('mymatfile.D_%d = D;', i_store));
end

%% Write to file dat file
fprintf('%s - Start writing data to dat file\n', datestr(now));
hostname = getComputerName();
timenow = datestr(now);
gitinfo = GitInfo();
mycpuinfo = cpuinfo();

dlmwrite(mydat, sprintf('mfilename              = %s', mfilename), 'delimiter', '');
dlmwrite(mydat, sprintf('Time                   = %s', timenow), 'delimiter', '', '-append');
dlmwrite(mydat, sprintf('Host                   = %s', hostname), 'delimiter', '', '-append');
dlmwrite(mydat, sprintf('CPU-Name               = %s', mycpuinfo.Name), 'delimiter', '', '-append');
dlmwrite(mydat, sprintf('CPU-Clock              = %s', mycpuinfo.Clock), 'delimiter', '', '-append');
dlmwrite(mydat, sprintf('CPU-Cache              = %s', mycpuinfo.Cache), 'delimiter', '', '-append');
dlmwrite(mydat, sprintf('CPU-NumProcessors      = %d', mycpuinfo.NumProcessors), 'delimiter', '', '-append');
dlmwrite(mydat, sprintf('CPU-OSType             = %s', mycpuinfo.OSType), 'delimiter', '', '-append');
dlmwrite(mydat, sprintf('CPU-OSVersion          = %s', mycpuinfo.OSVersion), 'delimiter', '', '-append');
dlmwrite(mydat, sprintf('Version                = %s', version()), 'delimiter', '', '-append');
dlmwrite(mydat, sprintf('Project remote-url     = %s', gitinfo.remote_url), 'delimiter', '', '-append');
dlmwrite(mydat, sprintf('Project git-sha        = %s', gitinfo.sha), 'delimiter', '', '-append');
dlmwrite(mydat, sprintf('Project git-branch     = %s', gitinfo.branch), 'delimiter', '', '-append');
dlmwrite(mydat, sprintf('name                   = %s', name), 'delimiter', '', '-append');
dlmwrite(mydat, sprintf('T                      = %e', T), 'delimiter', '', '-append');
dlmwrite(mydat, sprintf('k                      = %d', k), 'delimiter', '', '-append');
dlmwrite(mydat, sprintf('Solver                 = %s', char(solver_options.splitting)), 'delimiter', '', '-append');
dlmwrite(mydat, sprintf('time_solver            = %e', solver.wtime_time_step_until), 'delimiter', '', '-append');

HEADER = 'index,t';
dlmwrite(mydat, HEADER, 'delimiter', '', '-append');

for i_store = 1:length(store_at)
    dlmwrite(mydat, [i_store, store_at(i_store)], 'delimiter', ',', '-append', 'precision', 16);
end

fprintf('%s - Finished writing dat file\n\n', datestr(now));

%% write to mat file
% data and solver
mymatfile.name = name;
mymatfile.A = A;
mymatfile.B = B;
mymatfile.C = C;
mymatfile.M = M;
mymatfile.Z0 = Z0;
mymatfile.solver_options = solver_options;
mymatfile.solver_options_struct = struct(solver_options);
mymatfile.T = T;
mymatfile.k = k;
mymatfile.solver_name = char(solver_options.splitting);
mymatfile.store_at = store_at;
mymatfile.solver_str = struct(solver);
mymatfile.solver = solver;

% meta info
mymatfile.mfilename = mfilename;
mymatfile.timenow = timenow;
mymatfile.hostname = hostname;
mymatfile.cpu = mycpuinfo;
mymatfile.gitinfo = struct(gitinfo);
mymatfile.version = version();

%% turn diary off
diary('off');

end
