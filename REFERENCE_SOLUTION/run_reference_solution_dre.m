%RUN_REFERENCE_SOLUTION_DRE     Calls create_reference_solution_dre.
%
%   See also READ_REFERENCE_SOLUTION and CREATE_REFERENCE_SOLUTION_DRE.
%
%   Author: Maximilian Behr (MPI Magdeburg, CSC)

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019
%

%% clear
clearvars, clc, close all

%% parameters
instance = 'rail_371';
data = load(instance);
A = data.A;
E = data.E;
B = full(data.B);
C = full(data.C);
Z0 = zeros(size(A, 1), 1);
n = size(A, 1);

solver_options = splitting_dre_options();
solver_options.k = -2;
solver_options.T = 20;
solver_options.splitting = splitting_dre_t.SPLITTING_SYMMETRIC8;
solver_options.krylov_tol = 1e-9;
solver_options.CC_tol = n * eps;

store_at = 1:1:solver_options.T;
name = sprintf('%s_%s_k%d', instance, char(solver_options.splitting), solver_options.k);

%% call
control_parfor(false);
create_reference_solution_dre(name, A, E, B, C, Z0, solver_options, store_at);
